/**
 * @author Elmer Valencia Ledezmas / gmail: elmer.r.valencia@gmail.com / cel:
 * 71413596
 */
package Model;

import Container.EntityContainer;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.LinkedList;

public class IdiomsModel {

    //PRIMERA ENTIDAD:
    private int X;
    private int Y;
    private int WIDTH = 100;
    private int HEIGHT = 80;
    //SEGUNDA ENTIDAD:
    private int X_E2;
    private int Y_E2;
    private int WIDTH_E2 = 100;
    private int HEIGHT_E2 = 80;
    //TERCERA ENTIDAD:
    private int X_E3;
    private int Y_E3;
    private int WIDTH_E3 = 100;
    private int HEIGHT_E3 = 80;
    //CUARTA ENTIDAD:
    private int X_E4;
    private int Y_E4;
    private int WIDTH_E4 = 100;
    private int HEIGHT_E4 = 80;
    //QUINTA ENTIDAD:
    private int X_E5;
    private int Y_E5;
    private int WIDTH_E5 = 100;
    private int HEIGHT_E5 = 80;

    private String IDIOMS_TYPE;
    private String IDIOMS_POSITION;

    private Font FUENTE = new Font("Arial", Font.PLAIN, 12);

    //CASO ESPECIAL IDIOMS:
    private IdiomsModel IM;
    private int X_N;
    private int Y_N;
    private int WIDTH_N = 100;
    private int HEIGHT_N = 80;

    //LISTA DE CADA ENTIDAD(INFORMACION COMPLETA DE CADA ENTIDAD)
    private EntityContainer ENTITY_CONTAINER;
    //NOMBRES DE TODAS LAS ENTIDAD
    private String NAME_ENTITY_MAESTRO, NAME_ENTITY_DETALLE;
    private String NAME_ENTITY_CLASIFICADOR, NAME_ENTITY_CLASIFICADO;
    private String NAME_ENTITY_COMPOSICION, NAME_ENTITY_COMPONEDOR, NAME_ENTITY_COMPONEDOR1;
    private String NAME_ENTITY_GENERAL, NAME_ENTITY_ISA;
    private String NAME_ENTITY_RSIMPLE;
    private String NAME_ENTITY_ACTOR, NAME_ENTITY_HISTORIA;
    private String NAME_ENTITY_BASICO;

    private String ENTITY_M_NAME = "", ENTITY_D_NAME = "";
    private String ENTITY_CLR_NAME = "", ENTITY_CLO_NAME = "";
    private String ENTITY_C_NAME = "", ENTITY_C1_NAME = "", ENTITY_C2_NAME = "";
    private String ENTITY_G_NAME = "", ENTITY_I_NAME = "";
    private String ENTITY_RS_NAME = "";
    private String ENTITY_A_NAME = "", ENTITY_H_NAME = "";
    private String ENTITY_B_NAME = "";
    //VARIBLES DE ESTADO PARA LAS LLAVES PRIMARIAS
    private boolean STATE_EXIST_FK;
    //CONTADORES PARA LA CREACION DE ENTIDADES
    private int CONTADOR_E;
    //TIPO DE ENTIDAD RELACIONADORA:
    private String ENTITY_RELATION_NAME;
    private String ENTITY_TIPE;

    public IdiomsModel(int x, int y, String tipe, String posicion, EntityContainer eContainer, int contador_e, String entityRelation, String entityTipe) {
        this.X = x;
        this.Y = y;
        this.X_E2 = x + 250;
        this.Y_E2 = y;
        this.X_E3 = x - 250;
        this.Y_E3 = y;
        this.X_E4 = x;
        this.Y_E4 = y + 200;
        this.X_E5 = x;
        this.Y_E5 = y - 200;
        this.IDIOMS_TYPE = tipe;
        this.IDIOMS_POSITION = posicion;
        this.ENTITY_CONTAINER = eContainer;
        this.CONTADOR_E = contador_e;
        this.ENTITY_RELATION_NAME = entityRelation;
        this.ENTITY_TIPE = entityTipe;
    }

    public void diseniarIdioms(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        //TIPOS DE IDIOMS:
        if (this.IDIOMS_TYPE.equals("MD")) {
            diseniarMD(g2);
        }
        if (this.IDIOMS_TYPE.equals("CC")) {
            diseniarCC(g2);
        }
        if (this.IDIOMS_TYPE.equals("C")) {
            diseniarC(g2);
        }
        if (this.IDIOMS_TYPE.equals("ISA")) {
            diseniarISA(g2);
        }
        if (this.IDIOMS_TYPE.equals("RS")) {
            diseniarRS(g2);
        }
        if (this.IDIOMS_TYPE.equals("RH")) {
            diseniarRH(g2);
        }
        if (this.IDIOMS_TYPE.equals("B")) {
            diseniarB(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD MAESTRO:
        if (this.IDIOMS_TYPE.equals("Maestro") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarMaestroLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("Maestro") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarMaestroRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("Maestro") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarMaestroUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("Maestro") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarMaestroBelow(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD DETALLE:
        if (this.IDIOMS_TYPE.equals("Detalle") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarDetalleLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("Detalle") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarDetalleRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("Detalle") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarDetalleUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("Detalle") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarDetalleBelow(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD CLASIFICADOR:
        if (this.IDIOMS_TYPE.equals("Clasificador") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarClasificadorLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("Clasificador") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarClasificadorRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("Clasificador") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarClasificadorUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("Clasificador") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarClasificadorBelow(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD CLASIFICADO:
        if (this.IDIOMS_TYPE.equals("Clasificado") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarClasificadoLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("Clasificado") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarClasificadoRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("Clasificado") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarClasificadoUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("Clasificado") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarClasificadoBelow(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD COMPOSICION:
        if (this.IDIOMS_TYPE.equals("Composicion") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarComposicionLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("Composicion") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarComposicionRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("Composicion") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarComposicionUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("Composicion") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarComposicionBelow(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD COMPONEDOR:
        if (this.IDIOMS_TYPE.equals("Componedor") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarComponedorLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("Componedor") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarComponedorRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("Componedor") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarComponedorUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("Componedor") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarComponedorBelow(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD GENERAL:
        if (this.IDIOMS_TYPE.equals("General") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarGeneralLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("General") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarGeneralRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("General") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarGeneralUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("General") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarGeneralBelow(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD ISA:
        if (this.IDIOMS_TYPE.equals("Isa") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarIsaLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("Isa") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarIsaRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("Isa") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarIsaUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("Isa") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarIsaBelow(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD RSIMPLE:
        if (this.IDIOMS_TYPE.equals("RSimple") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarRSimpleLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("RSimple") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarRSimpleRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("RSimple") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarRSimpleUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("RSimple") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarRSimpleBelow(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD SEMEJANTE:
        if (this.IDIOMS_TYPE.equals("Actor") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarActorLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("Actor") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarActorRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("Actor") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarActorUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("Actor") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarActorBelow(g2);
        }
        //TIPOS DE POSICIONE DE LA ENTIDAD HISTORIA:
        if (this.IDIOMS_TYPE.equals("Historia") && this.IDIOMS_POSITION.equals("Left")) {
            diseniarHistoriaLeft(g2);
        }
        if (this.IDIOMS_TYPE.equals("Historia") && this.IDIOMS_POSITION.equals("Right")) {
            diseniarHistoriaRight(g2);
        }
        if (this.IDIOMS_TYPE.equals("Historia") && this.IDIOMS_POSITION.equals("Up")) {
            diseniarHistoriaUp(g2);
        }
        if (this.IDIOMS_TYPE.equals("Historia") && this.IDIOMS_POSITION.equals("Below")) {
            diseniarHistoriaBelow(g2);
        }
    }

//------------------------------------------------------------------------------    
    public void diseniarMD(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getMaestro(), "Maestro" + this.CONTADOR_E)
                && !estadoExistenciaEntidad(this.ENTITY_CONTAINER.getDetalle(), "Detalle" + (this.CONTADOR_E + 1))) {
            if (this.ENTITY_M_NAME.equals("") && this.ENTITY_D_NAME.equals("")) {
                agregarEstructuraPorDefecto("MD");
                this.ENTITY_M_NAME = "Maestro" + this.CONTADOR_E;
                this.ENTITY_D_NAME = "Detalle" + (this.CONTADOR_E + 1);
            }
        }
        int positionEntityM = returnPosition(this.ENTITY_CONTAINER.getMaestro(), this.ENTITY_M_NAME);
        int positionEntityD = returnPosition(this.ENTITY_CONTAINER.getDetalle(), this.ENTITY_D_NAME);
        LinkedList listM = (LinkedList) this.ENTITY_CONTAINER.getMaestro().get(positionEntityM);
        LinkedList listD = (LinkedList) this.ENTITY_CONTAINER.getDetalle().get(positionEntityD);
        g2.setColor(Color.black);
        g2.setFont(this.FUENTE);
        //NOMBRE DE LA ENTIDAD MAESTRO:
        this.NAME_ENTITY_MAESTRO = (String) listM.get(0);
        g2.drawString(this.NAME_ENTITY_MAESTRO, this.X + 10, this.Y + 17);
        //NOMBRE DE LA ENTIDAD DETALLE:
        this.NAME_ENTITY_DETALLE = (String) listD.get(0);
        g2.drawString(this.NAME_ENTITY_DETALLE, this.X_E2 + 10, this.Y_E2 + 17);
        //ATRIBUTOS DE LA ENTIDAD MAESTRO:
        LinkedList attributesM = (LinkedList) listM.get(1);
        LinkedList listFKsD = (LinkedList) this.ENTITY_CONTAINER.getDLlaveSegundaria();
        LinkedList listFKsM = (LinkedList) this.ENTITY_CONTAINER.getMLlaveSegundaria();
        int jumpLineM = 35;
        for (int i = 0; i < attributesM.size(); i = i + 5) {
            String stateMPK = (String) attributesM.get(i + 2);
            if (stateMPK.equals("true") && stateMPK != null) {
                String nameAttribute = (String) attributesM.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineM);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineM);
                jumpLineM = jumpLineM + 18;
                LinkedList foreignKeys = new LinkedList();
                this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsD, this.NAME_ENTITY_MAESTRO, this.NAME_ENTITY_DETALLE);
                if (this.STATE_EXIST_FK == false) {
                    foreignKeys.add(this.NAME_ENTITY_MAESTRO);
                    foreignKeys.add(nameAttribute);
                    foreignKeys.add("Maestro");
                    foreignKeys.add(this.NAME_ENTITY_MAESTRO + "_" + nameAttribute);
                    foreignKeys.add(this.NAME_ENTITY_DETALLE);
                    this.ENTITY_CONTAINER.getDLlaveSegundaria().add(foreignKeys);
                }
            }
        }
        LinkedList fksMList = retornarListaFKsDeLaEntidad(listFKsM, this.NAME_ENTITY_MAESTRO);
        for (int i = 0; i < fksMList.size(); i++) {
            LinkedList newFK = new LinkedList();
            LinkedList fkList = (LinkedList) fksMList.get(i);
            String attribute = (String) fkList.get(3);
            newFK.add(this.NAME_ENTITY_MAESTRO);
            newFK.add(attribute);
            newFK.add("Maestro");
            newFK.add(this.NAME_ENTITY_MAESTRO + "_" + attribute);
            newFK.add(this.NAME_ENTITY_DETALLE);
            if(estadoExistenciaFK(listFKsD, this.NAME_ENTITY_MAESTRO, this.NAME_ENTITY_DETALLE, this.NAME_ENTITY_MAESTRO + "_" + attribute) == false){
                listFKsD.add(newFK);
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD MAESTRO..............................
//        for (int i = 0; i < listFKsM.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsM.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_MAESTRO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineM);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineM);
//                    jumpLineM = jumpLineM + 18;
//                }
//            }
//        }
        //ATRIBUTOS DE LA ENTIDAD DETALLE.......................................
        LinkedList attributesD = (LinkedList) listD.get(1);
        int jumpLineD = 35;
        for (int i = 0; i < attributesD.size(); i = i + 5) {
            String stateDPK = (String) attributesD.get(i + 2);
            if (stateDPK.equals("true") && stateDPK != null) {
                String nameAttribute = (String) attributesD.get(i);
                g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineD);
                g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineD);
                jumpLineD = jumpLineD + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD DETALLE..............................
//        for (int i = 0; i < listFKsD.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsD.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_DETALLE)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineD);
//                    g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineD);
//                    jumpLineD = jumpLineD + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD MAESTRO:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //GRAFICA DE LA ENTIDAD DETALLE:
        g2.drawRoundRect(this.X_E2, this.Y_E2, this.WIDTH_E2, this.HEIGHT_E2, 6, 6);
        g2.drawLine(this.X_E2, this.Y_E2 + 20, this.X_E2 + this.WIDTH_E2, this.Y_E2 + 20);
        //RELACION MAESTRO-DETALLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 30, this.X_E2 - 20, this.Y_E2 + 50);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 30);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    public void diseniarCC(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getClasificador(), "Clasificador" + this.CONTADOR_E)
                && !estadoExistenciaEntidad(this.ENTITY_CONTAINER.getClasificado(), "Clasificado" + (this.CONTADOR_E + 1))) {
            if (this.ENTITY_CLR_NAME.equals("") && this.ENTITY_CLO_NAME.equals("")) {
                agregarEstructuraPorDefecto("CC");
                this.ENTITY_CLR_NAME = "Clasificador" + this.CONTADOR_E;
                this.ENTITY_CLO_NAME = "Clasificado" + (this.CONTADOR_E + 1);
            }
        }
        int positionEntityCOR = returnPosition(this.ENTITY_CONTAINER.getClasificador(), this.ENTITY_CLR_NAME);
        int positionEntityCDO = returnPosition(this.ENTITY_CONTAINER.getClasificado(), this.ENTITY_CLO_NAME);
        LinkedList listCOR = (LinkedList) this.ENTITY_CONTAINER.getClasificador().get(positionEntityCOR);
        LinkedList listCDO = (LinkedList) this.ENTITY_CONTAINER.getClasificado().get(positionEntityCDO);
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD CLASIFICADOR:
        this.NAME_ENTITY_CLASIFICADOR = (String) listCOR.get(0);
        g2.drawString(this.NAME_ENTITY_CLASIFICADOR, this.X + 10, this.Y + 17);
        //NOMBRE DE LA ENTIDAD CLASIFICADO:
        this.NAME_ENTITY_CLASIFICADO = (String) listCDO.get(0);
        g2.drawString(this.NAME_ENTITY_CLASIFICADO, this.X_E2 + 10, this.Y_E2 + 17);
        //ATRIBUTOS DE LA ENTIDAD CLASIFICADOR:
        LinkedList attributesCOR = (LinkedList) listCOR.get(1);
        LinkedList listFKsCOR = (LinkedList) this.ENTITY_CONTAINER.getCORLlaveSegundaria();
        LinkedList listFKsCDO = (LinkedList) this.ENTITY_CONTAINER.getCDOLlaveSegundaria();
        int jumpLineCOR = 35;
        for (int i = 0; i < attributesCOR.size(); i = i + 5) {
            String stateCORPK = (String) attributesCOR.get(i + 2);
            if (stateCORPK.equals("true") && stateCORPK != null) {
                String nameAttribute = (String) attributesCOR.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineCOR);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCOR);
                jumpLineCOR = jumpLineCOR + 18;
                LinkedList foreignKeys = new LinkedList();
                this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsCDO, this.NAME_ENTITY_CLASIFICADOR, this.NAME_ENTITY_CLASIFICADO);
                if (this.STATE_EXIST_FK == false) {
                    foreignKeys.add(this.NAME_ENTITY_CLASIFICADOR);
                    foreignKeys.add(nameAttribute);
                    foreignKeys.add("Clasificador");
                    foreignKeys.add(this.NAME_ENTITY_CLASIFICADOR + "_" + nameAttribute);
                    foreignKeys.add(this.NAME_ENTITY_CLASIFICADO);
                    this.ENTITY_CONTAINER.getCDOLlaveSegundaria().add(foreignKeys);
                }
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD CLASIFICADOR.........................
//        for (int i = 0; i < listFKsCOR.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCOR.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(NAME_ENTITY_CLASIFICADOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineCOR);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCOR);
//                    jumpLineCOR = jumpLineCOR + 18;
//                }
//            }
//        }
        //ATRIBUTOS DE LA ENTIDAD CLASIFICADO...................................
        LinkedList attributesCDO = (LinkedList) listCDO.get(1);
        int jumpLineCDO = 35;
        for (int i = 0; i < attributesCDO.size(); i = i + 5) {
            String stateCDOPK = (String) attributesCDO.get(i + 2);
            if (stateCDOPK.equals("true") && stateCDOPK != null) {
                String nameAttribute = (String) attributesCDO.get(i);
                g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineCDO);
                g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineCDO);
                jumpLineCDO = jumpLineCDO + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD CLASIFICADO..........................
//        for (int i = 0; i < listFKsCDO.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCDO.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_CLASIFICADO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineCDO);
//                    g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineCDO);
//                    jumpLineCDO = jumpLineCDO + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD CLASIFICADOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //GRAFICA DE LA ENTIDAD CLASIFICADO:
        g2.drawRoundRect(this.X_E2, this.Y_E2, this.WIDTH_E2, this.HEIGHT_E2, 6, 6);
        g2.drawLine(this.X_E2, this.Y_E2 + 20, this.X_E2 + this.WIDTH_E2, this.Y_E2 + 20);
        //RELACION CLASIFICADOR-CLASIFICADO:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 30);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    public void diseniarC(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComposicion(), "Composicion" + this.CONTADOR_E)
                && !estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComponedor(), "Componedor" + (this.CONTADOR_E + 1))
                && !estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComponedor(), "Componedor" + (this.CONTADOR_E + 2))) {
            if (this.ENTITY_C_NAME.equals("") && this.ENTITY_C1_NAME.equals("") && this.ENTITY_C2_NAME.equals("")) {
                agregarEstructuraPorDefecto("C");
                this.ENTITY_C_NAME = "Composicion" + this.CONTADOR_E;
                this.ENTITY_C1_NAME = "Componedor" + (this.CONTADOR_E + 1);
                this.ENTITY_C2_NAME = "Componedor" + (this.CONTADOR_E + 2);
            }
        }
        int positionEntityC = returnPosition(this.ENTITY_CONTAINER.getComposicion(), this.ENTITY_C_NAME);
        int positionEntityCC1 = returnPosition(this.ENTITY_CONTAINER.getComponedor(), this.ENTITY_C1_NAME);
        int positionEntityCC2 = returnPosition(this.ENTITY_CONTAINER.getComponedor(), this.ENTITY_C2_NAME);
        LinkedList listC = (LinkedList) this.ENTITY_CONTAINER.getComposicion().get(positionEntityC);
        LinkedList listCC1 = (LinkedList) this.ENTITY_CONTAINER.getComponedor().get(positionEntityCC1);
        LinkedList listCC2 = (LinkedList) this.ENTITY_CONTAINER.getComponedor().get(positionEntityCC2);
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPOSICION:
        this.NAME_ENTITY_COMPOSICION = (String) listC.get(0);
        g2.drawString(this.NAME_ENTITY_COMPOSICION, this.X + 10, this.Y + 17);
        //NOMBRE DE LA ENTIDAD COMPONEDOR 1:
        this.NAME_ENTITY_COMPONEDOR = (String) listCC1.get(0);
        g2.drawString(this.NAME_ENTITY_COMPONEDOR, this.X_E2 + 10, this.Y_E2 + 17);
        //NOMBRE DE LA ENTIDAD COMPONEDOR 2:
        this.NAME_ENTITY_COMPONEDOR1 = (String) listCC2.get(0);
        g2.drawString(this.NAME_ENTITY_COMPONEDOR1, this.X_E3 + 10, this.Y_E3 + 17);
        //ATRIBUTOS DE LA ENTIDAD COMOPONEDOR 1:
        LinkedList attributesC1 = (LinkedList) listCC1.get(1);
        LinkedList listFKsC = (LinkedList) this.ENTITY_CONTAINER.getCLlaveSegundaria();
        LinkedList listFKsCC = (LinkedList) this.ENTITY_CONTAINER.getCCLlaveSegundaria();
        int jumpLineC1 = 35;
        for (int i = 0; i < attributesC1.size(); i = i + 5) {
            String stateC1PK = (String) attributesC1.get(i + 2);
            if (stateC1PK.equals("true") && stateC1PK != null) {
                String nameAttribute = (String) attributesC1.get(i);
                g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineC1);
                g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineC1);
                jumpLineC1 = jumpLineC1 + 18;
                LinkedList foreignKeys = new LinkedList();
                this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsC, this.NAME_ENTITY_COMPONEDOR, this.NAME_ENTITY_COMPOSICION);
                if (this.STATE_EXIST_FK == false) {
                    foreignKeys.add(this.NAME_ENTITY_COMPONEDOR);
                    foreignKeys.add(nameAttribute);
                    foreignKeys.add("Componedor");
                    foreignKeys.add(this.NAME_ENTITY_COMPONEDOR + "_" + nameAttribute);
                    foreignKeys.add(this.NAME_ENTITY_COMPOSICION);
                    this.ENTITY_CONTAINER.getCLlaveSegundaria().add(foreignKeys);
                }
            }
        }
        LinkedList fksC2List = retornarListaFKsDeLaEntidad(listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        for (int i = 0; i < fksC2List.size(); i++) {
            LinkedList newFK = new LinkedList();
            LinkedList fkList = (LinkedList) fksC2List.get(i);
            String attribute = (String) fkList.get(3);
            newFK.add(this.NAME_ENTITY_COMPONEDOR);
            newFK.add(attribute);
            newFK.add("Componedor");
            newFK.add(this.NAME_ENTITY_COMPONEDOR + "_" + attribute);
            newFK.add(this.NAME_ENTITY_COMPOSICION);
            if(estadoExistenciaFK(listFKsC, this.NAME_ENTITY_COMPONEDOR, this.NAME_ENTITY_COMPOSICION, this.NAME_ENTITY_COMPONEDOR + "_" + attribute) == false){
                listFKsC.add(newFK);
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD COMPONEDOR1..........................
//        for (int i = 0; i < listFKsCC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPONEDOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineC1);
//                    g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineC1);
//                    jumpLineC1 = jumpLineC1 + 18;
//                }
//            }
//        }
        //ATRIBUTOS DE LA ENTIDAD COMPONEDOR 2:
        LinkedList attributesC2 = (LinkedList) listCC2.get(1);
        int jumpLineC2 = 35;
        for (int i = 0; i < attributesC2.size(); i = i + 5) {
            String stateC2PK = (String) attributesC2.get(i + 2);
            if (stateC2PK.equals("true") && stateC2PK != null) {
                String nameAttribute = (String) attributesC2.get(i);
                g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineC2);
                g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineC2);
                jumpLineC2 = jumpLineC2 + 18;
                LinkedList foreignKeys = new LinkedList();
                this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsC, this.NAME_ENTITY_COMPONEDOR1, this.NAME_ENTITY_COMPOSICION);
                if (this.STATE_EXIST_FK == false) {
                    foreignKeys.add(this.NAME_ENTITY_COMPONEDOR1);
                    foreignKeys.add(nameAttribute);
                    foreignKeys.add("Componedor");
                    foreignKeys.add(this.NAME_ENTITY_COMPONEDOR1 + "_" + nameAttribute);
                    foreignKeys.add(this.NAME_ENTITY_COMPOSICION);
                    this.ENTITY_CONTAINER.getCLlaveSegundaria().add(foreignKeys);
                }
            }
        }
        LinkedList fksC3List = retornarListaFKsDeLaEntidad(listFKsCC, this.NAME_ENTITY_COMPONEDOR1);
        for (int i = 0; i < fksC3List.size(); i++) {
            LinkedList newFK = new LinkedList();
            LinkedList fkList = (LinkedList) fksC3List.get(i);
            String attribute = (String) fkList.get(3);
            newFK.add(this.NAME_ENTITY_COMPONEDOR1);
            newFK.add(attribute);
            newFK.add("Componedor");
            newFK.add(this.NAME_ENTITY_COMPONEDOR1 + "_" + attribute);
            newFK.add(this.NAME_ENTITY_COMPOSICION);
            if(estadoExistenciaFK(listFKsC, this.NAME_ENTITY_COMPONEDOR1, this.NAME_ENTITY_COMPOSICION, this.NAME_ENTITY_COMPONEDOR1 + "_" + attribute) == false){
                listFKsC.add(newFK);
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD COMPONEDOR2..........................
//        for (int i = 0; i < listFKsCC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPONEDOR1)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineC2);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineC2);
//                    jumpLineC2 = jumpLineC2 + 18;
//                }
//            }
//        }
        //ATRIBUTOS DE LA ENTIDAD COMPOSICION...................................
//        LinkedList attributesC = (LinkedList) listC.get(1);
//        int jumpLineC = 35;
//        for (int i = 0; i < attributesC.size(); i = i + 5) {
//            String stateDPK = (String) attributesC.get(i + 2);
//            if (stateDPK.equals("true") && stateDPK != null) {
//                String nameAttribute = (String) attributesC.get(i);
//                g2.drawString("#", this.X + 10, this.Y + jumpLineC);
//                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineC);
//                jumpLineC = jumpLineC + 18;
//            }
//        }
        //LLAVES SEGUNDARIAS DE LA ENTITAD COMPOSICION..........................
//        for (int i = 0; i < listFKsC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPOSICION)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineC);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineC);
//                    jumpLineC = jumpLineC + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD COMPOSICION:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //GRAFICA DE LA ENTIDAD COMPONEDOR 1:
        g2.drawRoundRect(this.X_E2, this.Y_E2, this.WIDTH_E2, this.HEIGHT_E2, 6, 6);
        g2.drawLine(this.X_E2, this.Y_E2 + 20, this.X_E2 + this.WIDTH_E2, this.Y_E2 + 20);
        //GRAFICA DE LA ENTIDAD COMPONEDOR 1:
        g2.drawRoundRect(this.X_E3, this.Y_E3, this.WIDTH_E3, this.HEIGHT_E3, 6, 6);
        g2.drawLine(this.X_E3, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 20);
        //RELACION COMPONEDOR 1-COMPOSICION:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X - 20, this.Y + 30, this.X - 20, this.Y + 50);
        g2.drawLine(this.X - 20, this.Y + 40, this.X, this.Y + 30);
        g2.drawLine(this.X - 20, this.Y + 40, this.X, this.Y + 40);
        g2.drawLine(this.X - 20, this.Y + 40, this.X, this.Y + 50);
        //RELACION COMPONEDOR 2-COMPOSICION:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X + this.WIDTH + 20, this.Y + 30, this.X + this.WIDTH + 20, this.Y + 50);
        g2.drawLine(this.X + this.WIDTH + 20, this.Y + 40, this.X + this.WIDTH, this.Y + 30);
        g2.drawLine(this.X + this.WIDTH + 20, this.Y + 40, +this.X + this.WIDTH, this.Y + 40);
        g2.drawLine(this.X + this.WIDTH + 20, this.Y + 40, +this.X + this.WIDTH, this.Y + 50);
        //RELACION COMPONEDOR 1-COMPOSICION:
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X_E2, this.Y_E2 + 40, this.X + this.WIDTH + 20, this.Y + 40);
        g2.setStroke(restaurarStroke());
        //RELACION COMPONEDOR 2-CAOMPOSICION:
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X_E3 + this.WIDTH_E3, this.Y_E3 + 40, this.X - 20, this.Y + 40);
        g2.setStroke(restaurarStroke());
    }

    public void diseniarISA(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getGeneral(), "General" + this.CONTADOR_E)
                && !estadoExistenciaEntidad(this.ENTITY_CONTAINER.getIsa(), "Isa" + (this.CONTADOR_E + 1))) {
            if (this.ENTITY_G_NAME.equals("") && this.ENTITY_I_NAME.equals("")) {
                agregarEstructuraPorDefecto("ISA");
                this.ENTITY_G_NAME = "General" + this.CONTADOR_E;
                this.ENTITY_I_NAME = "Isa" + (this.CONTADOR_E + 1);
            }
        }
        int positionEntityG = returnPosition(this.ENTITY_CONTAINER.getGeneral(), this.ENTITY_G_NAME);
        int positionEntityI = returnPosition(this.ENTITY_CONTAINER.getIsa(), this.ENTITY_I_NAME);
        LinkedList listG = (LinkedList) this.ENTITY_CONTAINER.getGeneral().get(positionEntityG);
        LinkedList listI = (LinkedList) this.ENTITY_CONTAINER.getIsa().get(positionEntityI);
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD GENERAL:
        this.NAME_ENTITY_GENERAL = (String) listG.get(0);
        g2.drawString(this.NAME_ENTITY_GENERAL, this.X + 10, this.Y + 17);
        //NOMBRE DE LA ENTIDAD COMPONENTE IS_A:
        this.NAME_ENTITY_ISA = (String) listI.get(0);
        g2.drawString(this.NAME_ENTITY_ISA, this.X_E2 + 10, this.Y_E2 + 17);
        //ATRIBUTOS DE LA ENTIDAD GENERAL:
        LinkedList attributesG = (LinkedList) listG.get(1);
        LinkedList listFKsG = (LinkedList) this.ENTITY_CONTAINER.getGLlaveSegundaria();
        LinkedList listFKsI = (LinkedList) this.ENTITY_CONTAINER.getILlaveSegundaria();
        int jumpLineG = 35;
        for (int i = 0; i < attributesG.size(); i = i + 5) {
            String stateGPK = (String) attributesG.get(i + 2);
            if (stateGPK.equals("true") && stateGPK != null) {
                String nameAttribute = (String) attributesG.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineG);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineG);
                jumpLineG = jumpLineG + 18;
                LinkedList foreignKeys = new LinkedList();
                this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsI, this.NAME_ENTITY_GENERAL, this.NAME_ENTITY_ISA);
                if (this.STATE_EXIST_FK == false) {
                    foreignKeys.add(this.NAME_ENTITY_GENERAL);
                    foreignKeys.add(nameAttribute);
                    foreignKeys.add("General");
                    foreignKeys.add(this.NAME_ENTITY_GENERAL + "_" + nameAttribute);
                    foreignKeys.add(this.NAME_ENTITY_ISA);
                    this.ENTITY_CONTAINER.getILlaveSegundaria().add(foreignKeys);
                }
            }
        }
        LinkedList fksGList = retornarListaFKsDeLaEntidad(listFKsG, this.NAME_ENTITY_GENERAL);
        for (int i = 0; i < fksGList.size(); i++) {
            LinkedList newFK = new LinkedList();
            LinkedList fkList = (LinkedList) fksGList.get(i);
            String attribute = (String) fkList.get(3);
            newFK.add(this.NAME_ENTITY_GENERAL);
            newFK.add(attribute);
            newFK.add("General");
            newFK.add(this.NAME_ENTITY_GENERAL + "_" + attribute);
            newFK.add(this.NAME_ENTITY_ISA);
            if(estadoExistenciaFK(listFKsI, this.NAME_ENTITY_GENERAL, this.NAME_ENTITY_ISA, this.NAME_ENTITY_GENERAL + "_" + attribute) == false){
                listFKsI.add(newFK);
            }
        }
        //LLAVES SEGUNDARIA DE LA ENTITAD GENERAL...............................
//        for (int i = 0; i < listFKsG.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsG.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_GENERAL)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineG);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineG);
//                    jumpLineG = jumpLineG + 18;
//                }
//            }
//        }
        //ATRIBUTOS DE LA ENTIDAD IS_A..........................................
//        LinkedList attributesI = (LinkedList) listI.get(1);
//        int jumpLineI = 35;
//        for (int i = 0; i < attributesI.size(); i = i + 5) {
//            String stateIPK = (String) attributesI.get(i + 2);
//            if (stateIPK.equals("true") && stateIPK != null) {
//                String nameAttribute = (String) attributesI.get(i);
//                g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineI);
//                g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineI);
//                jumpLineI = jumpLineI + 18;
//            }
//        }
        //LLAVES SEGUNDARIA DE LA ENTITAD ISA...................................
//        for (int i = 0; i < listFKsI.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsI.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_ISA)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineI);
//                    g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineI);
//                    jumpLineI = jumpLineI + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD GENERAL:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //GRAFICA DE LA ENTIDAD IS_A:
        g2.drawRoundRect(this.X_E2, this.Y_E2, this.WIDTH_E2, this.HEIGHT_E2, 6, 6);
        g2.drawLine(this.X_E2, this.Y_E2 + 20, this.X_E2 + this.WIDTH_E2, this.Y_E2 + 20);
        //RELACION GENERAL-IS_A:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 30, this.X_E2 - 20, this.Y_E2 + 50);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    public void diseniarRS(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getRSimple(), "RSimple" + this.CONTADOR_E)) {
            if (this.ENTITY_RS_NAME.equals("")) {
                agregarEstructuraPorDefecto("RS");
                this.ENTITY_RS_NAME = "RSimple" + this.CONTADOR_E;
            }
        }
        int positionEntityRS = returnPosition(this.ENTITY_CONTAINER.getRSimple(), this.ENTITY_RS_NAME);
        LinkedList listRS = (LinkedList) this.ENTITY_CONTAINER.getRSimple().get(positionEntityRS);
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD REFLEXIVO SIMPLE:
        this.NAME_ENTITY_RSIMPLE = (String) listRS.get(0);
        g2.drawString(this.NAME_ENTITY_RSIMPLE, this.X + 10, this.Y + 17);
        //ATRIBUTOS DE LA ENTIDAD REFLEXIVO SIMPLE:
        LinkedList attributesRS = (LinkedList) listRS.get(1);
        LinkedList listFKsRS = (LinkedList) this.ENTITY_CONTAINER.getRSLlaveSegundaria();
        int jumpLineRS = 35;
        for (int i = 0; i < attributesRS.size(); i = i + 5) {
            String stateRSPK = (String) attributesRS.get(i + 2);
            if (stateRSPK.equals("true") && stateRSPK != null) {
                String nameAttribute = (String) attributesRS.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineRS);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineRS);
                jumpLineRS = jumpLineRS + 18;
                LinkedList foreignKeys = new LinkedList();
                this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsRS, this.NAME_ENTITY_RSIMPLE, this.NAME_ENTITY_RSIMPLE);
                if (this.STATE_EXIST_FK == false) {
                    foreignKeys.add(this.NAME_ENTITY_RSIMPLE);
                    foreignKeys.add(nameAttribute);
                    foreignKeys.add("RSimple");
                    foreignKeys.add(this.NAME_ENTITY_RSIMPLE + "_" + nameAttribute);
                    foreignKeys.add(this.NAME_ENTITY_RSIMPLE);
                    this.ENTITY_CONTAINER.getRSLlaveSegundaria().add(foreignKeys);
                }
            }
        }
        //LLAVES SEGUNDARIA DE LA ENTITAD RSIMPLE...............................
//        for (int i = 0; i < listFKsRS.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsRS.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_RSIMPLE)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineRS);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineRS);
//                    jumpLineRS = jumpLineRS + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD REFLEXIVO SIMPLE:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION REFLEXIVO SIMPLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X + 40, this.Y - 20, this.X + 30, this.Y);
        g2.drawLine(this.X + 40, this.Y - 20, this.X + 40, this.Y);
        g2.drawLine(this.X + 40, this.Y - 20, this.X + 50, this.Y);
        //LINEA DE LA RELACION DE INICIAL:
        g2.drawLine(this.X + this.WIDTH + 40, this.Y - 40, this.X + 40, this.Y - 40);
        g2.drawLine(this.X + 40, this.Y - 40, this.X + 40, this.Y - 20);
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X + this.WIDTH + 40, this.Y + 40);
        g2.drawLine(this.X + this.WIDTH + 40, this.Y + 40, this.X + this.WIDTH + 40, this.Y - 40);
        g2.setStroke(restaurarStroke());
    }

    public void diseniarRH(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getActor(), "Actor" + this.CONTADOR_E)
                && !estadoExistenciaEntidad(this.ENTITY_CONTAINER.getHistoria(), "Historia" + (this.CONTADOR_E + 1))) {
            if (this.ENTITY_A_NAME.equals("") && this.ENTITY_H_NAME.equals("")) {
                agregarEstructuraPorDefecto("RH");
                this.ENTITY_A_NAME = "Actor" + this.CONTADOR_E;
                this.ENTITY_H_NAME = "Historia" + (this.CONTADOR_E + 1);
            }
        }
        int positionEntityA = returnPosition(this.ENTITY_CONTAINER.getActor(), this.ENTITY_A_NAME);
        int positionEntityH = returnPosition(this.ENTITY_CONTAINER.getHistoria(), this.ENTITY_H_NAME);
        LinkedList listA = (LinkedList) this.ENTITY_CONTAINER.getActor().get(positionEntityA);
        LinkedList listH = (LinkedList) this.ENTITY_CONTAINER.getHistoria().get(positionEntityH);
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD ACTOR:
        this.NAME_ENTITY_ACTOR = (String) listA.get(0);
        g2.drawString(this.NAME_ENTITY_ACTOR, this.X + 10, this.Y + 17);
        //NOMBRE DE LA ENTIDAD HISTORIA:
        this.NAME_ENTITY_HISTORIA = (String) listH.get(0);
        g2.drawString(this.NAME_ENTITY_HISTORIA, this.X_E4 + 10, this.Y_E4 + 17);
        //ATRIBUTOS DE LA ENTIDAD ACTOR:
        LinkedList attributesA = (LinkedList) listA.get(1);
        LinkedList listFKsA = (LinkedList) this.ENTITY_CONTAINER.getALlaveSegundaria();
        LinkedList listFKsH = (LinkedList) this.ENTITY_CONTAINER.getHLlaveSegundaria();
        int jumpLineA = 35;
        for (int i = 0; i < attributesA.size(); i = i + 5) {
            String stateAPK = (String) attributesA.get(i + 2);
            if (stateAPK.equals("true") && stateAPK != null) {
                String nameAttribute = (String) attributesA.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineA);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineA);
                jumpLineA = jumpLineA + 18;
                LinkedList foreignKeys1 = new LinkedList();
                LinkedList foreignKeys2 = new LinkedList();
                this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsH, this.NAME_ENTITY_ACTOR, this.NAME_ENTITY_HISTORIA);
                if (this.STATE_EXIST_FK == false) {
                    foreignKeys1.add(this.NAME_ENTITY_ACTOR);
                    foreignKeys1.add(nameAttribute);
                    foreignKeys1.add("Actor");
                    foreignKeys1.add(this.NAME_ENTITY_ACTOR + "_" + nameAttribute);
                    foreignKeys1.add(this.NAME_ENTITY_HISTORIA);
                    this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys1);
                    foreignKeys2.add(this.NAME_ENTITY_ACTOR);
                    foreignKeys2.add(nameAttribute + "_2");
                    foreignKeys2.add("Actor");
                    foreignKeys2.add(this.NAME_ENTITY_ACTOR + "_" + nameAttribute + "_2");
                    foreignKeys2.add(this.NAME_ENTITY_HISTORIA);
                    this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys2);
                }
            }
        }
        LinkedList fksAList = retornarListaFKsDeLaEntidad(listFKsA, this.NAME_ENTITY_ACTOR);
        for (int i = 0; i < fksAList.size(); i++) {
            LinkedList newFK = new LinkedList();
            LinkedList fkList = (LinkedList) fksAList.get(i);
            String attribute = (String) fkList.get(3);
            newFK.add(this.NAME_ENTITY_ACTOR);
            newFK.add(attribute);
            newFK.add("Actor");
            newFK.add(this.NAME_ENTITY_ACTOR + "_" + attribute);
            newFK.add(this.NAME_ENTITY_HISTORIA);
            if(estadoExistenciaFK(listFKsH, this.NAME_ENTITY_ACTOR, this.NAME_ENTITY_HISTORIA, this.NAME_ENTITY_ACTOR + "_" + attribute) == false){
                listFKsH.add(newFK);
            }
        }
        //LLAVES SEGUNDARIA DE LA ENTITAD ACTOR.................................
//        for (int i = 0; i < listFKsA.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsA.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_ACTOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineA);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineA);
//                    jumpLineA = jumpLineA + 18;
//                }
//            }
//        }
        //ATRIBUTOS DE LA ENTIDAD HISTORIA......................................
        LinkedList attributesH = (LinkedList) listH.get(1);
        int jumpLineH = 35;
        for (int i = 0; i < attributesH.size(); i = i + 5) {
            String stateDPK = (String) attributesH.get(i + 2);
            if (stateDPK.equals("true") && stateDPK != null) {
                String nameAttribute = (String) attributesH.get(i);
                g2.drawString("#", this.X_E4 + 10, this.Y_E4 + jumpLineH);
                g2.drawString(nameAttribute, this.X_E4 + 20, this.Y_E4 + jumpLineH);
                jumpLineH = jumpLineH + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD HISTORA..............................
//        for (int i = 0; i < listFKsH.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsH.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_HISTORIA)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E4 + 10, this.Y_E4 + jumpLineH);
//                    g2.drawString(nameAttribute, this.X_E4 + 20, this.Y_E4 + jumpLineH);
//                    jumpLineH = jumpLineH + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD SEMEJANTE:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //GRAFICA DE LA TABLA HISTORIA:
        g2.drawRoundRect(this.X_E4, this.Y_E4, this.WIDTH_E4, this.HEIGHT_E4, 6, 6);
        g2.drawLine(this.X_E4, this.Y_E4 + 20, this.X_E4 + this.WIDTH_E4, this.Y_E4 + 20);
        //RELACION SEMEJANTE-HISTORIA:
        //FLECHA DE LA PRIMERA RELACION FINAL:
        g2.drawLine(this.X_E4 + 10, this.Y_E4 - 20, this.X_E4 + 30, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 20, this.Y_E4 - 20, this.X_E4 + 20 - 10, this.Y_E4);
        g2.drawLine(this.X_E4 + 20, this.Y_E4 - 20, this.X_E4 + 20, this.Y_E4);
        g2.drawLine(this.X_E4 + 20, this.Y_E4 - 20, this.X_E4 + 20 + 10, this.Y_E4);
        //FLECHA DE LA SEGUNDA RELACION FINAL:
        g2.drawLine(this.X_E4 + 70, this.Y_E4 - 20, this.X_E4 + 90, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 80, this.Y_E4 - 20, this.X_E4 + 80 - 10, this.Y_E4);
        g2.drawLine(this.X_E4 + 80, this.Y_E4 - 20, this.X_E4 + 80, this.Y_E4);
        g2.drawLine(this.X_E4 + 80, this.Y_E4 - 20, this.X_E4 + 80 + 10, this.Y_E4);
        g2.setStroke(flechaRelacion());
        //LINEA DE LA PRIMERA RELACION DE INICIAL:
        g2.drawLine(this.X + 20, this.Y + 80, this.X_E4 + 20, this.Y_E4 - 20);
        //LINEA DE LA SEGUNDA RELACION DE INICIAL:
        g2.drawLine(this.X + 80, this.Y + 80, this.X_E4 + 80, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }

    public void diseniarB(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getBasico(), "Basico" + this.CONTADOR_E)) {
            if (this.ENTITY_B_NAME.equals("")) {
                agregarEstructuraPorDefecto("B");
                this.ENTITY_B_NAME = "Basico" + this.CONTADOR_E;
            }
        }
        int positionEntityB = returnPosition(this.ENTITY_CONTAINER.getBasico(), this.ENTITY_B_NAME);
        LinkedList listB = (LinkedList) this.ENTITY_CONTAINER.getBasico().get(positionEntityB);
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD BASICO:
        this.NAME_ENTITY_BASICO = (String) listB.get(0);
        g2.drawString(this.NAME_ENTITY_BASICO, this.X + 10, this.Y + 17);
        //ATRIBUTOS DE LA ENTIDAD BASICO:
        LinkedList attributesB = (LinkedList) listB.get(1);
        LinkedList listFKsB = (LinkedList) this.ENTITY_CONTAINER.getBLlaveSegundaria();
        int jumpLineB = 35;
        for (int i = 0; i < attributesB.size(); i = i + 5) {
            String stateBPK = (String) attributesB.get(i + 2);
            if (stateBPK.equals("true") && stateBPK != null) {
                String nameAttribute = (String) attributesB.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineB);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineB);
                jumpLineB = jumpLineB + 18;
            }
        }
//        //LLAVES SEGUNDARIA DE LA ENTITAD BASICO................................
//        for (int i = 0; i < listFKsB.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsB.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_BASICO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineB);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineB);
//                    jumpLineB = jumpLineB + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD BASICO:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
    }

//------------------------------------------------------------------------------
    private void diseniarMaestroLeft(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getMaestro(), "Maestro_" + this.CONTADOR_E)) {
            if (this.ENTITY_M_NAME.equals("")) {
                agregarEstructuraPorDefecto("Maestro");
                this.ENTITY_M_NAME = "Maestro_" + this.CONTADOR_E;
            }
        }
        int positionEntityM = returnPosition(this.ENTITY_CONTAINER.getMaestro(), this.ENTITY_M_NAME);
        LinkedList listM = (LinkedList) this.ENTITY_CONTAINER.getMaestro().get(positionEntityM);
        LinkedList listFKsM = (LinkedList) this.ENTITY_CONTAINER.getMLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(this.FUENTE);
        //NOMBRE DE LA ENTIDAD MAESTRO:
        this.NAME_ENTITY_MAESTRO = (String) listM.get(0);
        g2.drawString(this.NAME_ENTITY_MAESTRO, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD MAESTRO...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_MAESTRO, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_MAESTRO, "Maestro");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_MAESTRO);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Maestro");
                foreignKeys.add(this.NAME_ENTITY_MAESTRO + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        //ATRIBUTOS DE LA ENTIDAD MAESTRO:
        LinkedList attributesM = (LinkedList) listM.get(1);
        int jumpLineM = 35;
        for (int i = 0; i < attributesM.size(); i = i + 5) {
            String stateMPK = (String) attributesM.get(i + 2);
            if (stateMPK.equals("true") && stateMPK != null) {
                String nameAttribute = (String) attributesM.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineM);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineM);
                jumpLineM = jumpLineM + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD MAESTRO..............................
//        for (int i = 0; i < listFKsM.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsM.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_MAESTRO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineM);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineM);
//                    jumpLineM = jumpLineM + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD MAESTRO:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION MAESTRO-DETALLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 30, this.X_E2 - 20, this.Y_E2 + 50);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 30);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarMaestroRight(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getMaestro(), "Maestro_" + this.CONTADOR_E)) {
            if (this.ENTITY_M_NAME.equals("")) {
                agregarEstructuraPorDefecto("Maestro");
                this.ENTITY_M_NAME = "Maestro_" + this.CONTADOR_E;
            }
        }
        int positionEntityM = returnPosition(this.ENTITY_CONTAINER.getMaestro(), this.ENTITY_M_NAME);
        LinkedList listM = (LinkedList) this.ENTITY_CONTAINER.getMaestro().get(positionEntityM);
        LinkedList listFKsM = (LinkedList) this.ENTITY_CONTAINER.getMLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(this.FUENTE);
        //NOMBRE DE LA ENTIDAD MAESTRO:
        this.NAME_ENTITY_MAESTRO = (String) listM.get(0);
        g2.drawString(this.NAME_ENTITY_MAESTRO, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD MAESTRO...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_MAESTRO, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_MAESTRO, "Maestro");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_MAESTRO);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Maestro");
                foreignKeys.add(this.NAME_ENTITY_MAESTRO + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        //ATRIBUTOS DE LA ENTIDAD MAESTRO:
        LinkedList attributesM = (LinkedList) listM.get(1);
        int jumpLineM = 35;
        for (int i = 0; i < attributesM.size(); i = i + 5) {
            String stateMPK = (String) attributesM.get(i + 2);
            if (stateMPK.equals("true") && stateMPK != null) {
                String nameAttribute = (String) attributesM.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineM);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineM);
                jumpLineM = jumpLineM + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD MAESTRO..............................
//        for (int i = 0; i < listFKsM.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsM.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_MAESTRO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineM);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineM);
//                    jumpLineM = jumpLineM + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD MAESTRO:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION MAESTRO-DETALLE:
        //FLECHA DE LA RELACION FINAL:        
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 30, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 50);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 30);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 40);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X, this.Y + 40, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarMaestroUp(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getMaestro(), "Maestro_" + this.CONTADOR_E)) {
            if (this.ENTITY_M_NAME.equals("")) {
                agregarEstructuraPorDefecto("Maestro");
                this.ENTITY_M_NAME = "Maestro_" + this.CONTADOR_E;
            }
        }
        int positionEntityM = returnPosition(this.ENTITY_CONTAINER.getMaestro(), this.ENTITY_M_NAME);
        LinkedList listM = (LinkedList) this.ENTITY_CONTAINER.getMaestro().get(positionEntityM);
        LinkedList listFKsM = (LinkedList) this.ENTITY_CONTAINER.getMLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(this.FUENTE);
        //NOMBRE DE LA ENTIDAD MAESTRO:
        this.NAME_ENTITY_MAESTRO = (String) listM.get(0);
        g2.drawString(this.NAME_ENTITY_MAESTRO, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD MAESTRO...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_MAESTRO, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_MAESTRO, "Maestro");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_MAESTRO);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Maestro");
                foreignKeys.add(this.NAME_ENTITY_MAESTRO + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        //ATRIBUTOS DE LA ENTIDAD MAESTRO:
        LinkedList attributesM = (LinkedList) listM.get(1);
        int jumpLineM = 35;
        for (int i = 0; i < attributesM.size(); i = i + 5) {
            String stateMPK = (String) attributesM.get(i + 2);
            if (stateMPK.equals("true") && stateMPK != null) {
                String nameAttribute = (String) attributesM.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineM);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineM);
                jumpLineM = jumpLineM + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD MAESTRO..............................
//        for (int i = 0; i < listFKsM.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsM.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_MAESTRO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineM);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineM);
//                    jumpLineM = jumpLineM + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD MAESTRO:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION MAESTRO-DETALLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E4 + 40, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 40, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 50, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y + this.HEIGHT, this.X_E4 + 50, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarMaestroBelow(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getMaestro(), "Maestro_" + this.CONTADOR_E)) {
            if (this.ENTITY_M_NAME.equals("")) {
                agregarEstructuraPorDefecto("Maestro");
                this.ENTITY_M_NAME = "Maestro_" + this.CONTADOR_E;
            }
        }
        int positionEntityM = returnPosition(this.ENTITY_CONTAINER.getMaestro(), this.ENTITY_M_NAME);
        LinkedList listM = (LinkedList) this.ENTITY_CONTAINER.getMaestro().get(positionEntityM);
        LinkedList listFKsM = (LinkedList) this.ENTITY_CONTAINER.getMLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(this.FUENTE);
        //NOMBRE DE LA ENTIDAD MAESTRO:
        this.NAME_ENTITY_MAESTRO = (String) listM.get(0);
        g2.drawString(this.NAME_ENTITY_MAESTRO, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD MAESTRO...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_MAESTRO, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_MAESTRO, "Maestro");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_MAESTRO);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Maestro");
                foreignKeys.add(this.NAME_ENTITY_MAESTRO + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsM, this.NAME_ENTITY_MAESTRO);
        }
        //ATRIBUTOS DE LA ENTIDAD MAESTRO:
        LinkedList attributesM = (LinkedList) listM.get(1);
        int jumpLineM = 35;
        for (int i = 0; i < attributesM.size(); i = i + 5) {
            String stateMPK = (String) attributesM.get(i + 2);
            if (stateMPK.equals("true") && stateMPK != null) {
                String nameAttribute = (String) attributesM.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineM);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineM);
                jumpLineM = jumpLineM + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD MAESTRO..............................
//        for (int i = 0; i < listFKsM.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsM.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_MAESTRO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineM);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineM);
//                    jumpLineM = jumpLineM + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD MAESTRO:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION MAESTRO-DETALLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.setStroke(restaurarStroke());
    }
//------------------------------------------------------------------------------    

    private void diseniarDetalleLeft(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getDetalle(), "Detalle_" + this.CONTADOR_E)) {
            if (this.ENTITY_D_NAME.equals("")) {
                agregarEstructuraPorDefecto("Detalle");
                this.ENTITY_D_NAME = "Detalle_" + this.CONTADOR_E;
            }
        }
        int positionEntityD = returnPosition(this.ENTITY_CONTAINER.getDetalle(), this.ENTITY_D_NAME);
        LinkedList listD = (LinkedList) this.ENTITY_CONTAINER.getDetalle().get(positionEntityD);
        LinkedList listFKsD = (LinkedList) this.ENTITY_CONTAINER.getDLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(this.FUENTE);
        //NOMBRE DE LA ENTIDAD DETALLE:
        this.NAME_ENTITY_DETALLE = (String) listD.get(0);
        g2.drawString(this.NAME_ENTITY_DETALLE, this.X_E3 + 10, this.Y_E3 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD DETALLE...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsD, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_DETALLE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_DETALLE);
                this.ENTITY_CONTAINER.getDLlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        //ATRIBUTOS DE LA ENTIDAD DETALLE:
        LinkedList attributesD = (LinkedList) listD.get(1);
        int jumpLineD = 35;
        for (int i = 0; i < attributesD.size(); i = i + 5) {
            String stateMPK = (String) attributesD.get(i + 2);
            if (stateMPK.equals("true") && stateMPK != null) {
                String nameAttribute = (String) attributesD.get(i);
                g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineD);
                g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineD);
                jumpLineD = jumpLineD + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD DETALLE..............................
//        for (int i = 0; i < listFKsD.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsD.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_DETALLE)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineD);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineD);
//                    jumpLineD = jumpLineD + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD DETALLE:
        g2.drawRoundRect(this.X_E3, this.Y_E3, this.WIDTH_E3, this.HEIGHT_E3, 6, 6);
        g2.drawLine(this.X_E3, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 20);
        //RELACION MAESTRO-DETALLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 30, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 50);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 30);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 40);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X, this.Y + 40, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarDetalleRight(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getDetalle(), "Detalle_" + this.CONTADOR_E)) {
            if (this.ENTITY_D_NAME.equals("")) {
                agregarEstructuraPorDefecto("Detalle");
                this.ENTITY_D_NAME = "Detalle_" + this.CONTADOR_E;
            }
        }
        int positionEntityD = returnPosition(this.ENTITY_CONTAINER.getDetalle(), this.ENTITY_D_NAME);
        LinkedList listD = (LinkedList) this.ENTITY_CONTAINER.getDetalle().get(positionEntityD);
        LinkedList listFKsD = (LinkedList) this.ENTITY_CONTAINER.getDLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(this.FUENTE);
        //NOMBRE DE LA ENTIDAD DETALLE:
        this.NAME_ENTITY_DETALLE = (String) listD.get(0);
        g2.drawString(this.NAME_ENTITY_DETALLE, this.X_E2 + 10, this.Y_E2 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD DETALLE...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsD, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_DETALLE);
        LinkedList myListEntityAttribute = retornarLista(ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        String nameAttributePK = (String) myListEntityAttribute.get(0);
        if (this.STATE_EXIST_FK == false) {
            foreignKeys.add(this.ENTITY_RELATION_NAME);
            foreignKeys.add(nameAttributePK);
            foreignKeys.add(this.ENTITY_TIPE);
            foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
            foreignKeys.add(this.NAME_ENTITY_DETALLE);
            this.ENTITY_CONTAINER.getDLlaveSegundaria().add(foreignKeys);
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        //ATRIBUTOS DE LA ENTIDAD DETALLE:
        LinkedList attributesD = (LinkedList) listD.get(1);
        int jumpLineD = 35;
        for (int i = 0; i < attributesD.size(); i = i + 5) {
            String stateMPK = (String) attributesD.get(i + 2);
            if (stateMPK.equals("true") && stateMPK != null) {
                String nameAttribute = (String) attributesD.get(i);
                g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineD);
                g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineD);
                jumpLineD = jumpLineD + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD DETALLE..............................
//        for (int i = 0; i < listFKsD.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsD.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_DETALLE)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineD);
//                    g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineD);
//                    jumpLineD = jumpLineD + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD DETALLE:
        g2.drawRoundRect(this.X_E2, this.Y_E2, this.WIDTH_E2, this.HEIGHT_E2, 6, 6);
        g2.drawLine(this.X_E2, this.Y_E2 + 20, this.X_E2 + this.WIDTH_E2, this.Y_E2 + 20);
        //RELACION MAESTRO-DETALLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 30, this.X_E2 - 20, this.Y_E2 + 50);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 30);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarDetalleUp(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getDetalle(), "Detalle_" + this.CONTADOR_E)) {
            if (this.ENTITY_D_NAME.equals("")) {
                agregarEstructuraPorDefecto("Detalle");
                this.ENTITY_D_NAME = "Detalle_" + this.CONTADOR_E;
            }
        }
        int positionEntityD = returnPosition(this.ENTITY_CONTAINER.getDetalle(), this.ENTITY_D_NAME);
        LinkedList listD = (LinkedList) this.ENTITY_CONTAINER.getDetalle().get(positionEntityD);
        LinkedList listFKsD = (LinkedList) this.ENTITY_CONTAINER.getDLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(this.FUENTE);
        //NOMBRE DE LA ENTIDAD DETALLE:
        this.NAME_ENTITY_DETALLE = (String) listD.get(0);
        g2.drawString(this.NAME_ENTITY_DETALLE, this.X_E5 + 10, this.Y_E5 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD DETALLE...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsD, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_DETALLE);
        LinkedList myListEntityAttribute = retornarLista(ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        String nameAttributePK = (String) myListEntityAttribute.get(0);
        if (this.STATE_EXIST_FK == false) {
            foreignKeys.add(this.ENTITY_RELATION_NAME);
            foreignKeys.add(nameAttributePK);
            foreignKeys.add(this.ENTITY_TIPE);
            foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
            foreignKeys.add(this.NAME_ENTITY_DETALLE);
            this.ENTITY_CONTAINER.getDLlaveSegundaria().add(foreignKeys);
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        //ATRIBUTOS DE LA ENTIDAD DETALLE:
        LinkedList attributesD = (LinkedList) listD.get(1);
        int jumpLineD = 35;
        for (int i = 0; i < attributesD.size(); i = i + 5) {
            String stateMPK = (String) attributesD.get(i + 2);
            if (stateMPK.equals("true") && stateMPK != null) {
                String nameAttribute = (String) attributesD.get(i);
                g2.drawString("#", this.X_E5 + 10, this.Y_E5 + jumpLineD);
                g2.drawString(nameAttribute, this.X_E5 + 20, this.Y_E5 + jumpLineD);
                jumpLineD = jumpLineD + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD DETALLE..............................
//        for (int i = 0; i < listFKsD.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsD.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_DETALLE)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E5 + 10, this.Y_E5 + jumpLineD);
//                    g2.drawString(nameAttribute, this.X_E5 + 20, this.Y_E5 + jumpLineD);
//                    jumpLineD = jumpLineD + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD DETALLE:
        g2.drawRoundRect(this.X_E5, this.Y_E5, this.WIDTH_E5, this.HEIGHT_E5, 6, 6);
        g2.drawLine(this.X_E5, this.Y_E5 + 20, this.X_E5 + this.WIDTH_E5, this.Y_E5 + 20);
        //RELACION MAESTRO-DETALLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarDetalleBelow(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getDetalle(), "Detalle_" + this.CONTADOR_E)) {
            if (this.ENTITY_D_NAME.equals("")) {
                agregarEstructuraPorDefecto("Detalle");
                this.ENTITY_D_NAME = "Detalle_" + this.CONTADOR_E;
            }
        }
        int positionEntityD = returnPosition(this.ENTITY_CONTAINER.getDetalle(), this.ENTITY_D_NAME);
        LinkedList listD = (LinkedList) this.ENTITY_CONTAINER.getDetalle().get(positionEntityD);
        LinkedList listFKsD = (LinkedList) this.ENTITY_CONTAINER.getDLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(this.FUENTE);
        //NOMBRE DE LA ENTIDAD DETALLE:
        this.NAME_ENTITY_DETALLE = (String) listD.get(0);
        g2.drawString(this.NAME_ENTITY_DETALLE, this.X_E4 + 10, this.Y_E4 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD DETALLE...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsD, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_DETALLE);
        LinkedList myListEntityAttribute = retornarLista(ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        String nameAttributePK = (String) myListEntityAttribute.get(0);
        if (this.STATE_EXIST_FK == false) {
            foreignKeys.add(this.ENTITY_RELATION_NAME);
            foreignKeys.add(nameAttributePK);
            foreignKeys.add(this.ENTITY_TIPE);
            foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
            foreignKeys.add(this.NAME_ENTITY_DETALLE);
            this.ENTITY_CONTAINER.getDLlaveSegundaria().add(foreignKeys);
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsD, this.NAME_ENTITY_DETALLE);
        }
        //ATRIBUTOS DE LA ENTIDAD DETALLE:
        LinkedList attributesD = (LinkedList) listD.get(1);
        int jumpLineD = 35;
        for (int i = 0; i < attributesD.size(); i = i + 5) {
            String stateMPK = (String) attributesD.get(i + 2);
            if (stateMPK.equals("true") && stateMPK != null) {
                String nameAttribute = (String) attributesD.get(i);
                g2.drawString("#", this.X_E4 + 10, this.Y_E4 + jumpLineD);
                g2.drawString(nameAttribute, this.X_E4 + 20, this.Y_E4 + jumpLineD);
                jumpLineD = jumpLineD + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD DETALLE..............................
//        for (int i = 0; i < listFKsD.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsD.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_DETALLE)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E4 + 10, this.Y_E4 + jumpLineD);
//                    g2.drawString(nameAttribute, this.X_E4 + 20, this.Y_E4 + jumpLineD);
//                    jumpLineD = jumpLineD + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD DETALLE:
        g2.drawRoundRect(this.X_E4, this.Y_E4, this.WIDTH_E4, this.HEIGHT_E4, 6, 6);
        g2.drawLine(this.X_E4, this.Y_E4 + 20, this.X_E4 + this.WIDTH_E4, this.Y_E4 + 20);
        //RELACION MAESTRO-DETALLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E4 + 40, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 40, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 50, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y + this.HEIGHT_E4, this.X_E4 + 50, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }
//------------------------------------------------------------------------------    

    private void diseniarClasificadorLeft(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getClasificador(), "Clasificador_" + this.CONTADOR_E)) {
            if (this.ENTITY_CLR_NAME.equals("")) {
                agregarEstructuraPorDefecto("Clasificador");
                this.ENTITY_CLR_NAME = "Clasificador_" + this.CONTADOR_E;
            }
        }
        int positionEntityCOR = returnPosition(this.ENTITY_CONTAINER.getClasificador(), this.ENTITY_CLR_NAME);
        LinkedList listCOR = (LinkedList) this.ENTITY_CONTAINER.getClasificador().get(positionEntityCOR);
        LinkedList listFKsCOR = (LinkedList) this.ENTITY_CONTAINER.getCORLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD CLASIFICADOR:
        this.NAME_ENTITY_CLASIFICADOR = (String) listCOR.get(0);
        g2.drawString(this.NAME_ENTITY_CLASIFICADOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD CLASIFICADOR..........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_CLASIFICADOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(ENTITY_CONTAINER, this.NAME_ENTITY_CLASIFICADOR, "Clasificador");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Clasificador");
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        //ATRIBUTOS DE LA ENTIDAD CLASIFICADOR:
        LinkedList attributes = (LinkedList) listCOR.get(1);
        int jumpLineCOR = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineCOR);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCOR);
                jumpLineCOR = jumpLineCOR + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD CLASIFICADOR.........................
//        for (int i = 0; i < listFKsCOR.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCOR.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_CLASIFICADOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineCOR);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCOR);
//                    jumpLineCOR = jumpLineCOR + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD CLASIFICADOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION CLASIFICADOR-CLASIFICADO:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 30);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarClasificadorRight(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getClasificador(), "Clasificador_" + this.CONTADOR_E)) {
            if (this.ENTITY_CLR_NAME.equals("")) {
                agregarEstructuraPorDefecto("Clasificador");
                this.ENTITY_CLR_NAME = "Clasificador_" + this.CONTADOR_E;
            }
        }
        int positionEntityCOR = returnPosition(this.ENTITY_CONTAINER.getClasificador(), this.ENTITY_CLR_NAME);
        LinkedList listCOR = (LinkedList) this.ENTITY_CONTAINER.getClasificador().get(positionEntityCOR);
        LinkedList listFKsCOR = (LinkedList) this.ENTITY_CONTAINER.getCORLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD CLASIFICADOR:
        this.NAME_ENTITY_CLASIFICADOR = (String) listCOR.get(0);
        g2.drawString(this.NAME_ENTITY_CLASIFICADOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD CLASIFICADOR..........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_CLASIFICADOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(ENTITY_CONTAINER, this.NAME_ENTITY_CLASIFICADOR, "Clasificador");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Clasificador");
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        //ATRIBUTOS DE LA ENTIDAD CLASIFICADOR:
        LinkedList attributes = (LinkedList) listCOR.get(1);
        int jumpLineCOR = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("", this.X + 10, this.Y + jumpLineCOR);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCOR);
                jumpLineCOR = jumpLineCOR + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD CLASIFICADOR.........................
//        for (int i = 0; i < listFKsCOR.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCOR.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_CLASIFICADOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineCOR);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCOR);
//                    jumpLineCOR = jumpLineCOR + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD CLASIFICADOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION CLASIFICADOR-CLASIFICADO:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 30);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 40);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X, this.Y + 40, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarClasificadorUp(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getClasificador(), "Clasificador_" + this.CONTADOR_E)) {
            if (this.ENTITY_CLR_NAME.equals("")) {
                agregarEstructuraPorDefecto("Clasificador");
                this.ENTITY_CLR_NAME = "Clasificador_" + this.CONTADOR_E;
            }
        }
        int positionEntityCOR = returnPosition(this.ENTITY_CONTAINER.getClasificador(), this.ENTITY_CLR_NAME);
        LinkedList listCOR = (LinkedList) this.ENTITY_CONTAINER.getClasificador().get(positionEntityCOR);
        LinkedList listFKsCOR = (LinkedList) this.ENTITY_CONTAINER.getCORLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD CLASIFICADOR:
        this.NAME_ENTITY_CLASIFICADOR = (String) listCOR.get(0);
        g2.drawString(this.NAME_ENTITY_CLASIFICADOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD CLASIFICADOR..........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_CLASIFICADOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(ENTITY_CONTAINER, this.NAME_ENTITY_CLASIFICADOR, "Clasificador");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Clasificador");
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        //ATRIBUTOS DE LA ENTIDAD CLASIFICADOR:
        LinkedList attributes = (LinkedList) listCOR.get(1);
        int jumpLineCOR = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineCOR);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCOR);
                jumpLineCOR = jumpLineCOR + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD MAESTRO..............................
//        for (int i = 0; i < listFKsCOR.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCOR.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_CLASIFICADOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineCOR);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCOR);
//                    jumpLineCOR = jumpLineCOR + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD CLASIFICADOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION CLASIFICADOR-CLASIFICADO:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 40, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 50, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y + this.HEIGHT, this.X_E4 + 50, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarClasificadorBelow(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getClasificador(), "Clasificador_" + this.CONTADOR_E)) {
            if (this.ENTITY_CLR_NAME.equals("")) {
                agregarEstructuraPorDefecto("Clasificador");
                this.ENTITY_CLR_NAME = "Clasificador_" + this.CONTADOR_E;
            }
        }
        int positionEntityCOR = returnPosition(this.ENTITY_CONTAINER.getClasificador(), this.ENTITY_CLR_NAME);
        LinkedList listCOR = (LinkedList) this.ENTITY_CONTAINER.getClasificador().get(positionEntityCOR);
        LinkedList listFKsCOR = (LinkedList) this.ENTITY_CONTAINER.getCORLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD CLASIFICADOR:
        this.NAME_ENTITY_CLASIFICADOR = (String) listCOR.get(0);
        g2.drawString(this.NAME_ENTITY_CLASIFICADOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD CLASIFICADOR..........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_CLASIFICADOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(ENTITY_CONTAINER, this.NAME_ENTITY_CLASIFICADOR, "Clasificador");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Clasificador");
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        //ATRIBUTOS DE LA ENTIDAD CLASIFICADOR:
        LinkedList attributes = (LinkedList) listCOR.get(1);
        int jumpLineCOR = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineCOR);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCOR);
                jumpLineCOR = jumpLineCOR + 18;
            }
        }
//        //LLAVES SEGUNDARIAS DE LA ENTIDAD CLASIFICADOR.........................
//        for (int i = 0; i < listFKsCOR.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCOR.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_CLASIFICADOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineCOR);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCOR);
//                    jumpLineCOR = jumpLineCOR + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD CLASIFICADOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION CLASIFICADOR-CLASIFICADO:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.setStroke(restaurarStroke());
    }
//------------------------------------------------------------------------------

    private void diseniarClasificadoLeft(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getClasificado(), "Clasificado_" + this.CONTADOR_E)) {
            if (this.ENTITY_CLO_NAME.equals("")) {
                agregarEstructuraPorDefecto("Clasificado");
                this.ENTITY_CLO_NAME = "Clasificado_" + this.CONTADOR_E;
            }
        }
        int positionEntityCDO = returnPosition(this.ENTITY_CONTAINER.getClasificado(), this.ENTITY_CLO_NAME);
        LinkedList listCDO = (LinkedList) this.ENTITY_CONTAINER.getClasificado().get(positionEntityCDO);
        LinkedList listFKsCDO = (LinkedList) this.ENTITY_CONTAINER.getCDOLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD CLASIFICADO:
        this.NAME_ENTITY_CLASIFICADO = (String) listCDO.get(0);
        g2.drawString(this.NAME_ENTITY_CLASIFICADO, this.X_E3 + 10, this.Y_E3 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD CLASIFICADO...........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsCDO, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_CLASIFICADO);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADO);
                this.ENTITY_CONTAINER.getCDOLlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        //ATRIBUTOS DE LA ENTIDAD CLASIFICADO:
        LinkedList attributes = (LinkedList) listCDO.get(1);
        int jumpLineCDO = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineCDO);
                g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineCDO);
                jumpLineCDO = jumpLineCDO + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD CLASIFICADO..........................
//        for (int i = 0; i < listFKsCDO.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCDO.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_CLASIFICADO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineCDO);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineCDO);
//                    jumpLineCDO = jumpLineCDO + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD CLASIFICADO:
        g2.drawRoundRect(this.X_E3, this.Y_E3, this.WIDTH_E3, this.HEIGHT_E3, 6, 6);
        g2.drawLine(this.X_E3, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 20);
        //RELACION CLASIFICADOR-CLASIFICADO:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 30);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 40);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X, this.Y + 40, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarClasificadoRight(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getClasificado(), "Clasificado_" + this.CONTADOR_E)) {
            if (this.ENTITY_CLO_NAME.equals("")) {
                agregarEstructuraPorDefecto("Clasificado");
                this.ENTITY_CLO_NAME = "Clasificado_" + this.CONTADOR_E;
            }
        }
        int positionEntityCDO = returnPosition(this.ENTITY_CONTAINER.getClasificado(), this.ENTITY_CLO_NAME);
        LinkedList listCDO = (LinkedList) this.ENTITY_CONTAINER.getClasificado().get(positionEntityCDO);
        LinkedList listFKsCDO = (LinkedList) this.ENTITY_CONTAINER.getCDOLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD CLASIFICADO:
        this.NAME_ENTITY_CLASIFICADO = (String) listCDO.get(0);
        g2.drawString(this.NAME_ENTITY_CLASIFICADO, this.X_E2 + 10, this.Y_E2 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD CALSIFICADO...........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsCDO, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_CLASIFICADO);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADO);
                this.ENTITY_CONTAINER.getCDOLlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        //ATRIBUTOS DE LA ENTIDAD CLASIFICADO:
        LinkedList attributes = (LinkedList) listCDO.get(1);
        int jumpLineCDO = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineCDO);
                g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineCDO);
                jumpLineCDO = jumpLineCDO + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD CLASIFICADO.......................... 
//        for (int i = 0; i < listFKsCDO.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCDO.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_CLASIFICADO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineCDO);
//                    g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineCDO);
//                    jumpLineCDO = jumpLineCDO + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD CLASIFICADO:
        g2.drawRoundRect(this.X_E2, this.Y_E2, this.WIDTH_E2, this.HEIGHT_E2, 6, 6);
        g2.drawLine(this.X_E2, this.Y_E2 + 20, this.X_E2 + this.WIDTH_E2, this.Y_E2 + 20);
        //RELACION CLASIFICADOR-CLASIFICADO:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 30);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarClasificadoUp(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getClasificado(), "Clasificado_" + this.CONTADOR_E)) {
            if (this.ENTITY_CLO_NAME.equals("")) {
                agregarEstructuraPorDefecto("Clasificado");
                this.ENTITY_CLO_NAME = "Clasificado_" + this.CONTADOR_E;
            }
        }
        int positionEntityCDO = returnPosition(this.ENTITY_CONTAINER.getClasificado(), this.ENTITY_CLO_NAME);
        LinkedList listCDO = (LinkedList) this.ENTITY_CONTAINER.getClasificado().get(positionEntityCDO);
        LinkedList listFKsCDO = (LinkedList) this.ENTITY_CONTAINER.getCDOLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD CLASIFICADO:
        this.NAME_ENTITY_CLASIFICADO = (String) listCDO.get(0);
        g2.drawString(this.NAME_ENTITY_CLASIFICADO, this.X_E5 + 10, this.Y_E5 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD CALSIFICADO...........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsCDO, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_CLASIFICADO);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if(0 != myListEntityAttribute.size()){
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADO);
                this.ENTITY_CONTAINER.getCDOLlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        //ATRIBUTOS DE LA ENTIDAD CLASIFICADO:
        LinkedList attributes = (LinkedList) listCDO.get(1);
        int jumpLineCDO = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X_E5 + 10, this.Y_E5 + jumpLineCDO);
                g2.drawString(nameAttribute, this.X_E5 + 20, this.Y_E5 + jumpLineCDO);
                jumpLineCDO = jumpLineCDO + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD CLASIFICADO..........................
//        for (int i = 0; i < listFKsCDO.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCDO.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_CLASIFICADO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E5 + 10, this.Y_E5 + jumpLineCDO);
//                    g2.drawString(nameAttribute, this.X_E5 + 20, this.Y_E5 + jumpLineCDO);
//                    jumpLineCDO = jumpLineCDO + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD CLASIFICADO:
        g2.drawRoundRect(this.X_E5, this.Y_E5, this.WIDTH_E5, this.HEIGHT_E5, 6, 6);
        g2.drawLine(this.X_E5, this.Y_E5 + 20, this.X_E5 + this.WIDTH_E5, this.Y_E5 + 20);
        //RELACION CLASIFICADOR-CLASIFICADO:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarClasificadoBelow(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getClasificado(), "Clasificado_" + this.CONTADOR_E)) {
            if (this.ENTITY_CLO_NAME.equals("")) {
                agregarEstructuraPorDefecto("Clasificado");
                this.ENTITY_CLO_NAME = "Clasificado_" + this.CONTADOR_E;
            }
        }
        int positionEntityCDO = returnPosition(this.ENTITY_CONTAINER.getClasificado(), this.ENTITY_CLO_NAME);
        LinkedList listCDO = (LinkedList) this.ENTITY_CONTAINER.getClasificado().get(positionEntityCDO);
        LinkedList listFKsCDO = (LinkedList) this.ENTITY_CONTAINER.getCDOLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD CLASIFICADO:
        this.NAME_ENTITY_CLASIFICADO = (String) listCDO.get(0);
        g2.drawString(this.NAME_ENTITY_CLASIFICADO, this.X_E4 + 10, this.Y_E4 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD CALSIFICADO...........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsCDO, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_CLASIFICADO);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_CLASIFICADO);
                this.ENTITY_CONTAINER.getCDOLlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCDO, this.NAME_ENTITY_CLASIFICADO, "Clasificado");
        }
        //ATRIBUTOS DE LA ENTIDAD CLASIFICADO:
        LinkedList attributes = (LinkedList) listCDO.get(1);
        int jumpLineCDO = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X_E4 + 10, this.Y_E4 + jumpLineCDO);
                g2.drawString(nameAttribute, this.X_E4 + 20, this.Y_E4 + jumpLineCDO);
                jumpLineCDO = jumpLineCDO + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD CLASIFICADO..........................
//        for (int i = 0; i < listFKsCDO.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCDO.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_CLASIFICADO)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E4 + 10, this.Y_E4 + jumpLineCDO);
//                    g2.drawString(nameAttribute, this.X_E4 + 20, this.Y_E4 + jumpLineCDO);
//                    jumpLineCDO = jumpLineCDO + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD CLASIFICADO:
        g2.drawRoundRect(this.X_E4, this.Y_E4, this.WIDTH_E4, this.HEIGHT_E4, 6, 6);
        g2.drawLine(this.X_E4, this.Y_E4 + 20, this.X_E4 + this.WIDTH_E4, this.Y_E4 + 20);
        //RELACION CLASIFICADOR-CLASIFICADO:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 40, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 50, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y + this.HEIGHT_E4, this.X_E4 + 50, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }
//------------------------------------------------------------------------------

    private void diseniarComposicionLeft(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComposicion(), "Composicion_" + this.CONTADOR_E)) {
            if (this.ENTITY_C_NAME.equals("")) {
                agregarEstructuraPorDefecto("Composicion");
                this.ENTITY_C_NAME = "Composicion_" + this.CONTADOR_E;
            }
        }
        int positionEntityC = returnPosition(this.ENTITY_CONTAINER.getComposicion(), this.ENTITY_C_NAME);
        LinkedList listC = (LinkedList) this.ENTITY_CONTAINER.getComposicion().get(positionEntityC);
        LinkedList listFKsC = (LinkedList) this.ENTITY_CONTAINER.getCLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPOSICION:
        this.NAME_ENTITY_COMPOSICION = (String) listC.get(0);
        g2.drawString(this.NAME_ENTITY_COMPOSICION, this.X_E3 + 10, this.Y_E3 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD COMPOSICION...........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsC, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_COMPOSICION);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_COMPOSICION);
                this.ENTITY_CONTAINER.getCLlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        //ATRIBUTOS DE LA ENTIDAD COMPOSICION:
//        LinkedList attributes = (LinkedList) listC.get(1);
//        int jumpLineC = 35;
//        for (int i = 0; i < attributes.size(); i = i + 5) {
//            String statePK = (String) attributes.get(i + 2);
//            if (statePK.equals("true") && statePK != null) {
//                String nameAttribute = (String) attributes.get(i);
//                g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineC);
//                g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineC);
//                jumpLineC = jumpLineC + 18;
//            }
//        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD COMPOSICION..........................
//        for (int i = 0; i < listFKsC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPOSICION)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineC);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineC);
//                    jumpLineC = jumpLineC + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD COMPOSICION:
        g2.drawRoundRect(this.X_E3, this.Y_E3, this.WIDTH_E3, this.HEIGHT_E2, 6, 6);
        g2.drawLine(this.X_E3, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 20);
        //RELACION COMPONEDOR 1-COMPOSICION:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 30, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 50);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 30);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 40);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X, this.Y + 40, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarComposicionRight(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComposicion(), "Composicion_" + this.CONTADOR_E)) {
            if (this.ENTITY_C_NAME.equals("")) {
                agregarEstructuraPorDefecto("Composicion");
                this.ENTITY_C_NAME = "Composicion_" + this.CONTADOR_E;
            }
        }
        int positionEntityC = returnPosition(this.ENTITY_CONTAINER.getComposicion(), this.ENTITY_C_NAME);
        LinkedList listC = (LinkedList) this.ENTITY_CONTAINER.getComposicion().get(positionEntityC);
        LinkedList listFKsC = (LinkedList) this.ENTITY_CONTAINER.getCLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPOSICION:
        this.NAME_ENTITY_COMPOSICION = (String) listC.get(0);
        g2.drawString(this.NAME_ENTITY_COMPOSICION, this.X_E2 + 10, this.Y_E2 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD COMPOSICION...........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsC, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_COMPOSICION);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_COMPOSICION);
                this.ENTITY_CONTAINER.getCLlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        //ATRIBUTOS DE LA ENTIDAD COMPOSICION:
//        LinkedList attributes = (LinkedList) listC.get(1);
//        int jumpLineC = 35;
//        for (int i = 0; i < attributes.size(); i = i + 5) {
//            String statePK = (String) attributes.get(i + 2);
//            if (statePK.equals("true") && statePK != null) {
//                String nameAttribute = (String) attributes.get(i);
//                g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineC);
//                g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineC);
//                jumpLineC = jumpLineC + 18;
//            }
//        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD COMPOSICION..........................  
//        for (int i = 0; i < listFKsC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPOSICION)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineC);
//                    g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineC);
//                    jumpLineC = jumpLineC + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD COMPOSICION:
        g2.drawRoundRect(this.X_E2, this.Y_E2, this.WIDTH_E2, this.HEIGHT_E2, 6, 6);
        g2.drawLine(this.X_E2, this.Y_E2 + 20, this.X_E2 + this.WIDTH_E2, this.Y_E2 + 20);
        //RELACION COMPONEDOR 1-COMPOSICION:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 30, this.X_E2 - 20, this.Y_E2 + 50);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 30);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarComposicionUp(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComposicion(), "Composicion_" + this.CONTADOR_E)) {
            if (this.ENTITY_C_NAME.equals("")) {
                agregarEstructuraPorDefecto("Composicion");
                this.ENTITY_C_NAME = "Composicion_" + this.CONTADOR_E;
            }
        }
        int positionEntityC = returnPosition(this.ENTITY_CONTAINER.getComposicion(), this.ENTITY_C_NAME);
        LinkedList listC = (LinkedList) this.ENTITY_CONTAINER.getComposicion().get(positionEntityC);
        LinkedList listFKsC = (LinkedList) this.ENTITY_CONTAINER.getCLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPOSICION:
        this.NAME_ENTITY_COMPOSICION = (String) listC.get(0);
        g2.drawString(this.NAME_ENTITY_COMPOSICION, this.X_E5 + 10, this.Y_E5 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD COMPOSICION...........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsC, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_COMPOSICION);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_COMPOSICION);
                this.ENTITY_CONTAINER.getCLlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        //ATRIBUTOS DE LA ENTIDAD COMPOSICION:
//        LinkedList attributes = (LinkedList) listC.get(1);
//        int jumpLineC = 35;
//        for (int i = 0; i < attributes.size(); i = i + 5) {
//            String statePK = (String) attributes.get(i + 2);
//            if (statePK.equals("true") && statePK != null) {
//                String nameAttribute = (String) attributes.get(i);
//                g2.drawString("#", this.X_E5 + 10, this.Y_E5 + jumpLineC);
//                g2.drawString(nameAttribute, this.X_E5 + 20, this.Y_E5 + jumpLineC);
//                jumpLineC = jumpLineC + 18;
//            }
//        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD COMPOSICION..........................
//        for (int i = 0; i < listFKsC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPOSICION)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E5 + 10, this.Y_E5 + jumpLineC);
//                    g2.drawString(nameAttribute, this.X_E5 + 20, this.Y_E5 + jumpLineC);
//                    jumpLineC = jumpLineC + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD COMPOSICION:
        g2.drawRoundRect(this.X_E5, this.Y_E5, this.WIDTH_E5, this.HEIGHT_E5, 6, 6);
        g2.drawLine(this.X_E5, this.Y_E5 + 20, this.X_E5 + this.WIDTH_E5, this.Y_E5 + 20);
        //RELACION COMPONEDOR 1-COMPOSICION:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarComposicionBelow(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComposicion(), "Composicion_" + this.CONTADOR_E)) {
            if (this.ENTITY_C_NAME.equals("")) {
                agregarEstructuraPorDefecto("Composicion");
                this.ENTITY_C_NAME = "Composicion_" + this.CONTADOR_E;
            }
        }
        int positionEntityC = returnPosition(this.ENTITY_CONTAINER.getComposicion(), this.ENTITY_C_NAME);
        LinkedList listC = (LinkedList) this.ENTITY_CONTAINER.getComposicion().get(positionEntityC);
        LinkedList listFKsC = (LinkedList) this.ENTITY_CONTAINER.getCLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPOSICION:
        this.NAME_ENTITY_COMPOSICION = (String) listC.get(0);
        g2.drawString(this.NAME_ENTITY_COMPOSICION, this.X_E4 + 10, this.Y_E4 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD COMPOSICION...........................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsC, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_COMPOSICION);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_COMPOSICION);
                this.ENTITY_CONTAINER.getCLlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsC, this.NAME_ENTITY_COMPOSICION);
        }
        //ATRIBUTOS DE LA ENTIDAD COMPOSICION:
//        LinkedList attributes = (LinkedList) listC.get(1);
//        int jumpLineC = 35;
//        for (int i = 0; i < attributes.size(); i = i + 5) {
//            String statePK = (String) attributes.get(i + 2);
//            if (statePK.equals("true") && statePK != null) {
//                String nameAttribute = (String) attributes.get(i);
//                g2.drawString("#", this.X_E4 + 10, this.Y_E4 + jumpLineC);
//                g2.drawString(nameAttribute, this.X_E4 + 20, this.Y_E4 + jumpLineC);
//                jumpLineC = jumpLineC + 18;
//            }
//        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD COMPOSICION..........................
//        for (int i = 0; i < listFKsC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPOSICION)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E5 + 10, this.Y_E5 + jumpLineC);
//                    g2.drawString(nameAttribute, this.X_E5 + 20, this.Y_E5 + jumpLineC);
//                    jumpLineC = jumpLineC + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD COMPOSICION:
        g2.drawRoundRect(this.X_E4, this.Y_E4, this.WIDTH_E4, this.HEIGHT_E4, 6, 6);
        g2.drawLine(this.X_E4, this.Y_E4 + 20, this.X_E4 + this.WIDTH_E4, this.Y_E4 + 20);
        //RELACION COMPONEDOR 1-COMPOSICION:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E4 + 40, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 40, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 50, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y + this.HEIGHT_E4, this.X_E4 + 50, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }
//------------------------------------------------------------------------------

    private void diseniarComponedorLeft(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComponedor(), "Componedor_" + this.CONTADOR_E)) {
            if (this.ENTITY_C1_NAME.equals("")) {
                agregarEstructuraPorDefecto("Componedor");
                this.ENTITY_C1_NAME = "Componedor_" + this.CONTADOR_E;
            }
        }
        int positionEntityCC = returnPosition(this.ENTITY_CONTAINER.getComponedor(), this.ENTITY_C1_NAME);
        LinkedList listCC = (LinkedList) this.ENTITY_CONTAINER.getComponedor().get(positionEntityCC);
        LinkedList listFKsCC = (LinkedList) this.ENTITY_CONTAINER.getCCLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPONEDOR:
        this.NAME_ENTITY_COMPONEDOR = (String) listCC.get(0);
        g2.drawString(this.NAME_ENTITY_COMPONEDOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD COMPONEDOR............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_COMPONEDOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_COMPONEDOR, "Componedor");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_COMPONEDOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Componedor");
                foreignKeys.add(this.NAME_ENTITY_COMPONEDOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        //ATRIBUTOS DE LA ENTIDAD COMOPONEDOR:
        LinkedList attributes = (LinkedList) listCC.get(1);
        int jumpLineCC = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineCC);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCC);
                jumpLineCC = jumpLineCC + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD COMPONEDOR...........................
//        for (int i = 0; i < listFKsCC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPONEDOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineCC);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCC);
//                    jumpLineCC = jumpLineCC + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD COMPONEDOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION COMPONEDOR 1-COMPOSICION:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 30, this.X_E2 - 20, this.Y_E2 + 50);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 30);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarComponedorRight(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComponedor(), "Componedor_" + this.CONTADOR_E)) {
            if (this.ENTITY_C1_NAME.equals("")) {
                agregarEstructuraPorDefecto("Componedor");
                this.ENTITY_C1_NAME = "Componedor_" + this.CONTADOR_E;
            }
        }
        int positionEntityCC = returnPosition(this.ENTITY_CONTAINER.getComponedor(), this.ENTITY_C1_NAME);
        LinkedList listCC = (LinkedList) this.ENTITY_CONTAINER.getComponedor().get(positionEntityCC);
        LinkedList listFKsCC = (LinkedList) this.ENTITY_CONTAINER.getCCLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPONEDOR:
        this.NAME_ENTITY_COMPONEDOR = (String) listCC.get(0);
        g2.drawString(this.NAME_ENTITY_COMPONEDOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD COMPONEDOR............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_COMPONEDOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_COMPONEDOR, "Componedor");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_COMPONEDOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Componedor");
                foreignKeys.add(this.NAME_ENTITY_COMPONEDOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
//        ATRIBUTOS DE LA ENTIDAD COMOPONEDOR:
        LinkedList attributes = (LinkedList) listCC.get(1);
        int jumpLineCC = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineCC);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCC);
                jumpLineCC = jumpLineCC + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD COMPONEDOR...........................
//        for (int i = 0; i < listFKsCC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPONEDOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineCC);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCC);
//                    jumpLineCC = jumpLineCC + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD COMPONEDOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION COMPONEDOR 1-COMPOSICION:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 30, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 50);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 30);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 40);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 50);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X, this.Y + 40, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarComponedorUp(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComponedor(), "Componedor_" + this.CONTADOR_E)) {
            if (this.ENTITY_C1_NAME.equals("")) {
                agregarEstructuraPorDefecto("Componedor");
                this.ENTITY_C1_NAME = "Componedor_" + this.CONTADOR_E;
            }
        }
        int positionEntityCC = returnPosition(this.ENTITY_CONTAINER.getComponedor(), this.ENTITY_C1_NAME);
        LinkedList listCC = (LinkedList) this.ENTITY_CONTAINER.getComponedor().get(positionEntityCC);
        LinkedList listFKsCC = (LinkedList) this.ENTITY_CONTAINER.getCCLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPONEDOR:
        this.NAME_ENTITY_COMPONEDOR = (String) listCC.get(0);
        g2.drawString(this.NAME_ENTITY_COMPONEDOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD COMPONEDOR............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_COMPONEDOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_COMPONEDOR, "Componedor");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_COMPONEDOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Componedor");
                foreignKeys.add(this.NAME_ENTITY_COMPONEDOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        //ATRIBUTOS DE LA ENTIDAD COMOPONEDOR:
        LinkedList attributes = (LinkedList) listCC.get(1);
        int jumpLineCC = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineCC);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCC);
                jumpLineCC = jumpLineCC + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD COMPONEDOR...........................
//        for (int i = 0; i < listFKsCC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPONEDOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineCC);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCC);
//                    jumpLineCC = jumpLineCC + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD COMPONEDOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION COMPONEDOR 1-COMPOSICION:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E4 + 40, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 40, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 50, this.Y_E4);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y + this.HEIGHT, this.X_E4 + 50, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarComponedorBelow(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getComponedor(), "Componedor_" + this.CONTADOR_E)) {
            if (this.ENTITY_C1_NAME.equals("")) {
                agregarEstructuraPorDefecto("Componedor");
                this.ENTITY_C1_NAME = "Componedor_" + this.CONTADOR_E;
            }
        }
        int positionEntityCC = returnPosition(this.ENTITY_CONTAINER.getComponedor(), this.ENTITY_C1_NAME);
        LinkedList listCC = (LinkedList) this.ENTITY_CONTAINER.getComponedor().get(positionEntityCC);
        LinkedList listFKsCC = (LinkedList) this.ENTITY_CONTAINER.getCCLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPONEDOR:
        this.NAME_ENTITY_COMPONEDOR = (String) listCC.get(0);
        g2.drawString(this.NAME_ENTITY_COMPONEDOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD COMPONEDOR............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_COMPONEDOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_COMPONEDOR, "Componedor");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_COMPONEDOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Componedor");
                foreignKeys.add(this.NAME_ENTITY_COMPONEDOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsCC, this.NAME_ENTITY_COMPONEDOR);
        }
        //ATRIBUTOS DE LA ENTIDAD COMOPONEDOR:
        LinkedList attributes = (LinkedList) listCC.get(1);
        int jumpLineCC = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineCC);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCC);
                jumpLineCC = jumpLineCC + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD COMPONEDOR...........................
//        for (int i = 0; i < listFKsCC.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsCC.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_COMPONEDOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineCC);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineCC);
//                    jumpLineCC = jumpLineCC + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD COMPONEDOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION COMPONEDOR 1-COMPOSICION:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.setStroke(restaurarStroke());
    }
//------------------------------------------------------------------------------    

    private void diseniarGeneralLeft(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getGeneral(), "General_" + this.CONTADOR_E)) {
            if (this.ENTITY_G_NAME.equals("")) {
                agregarEstructuraPorDefecto("General");
                this.ENTITY_G_NAME = "General_" + this.CONTADOR_E;
            }
        }
        int positionEntityG = returnPosition(this.ENTITY_CONTAINER.getGeneral(), this.ENTITY_G_NAME);
        LinkedList listG = (LinkedList) this.ENTITY_CONTAINER.getGeneral().get(positionEntityG);
        LinkedList listFKsG = (LinkedList) this.ENTITY_CONTAINER.getGLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD GENERAL:
        this.NAME_ENTITY_GENERAL = (String) listG.get(0);
        g2.drawString(this.NAME_ENTITY_GENERAL, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD GENERAL...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_GENERAL, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_GENERAL, "General");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_GENERAL);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("General");
                foreignKeys.add(this.NAME_ENTITY_GENERAL + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        //ATRIBUTOS DE LA ENTIDAD GENERAL:
        LinkedList attributes = (LinkedList) listG.get(1);
        int jumpLineG = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineG);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineG);
                jumpLineG = jumpLineG + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD GENERAL..............................
//        for (int i = 0; i < listFKsG.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsG.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_GENERAL)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineG);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineG);
//                    jumpLineG = jumpLineG + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD GENERAL:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION GENERAL-IS_A:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 30, this.X_E2 - 20, this.Y_E2 + 50);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarGeneralRight(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getGeneral(), "General_" + this.CONTADOR_E)) {
            if (this.ENTITY_G_NAME.equals("")) {
                agregarEstructuraPorDefecto("General");
                this.ENTITY_G_NAME = "General_" + this.CONTADOR_E;
            }
        }
        int positionEntityG = returnPosition(this.ENTITY_CONTAINER.getGeneral(), this.ENTITY_G_NAME);
        LinkedList listG = (LinkedList) this.ENTITY_CONTAINER.getGeneral().get(positionEntityG);
        LinkedList listFKsG = (LinkedList) this.ENTITY_CONTAINER.getGLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD GENERAL:
        this.NAME_ENTITY_GENERAL = (String) listG.get(0);
        g2.drawString(this.NAME_ENTITY_GENERAL, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD GENERAL...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_GENERAL, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_GENERAL, "General");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_GENERAL);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("General");
                foreignKeys.add(this.NAME_ENTITY_GENERAL + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        //ATRIBUTOS DE LA ENTIDAD GENERAL:
        LinkedList attributes = (LinkedList) listG.get(1);
        int jumpLineG = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineG);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineG);
                jumpLineG = jumpLineG + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD GENERAL..............................
//        for (int i = 0; i < listFKsG.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsG.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_GENERAL)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineG);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineG);
//                    jumpLineG = jumpLineG + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD GENERAL:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION GENERAL-IS_A:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 30, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 50);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 40);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X, this.Y + 40, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarGeneralUp(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getGeneral(), "General_" + this.CONTADOR_E)) {
            if (this.ENTITY_G_NAME.equals("")) {
                agregarEstructuraPorDefecto("General");
                this.ENTITY_G_NAME = "General_" + this.CONTADOR_E;
            }
        }
        int positionEntityG = returnPosition(this.ENTITY_CONTAINER.getGeneral(), this.ENTITY_G_NAME);
        LinkedList listG = (LinkedList) this.ENTITY_CONTAINER.getGeneral().get(positionEntityG);
        LinkedList listFKsG = (LinkedList) this.ENTITY_CONTAINER.getGLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD GENERAL:
        this.NAME_ENTITY_GENERAL = (String) listG.get(0);
        g2.drawString(this.NAME_ENTITY_GENERAL, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD GENERAL...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_GENERAL, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_GENERAL, "General");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_GENERAL);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("General");
                foreignKeys.add(this.NAME_ENTITY_GENERAL + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        //ATRIBUTOS DE LA ENTIDAD GENERAL:
        LinkedList attributes = (LinkedList) listG.get(1);
        int jumpLineG = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineG);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineG);
                jumpLineG = jumpLineG + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD GENERAL..............................
//        for (int i = 0; i < listFKsG.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsG.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_GENERAL)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineG);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineG);
//                    jumpLineG = jumpLineG + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD GENERAL:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION GENERAL-IS_A:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E4 + 40, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 50, this.Y_E4);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y + this.HEIGHT, this.X_E4 + 50, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarGeneralBelow(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getGeneral(), "General_" + this.CONTADOR_E)) {
            if (this.ENTITY_G_NAME.equals("")) {
                agregarEstructuraPorDefecto("General");
                this.ENTITY_G_NAME = "General_" + this.CONTADOR_E;
            }
        }
        int positionEntityG = returnPosition(this.ENTITY_CONTAINER.getGeneral(), this.ENTITY_G_NAME);
        LinkedList listG = (LinkedList) this.ENTITY_CONTAINER.getGeneral().get(positionEntityG);
        LinkedList listFKsG = (LinkedList) this.ENTITY_CONTAINER.getGLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD GENERAL:
        this.NAME_ENTITY_GENERAL = (String) listG.get(0);
        g2.drawString(this.NAME_ENTITY_GENERAL, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD GENERAL...............................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_GENERAL, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_GENERAL, "General");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_GENERAL);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("General");
                foreignKeys.add(this.NAME_ENTITY_GENERAL + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsG, this.NAME_ENTITY_GENERAL);
        }
        //ATRIBUTOS DE LA ENTIDAD GENERAL:
        LinkedList attributes = (LinkedList) listG.get(1);
        int jumpLineG = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineG);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineG);
                jumpLineG = jumpLineG + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD GENERAL..............................
//        for (int i = 0; i < listFKsG.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsG.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_GENERAL)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineG);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineG);
//                    jumpLineG = jumpLineG + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD GENERAL:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION GENERAL-IS_A:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.setStroke(restaurarStroke());
    }
//------------------------------------------------------------------------------    

    private void diseniarIsaLeft(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getIsa(), "Isa_" + this.CONTADOR_E)) {
            if (this.ENTITY_I_NAME.equals("")) {
                agregarEstructuraPorDefecto("Isa");
                this.ENTITY_I_NAME = "Isa_" + this.CONTADOR_E;
            }
        }
        int positionEntityI = returnPosition(this.ENTITY_CONTAINER.getIsa(), this.ENTITY_I_NAME);
        LinkedList listI = (LinkedList) this.ENTITY_CONTAINER.getIsa().get(positionEntityI);
        LinkedList listFKsI = (LinkedList) this.ENTITY_CONTAINER.getILlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPONENTE IS_A:
        this.NAME_ENTITY_ISA = (String) listI.get(0);
        g2.drawString(this.NAME_ENTITY_ISA, this.X_E3 + 10, this.Y_E3 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD ISA...................................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsI, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_ISA);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_ISA);
                this.ENTITY_CONTAINER.getILlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        //ATRIBUTOS DE LA ENTIDAD IS_A:
//        LinkedList attributes = (LinkedList) listI.get(1);
//        int jumpLineI = 35;
//        for (int i = 0; i < attributes.size(); i = i + 5) {
//            String statePK = (String) attributes.get(i + 2);
//            if (statePK.equals("true") && statePK != null) {
//                String nameAttribute = (String) attributes.get(i);
//                g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineI);
//                g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineI);
//                jumpLineI = jumpLineI + 18;
//            }
//        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD ISA..................................
//        for (int i = 0; i < listFKsI.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsI.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_ISA)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineI);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineI);
//                    jumpLineI = jumpLineI + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD IS_A:
        g2.drawRoundRect(this.X_E3, this.Y_E3, this.WIDTH_E3, this.HEIGHT_E3, 6, 6);
        g2.drawLine(this.X_E3, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 20);
        //RELACION GENERAL-IS_A:
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 30, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 50);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 40);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X, this.Y + 40, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarIsaRight(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getIsa(), "Isa_" + this.CONTADOR_E)) {
            if (this.ENTITY_I_NAME.equals("")) {
                agregarEstructuraPorDefecto("Isa");
                this.ENTITY_I_NAME = "Isa_" + this.CONTADOR_E;
            }
        }
        int positionEntityI = returnPosition(this.ENTITY_CONTAINER.getIsa(), this.ENTITY_I_NAME);
        LinkedList listI = (LinkedList) this.ENTITY_CONTAINER.getIsa().get(positionEntityI);
        LinkedList listFKsI = (LinkedList) this.ENTITY_CONTAINER.getILlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPONENTE IS_A:
        this.NAME_ENTITY_ISA = (String) listI.get(0);
        g2.drawString(this.NAME_ENTITY_ISA, this.X_E2 + 10, this.Y_E2 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD ISA...................................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsI, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_ISA);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_ISA);
                this.ENTITY_CONTAINER.getILlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        //ATRIBUTOS DE LA ENTIDAD IS_A:
//        LinkedList attributes = (LinkedList) listI.get(1);
//        int jumpLineI = 35;
//        for (int i = 0; i < attributes.size(); i = i + 5) {
//            String statePK = (String) attributes.get(i + 2);
//            if (statePK.equals("true") && statePK != null) {
//                String nameAttribute = (String) attributes.get(i);
//                g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineI);
//                g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineI);
//                jumpLineI = jumpLineI + 18;
//            }
//        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD ISA..................................
//        for (int i = 0; i < listFKsI.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsI.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_ISA)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineI);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineI);
//                    jumpLineI = jumpLineI + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD IS_A:
        g2.drawRoundRect(this.X_E2, this.Y_E2, this.WIDTH_E2, this.HEIGHT_E2, 6, 6);
        g2.drawLine(this.X_E2, this.Y_E2 + 20, this.X_E2 + this.WIDTH_E2, this.Y_E2 + 20);
        //RELACION GENERAL-IS_A:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 30, this.X_E2 - 20, this.Y_E2 + 50);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 40, this.X_E2, this.Y_E2 + 40);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X_E2 - 20, this.Y_E2 + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarIsaUp(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getIsa(), "Isa_" + this.CONTADOR_E)) {
            if (this.ENTITY_I_NAME.equals("")) {
                agregarEstructuraPorDefecto("Isa");
                this.ENTITY_I_NAME = "Isa_" + this.CONTADOR_E;
            }
        }
        int positionEntityI = returnPosition(this.ENTITY_CONTAINER.getIsa(), this.ENTITY_I_NAME);
        LinkedList listI = (LinkedList) this.ENTITY_CONTAINER.getIsa().get(positionEntityI);
        LinkedList listFKsI = (LinkedList) this.ENTITY_CONTAINER.getILlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPONENTE IS_A:
        this.NAME_ENTITY_ISA = (String) listI.get(0);
        g2.drawString(this.NAME_ENTITY_ISA, this.X_E5 + 10, this.Y_E5 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD ISA...................................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsI, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_ISA);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_ISA);
                this.ENTITY_CONTAINER.getILlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        //ATRIBUTOS DE LA ENTIDAD IS_A:
//        LinkedList attributes = (LinkedList) listI.get(1);
//        int jumpLineI = 35;
//        for (int i = 0; i < attributes.size(); i = i + 5) {
//            String statePK = (String) attributes.get(i + 2);
//            if (statePK.equals("true") && statePK != null) {
//                String nameAttribute = (String) attributes.get(i);
//                g2.drawString("#", this.X_E5 + 10, this.Y_E5 + jumpLineI);
//                g2.drawString(nameAttribute, this.X_E5 + 20, this.Y_E5 + jumpLineI);
//                jumpLineI = jumpLineI + 18;
//            }
//        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD ISA..................................
//        for (int i = 0; i < listFKsI.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsI.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_ISA)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineI);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineI);
//                    jumpLineI = jumpLineI + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD IS_A:
        g2.drawRoundRect(this.X_E5, this.Y_E5, this.WIDTH_E5, this.HEIGHT_E5, 6, 6);
        g2.drawLine(this.X_E5, this.Y_E5 + 20, this.X_E5 + this.WIDTH_E5, this.Y_E5 + 20);
        //RELACION GENERAL-IS_A:
        g2.drawLine(this.X_E5 + 40, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 60, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.drawLine(this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y, this.X_E5 + 50, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarIsaBelow(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getIsa(), "Isa_" + this.CONTADOR_E)) {
            if (this.ENTITY_I_NAME.equals("")) {
                agregarEstructuraPorDefecto("Isa");
                this.ENTITY_I_NAME = "Isa_" + this.CONTADOR_E;
            }
        }
        int positionEntityI = returnPosition(this.ENTITY_CONTAINER.getIsa(), this.ENTITY_I_NAME);
        LinkedList listI = (LinkedList) this.ENTITY_CONTAINER.getIsa().get(positionEntityI);
        LinkedList listFKsI = (LinkedList) this.ENTITY_CONTAINER.getILlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD COMPONENTE IS_A:
        this.NAME_ENTITY_ISA = (String) listI.get(0);
        g2.drawString(this.NAME_ENTITY_ISA, this.X_E4 + 10, this.Y_E4 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD ISA...................................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsI, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_ISA);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_ISA);
                this.ENTITY_CONTAINER.getILlaveSegundaria().add(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsI, this.NAME_ENTITY_ISA);
        }
        //ATRIBUTOS DE LA ENTIDAD IS_A:
//        LinkedList attributes = (LinkedList) listI.get(1);
//        int jumpLineI = 35;
//        for (int i = 0; i < attributes.size(); i = i + 5) {
//            String statePK = (String) attributes.get(i + 2);
//            if (statePK.equals("true") && statePK != null) {
//                String nameAttribute = (String) attributes.get(i);
//                g2.drawString("#", this.X_E4 + 10, this.Y_E4 + jumpLineI);
//                g2.drawString(nameAttribute, this.X_E4 + 20, this.Y_E4 + jumpLineI);
//                jumpLineI = jumpLineI + 18;
//            }
//        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD ISA..................................
//        for (int i = 0; i < listFKsI.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsI.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_ISA)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineI);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineI);
//                    jumpLineI = jumpLineI + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD IS_A:
        g2.drawRoundRect(this.X_E4, this.Y_E4, this.WIDTH_E4, this.HEIGHT_E4, 6, 6);
        g2.drawLine(this.X_E4, this.Y_E4 + 20, this.X_E4 + this.WIDTH_E4, this.Y_E4 + 20);
        //RELACION GENERAL-IS_A:
        g2.drawLine(this.X_E4 + 40, this.Y_E4 - 20, this.X_E4 + 60, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 50, this.Y_E4 - 20, this.X_E4 + 50, this.Y_E4);
        //LINEA DE LA RELACION DE INICIAL:
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + 50, this.Y + this.HEIGHT_E4, this.X_E4 + 50, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }
//------------------------------------------------------------------------------    

    private void diseniarRSimpleLeft(Graphics2D g2) {
        LinkedList listFKsRS = (LinkedList) this.ENTITY_CONTAINER.getRSLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        this.NAME_ENTITY_RSIMPLE = this.ENTITY_RELATION_NAME;
        //LLAVES FORANEAS PARA LA ENTIDAD REFLEXIVO SIMPLE......................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("RSimple");
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        //RELACION REFLEXIVO SIMPLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X + 40, this.Y + this.HEIGHT + 20, this.X + 30, this.Y + this.HEIGHT);
        g2.drawLine(this.X + 40, this.Y + this.HEIGHT + 20, this.X + 40, this.Y + this.HEIGHT);
        g2.drawLine(this.X + 40, this.Y + this.HEIGHT + 20, this.X + 50, this.Y + this.HEIGHT);
        //LINEA DE LA RELACION DE INICIAL:
        g2.drawLine(this.X - 40, this.Y + this.HEIGHT + 40, this.X + 40, this.Y + this.HEIGHT + 40);
        g2.drawLine(this.X + 40, this.Y + this.HEIGHT + 40, this.X + 40, this.Y + this.HEIGHT + 20);
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X, this.Y + 40, this.X - 40, this.Y + 40);
        g2.drawLine(this.X - 40, this.Y + 40, this.X - 40, this.Y + this.HEIGHT + 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarRSimpleRight(Graphics2D g2) {
        LinkedList listFKsRS = (LinkedList) this.ENTITY_CONTAINER.getRSLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        this.NAME_ENTITY_RSIMPLE = this.ENTITY_RELATION_NAME;
        //LLAVES FORANEAS PARA LA ENTIDAD REFLEXIVO SIMPLE......................
        LinkedList foreignKeys = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("RSimple");
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        if(this.ENTITY_TIPE.equals("Basico")){
            agregarNuevosFKsAlaEntidadCCRS(this.ENTITY_CONTAINER.getBLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsRS, this.NAME_ENTITY_RSIMPLE, "RSimple");
        }
        //RELACION REFLEXIVO SIMPLE:
        //FLECHA DE LA RELACION FINAL:
        g2.drawLine(this.X + 40, this.Y - 20, this.X + 30, this.Y);
        g2.drawLine(this.X + 40, this.Y - 20, this.X + 40, this.Y);
        g2.drawLine(this.X + 40, this.Y - 20, this.X + 50, this.Y);
        //LINEA DE LA RELACION DE INICIAL:
        g2.drawLine(this.X + this.WIDTH + 40, this.Y - 40, this.X + 40, this.Y - 40);
        g2.drawLine(this.X + 40, this.Y - 40, this.X + 40, this.Y - 20);
        g2.setStroke(flechaRelacion());
        g2.drawLine(this.X + this.WIDTH, this.Y + 40, this.X + this.WIDTH + 40, this.Y + 40);
        g2.drawLine(this.X + this.WIDTH + 40, this.Y + 40, this.X + this.WIDTH + 40, this.Y - 40);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarRSimpleUp(Graphics2D g2) {

    }

    private void diseniarRSimpleBelow(Graphics2D g2) {

    }
//------------------------------------------------------------------------------    

    private void diseniarActorLeft(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getActor(), "Actor_" + this.CONTADOR_E)) {
            if (this.ENTITY_A_NAME.equals("")) {
                agregarEstructuraPorDefecto("Actor");
                this.ENTITY_A_NAME = "Actor_" + this.CONTADOR_E;
            }
        }
        int positionEntityA = returnPosition(this.ENTITY_CONTAINER.getActor(), this.ENTITY_A_NAME);
        LinkedList listA = (LinkedList) this.ENTITY_CONTAINER.getActor().get(positionEntityA);
        LinkedList listFKsA = (LinkedList) this.ENTITY_CONTAINER.getALlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD ACTOR:
        this.NAME_ENTITY_ACTOR = (String) listA.get(0);
        g2.drawString(this.NAME_ENTITY_ACTOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD ACTOR.................................
        LinkedList foreignKeys = new LinkedList();
        LinkedList foreignKeys2 = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_ACTOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_ACTOR, "Actor");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_ACTOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Actor");
                foreignKeys.add(this.NAME_ENTITY_ACTOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
                foreignKeys2.add(this.NAME_ENTITY_ACTOR);
                foreignKeys2.add(nameAttributePK + "_2");
                foreignKeys2.add("Actor");
                foreignKeys2.add(this.NAME_ENTITY_ACTOR + "_" + nameAttributePK + "_2");
                foreignKeys2.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys2);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        //ATRIBUTOS DE LA ENTIDAD ACTOR:
        LinkedList attributes = (LinkedList) listA.get(1);
        int jumpLineA = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineA);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineA);
                jumpLineA = jumpLineA + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD ACTOR................................
//        for (int i = 0; i < listFKsA.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsA.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_ACTOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineA);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineA);
//                    jumpLineA = jumpLineA + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD ACTOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION ACTOR-HISTORIA:
        //FLECHA DE LA PRIMERA RELACION FINAL:      
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 10, this.X_E2 - 20, this.Y_E2 + 30);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 20, this.X_E2, this.Y_E2 + 10);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 20, this.X_E2, this.Y_E2 + 20);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 20, this.X_E2, this.Y_E2 + 30);
        //FLECHA DE LA SEGUNDA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 50, this.X_E2 - 20, this.Y_E2 + 70);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 60, this.X_E2, this.Y_E2 + 50);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 60, this.X_E2, this.Y_E2 + 60);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 60, this.X_E2, this.Y_E2 + 70);
        g2.setStroke(flechaRelacion());
        //LINEA DE LA PRIMERA RELACION DE INICIAL:
        g2.drawLine(this.X + this.WIDTH, this.Y + 20, this.X_E2 - 20, this.Y_E2 + 20);
        //LINEA DE LA SEGUNDA RELACION DE INICIAL:
        g2.drawLine(this.X + this.WIDTH, this.Y + 60, this.X_E2 - 20, this.Y_E2 + 60);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarActorRight(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getActor(), "Actor_" + this.CONTADOR_E)) {
            if (this.ENTITY_A_NAME.equals("")) {
                agregarEstructuraPorDefecto("Actor");
                this.ENTITY_A_NAME = "Actor_" + this.CONTADOR_E;
            }
        }
        int positionEntityA = returnPosition(this.ENTITY_CONTAINER.getActor(), this.ENTITY_A_NAME);
        LinkedList listA = (LinkedList) this.ENTITY_CONTAINER.getActor().get(positionEntityA);
        LinkedList listFKsA = (LinkedList) this.ENTITY_CONTAINER.getALlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD ACTOR:
        this.NAME_ENTITY_ACTOR = (String) listA.get(0);
        g2.drawString(this.NAME_ENTITY_ACTOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD ACTOR.................................
        LinkedList foreignKeys = new LinkedList();
        LinkedList foreignKeys2 = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_ACTOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_ACTOR, "Actor");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_ACTOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Actor");
                foreignKeys.add(this.NAME_ENTITY_ACTOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
                foreignKeys2.add(this.NAME_ENTITY_ACTOR);
                foreignKeys2.add(nameAttributePK + "_2");
                foreignKeys2.add("Actor");
                foreignKeys2.add(this.NAME_ENTITY_ACTOR + "_" + nameAttributePK + "_2");
                foreignKeys2.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys2);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        //ATRIBUTOS DE LA ENTIDAD ACTOR:
        LinkedList attributes = (LinkedList) listA.get(1);
        int jumpLineA = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineA);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineA);
                jumpLineA = jumpLineA + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD ACTOR................................
//        for (int i = 0; i < listFKsA.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsA.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_ACTOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineA);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineA);
//                    jumpLineA = jumpLineA + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD ACTOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION ACTOR-HISTORIA:
        //FLECHA DE LA PRIMERA RELACION FINAL:      
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 10, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 30);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 10);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 20);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 30);
        //FLECHA DE LA SEGUNDA RELACION FINAL:
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 50, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 70);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 60, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 50);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 60, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 60);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 60, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 70);
        g2.setStroke(flechaRelacion());
        //LINEA DE LA PRIMERA RELACION DE INICIAL:
        g2.drawLine(this.X, this.Y + 20, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 20);
        //LINEA DE LA SEGUNDA RELACION DE INICIAL:
        g2.drawLine(this.X, this.Y + 60, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 60);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarActorUp(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getActor(), "Actor_" + this.CONTADOR_E)) {
            if (this.ENTITY_A_NAME.equals("")) {
                agregarEstructuraPorDefecto("Actor");
                this.ENTITY_A_NAME = "Actor_" + this.CONTADOR_E;
            }
        }
        int positionEntityA = returnPosition(this.ENTITY_CONTAINER.getActor(), this.ENTITY_A_NAME);
        LinkedList listA = (LinkedList) this.ENTITY_CONTAINER.getActor().get(positionEntityA);
        LinkedList listFKsA = (LinkedList) this.ENTITY_CONTAINER.getALlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD ACTOR:
        this.NAME_ENTITY_ACTOR = (String) listA.get(0);
        g2.drawString(this.NAME_ENTITY_ACTOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD ACTOR.................................
        LinkedList foreignKeys = new LinkedList();
        LinkedList foreignKeys2 = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_ACTOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_ACTOR, "Actor");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_ACTOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Actor");
                foreignKeys.add(this.NAME_ENTITY_ACTOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
                foreignKeys2.add(this.NAME_ENTITY_ACTOR);
                foreignKeys2.add(nameAttributePK + "_2");
                foreignKeys2.add("Actor");
                foreignKeys2.add(this.NAME_ENTITY_ACTOR + "_" + nameAttributePK + "_2");
                foreignKeys2.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys2);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        //ATRIBUTOS DE LA ENTIDAD ACTOR:
        LinkedList attributes = (LinkedList) listA.get(1);
        int jumpLineA = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineA);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineA);
                jumpLineA = jumpLineA + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD ACTOR................................
//        for (int i = 0; i < listFKsA.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsA.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_ACTOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineA);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineA);
//                    jumpLineA = jumpLineA + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD ACTOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION ACTOR-HISTORIA:
        //FLECHA DE LA PRIMERA RELACION FINAL:      
        g2.drawLine(this.X_E4 + 10, this.Y_E4 - 20, this.X_E4 + 30, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 20, this.Y_E4 - 20, this.X_E4 + 10, this.Y_E4);
        g2.drawLine(this.X_E4 + 20, this.Y_E4 - 20, this.X_E4 + 20, this.Y_E4);
        g2.drawLine(this.X_E4 + 20, this.Y_E4 - 20, this.X_E4 + 30, this.Y_E4);
        //FLECHA DE LA SEGUNDA RELACION FINAL:
        g2.drawLine(this.X_E4 + 70, this.Y_E4 - 20, this.X_E4 + 90, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 80, this.Y_E4 - 20, this.X_E4 + 70, this.Y_E4);
        g2.drawLine(this.X_E4 + 80, this.Y_E4 - 20, this.X_E4 + 80, this.Y_E4);
        g2.drawLine(this.X_E4 + 80, this.Y_E4 - 20, this.X_E4 + 90, this.Y_E4);
        g2.setStroke(flechaRelacion());
        //LINEA DE LA PRIMERA RELACION DE INICIAL:
        g2.drawLine(this.X + 20, this.Y + this.HEIGHT, this.X_E4 + 20, this.Y_E4 - 20);
        //LINEA DE LA SEGUNDA RELACION DE INICIAL:
        g2.drawLine(this.X + 80, this.Y + this.HEIGHT, this.X_E4 + 80, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarActorBelow(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getActor(), "Actor_" + this.CONTADOR_E)) {
            if (this.ENTITY_A_NAME.equals("")) {
                agregarEstructuraPorDefecto("Actor");
                this.ENTITY_A_NAME = "Actor_" + this.CONTADOR_E;
            }
        }
        int positionEntityA = returnPosition(this.ENTITY_CONTAINER.getActor(), this.ENTITY_A_NAME);
        LinkedList listA = (LinkedList) this.ENTITY_CONTAINER.getActor().get(positionEntityA);
        LinkedList listFKsA = (LinkedList) this.ENTITY_CONTAINER.getALlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD ACTOR:
        this.NAME_ENTITY_ACTOR = (String) listA.get(0);
        g2.drawString(this.NAME_ENTITY_ACTOR, this.X + 10, this.Y + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD ACTOR.................................
        LinkedList foreignKeys = new LinkedList();
        LinkedList foreignKeys2 = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaFK(this.ENTITY_CONTAINER, this.NAME_ENTITY_ACTOR, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.NAME_ENTITY_ACTOR, "Actor");
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.NAME_ENTITY_ACTOR);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add("Actor");
                foreignKeys.add(this.NAME_ENTITY_ACTOR + "_" + nameAttributePK);
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys);
                foreignKeys2.add(this.NAME_ENTITY_ACTOR);
                foreignKeys2.add(nameAttributePK + "_2");
                foreignKeys2.add("Actor");
                foreignKeys2.add(this.NAME_ENTITY_ACTOR + "_" + nameAttributePK + "_2");
                foreignKeys2.add(this.ENTITY_RELATION_NAME);
                addListaLlavesSegundarias(foreignKeys2);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidadFuerte(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsA, this.NAME_ENTITY_ACTOR);
        }
        //ATRIBUTOS DE LA ENTIDAD ACTOR:
        LinkedList attributes = (LinkedList) listA.get(1);
        int jumpLineA = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X + 10, this.Y + jumpLineA);
                g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineA);
                jumpLineA = jumpLineA + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD ACTOR................................
//        for (int i = 0; i < listFKsA.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsA.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_ACTOR)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X + 10, this.Y + jumpLineA);
//                    g2.drawString(nameAttribute, this.X + 20, this.Y + jumpLineA);
//                    jumpLineA = jumpLineA + 18;
//                }
//            }
//        }
        //GRAFICA DE LA ENTIDAD ACTOR:
        g2.drawRoundRect(this.X, this.Y, this.WIDTH, this.HEIGHT, 6, 6);
        g2.drawLine(this.X, this.Y + 20, this.X + this.WIDTH, this.Y + 20);
        //RELACION ACTOR-HISTORIA:
        //FLECHA DE LA PRIMERA RELACION FINAL:      
        g2.drawLine(this.X_E5 + 10, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 30, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.drawLine(this.X_E5 + 20, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 10, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 20, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 20, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 20, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 30, this.Y_E5 + this.HEIGHT_E5);
        //FLECHA DE LA SEGUNDA RELACION FINAL:
        g2.drawLine(this.X_E5 + 70, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 90, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.drawLine(this.X_E5 + 80, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 70, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 80, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 80, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 80, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 90, this.Y_E5 + this.HEIGHT_E5);
        g2.setStroke(flechaRelacion());
        //LINEA DE LA PRIMERA RELACION DE INICIAL:
        g2.drawLine(this.X + 20, this.Y, this.X_E5 + 20, this.Y_E5 + this.HEIGHT_E5 + 20);
        //LINEA DE LA SEGUNDA RELACION DE INICIAL:
        g2.drawLine(this.X + 80, this.Y, this.X_E5 + 80, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.setStroke(restaurarStroke());
    }
//------------------------------------------------------------------------------    

    private void diseniarHistoriaLeft(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getHistoria(), "Historia_" + this.CONTADOR_E)) {
            if (this.ENTITY_H_NAME.equals("")) {
                agregarEstructuraPorDefecto("Historia");
                this.ENTITY_H_NAME = "Historia_" + this.CONTADOR_E;
            }
        }
        int positionEntityH = returnPosition(this.ENTITY_CONTAINER.getHistoria(), this.ENTITY_H_NAME);
        LinkedList listH = (LinkedList) this.ENTITY_CONTAINER.getHistoria().get(positionEntityH);
        LinkedList listFKsH = (LinkedList) this.ENTITY_CONTAINER.getHLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD HISTORIA:
        this.NAME_ENTITY_HISTORIA = (String) listH.get(0);
        g2.drawString(this.NAME_ENTITY_HISTORIA, this.X_E3 + 10, this.Y_E3 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD HISTORIA..............................
        LinkedList foreignKeys = new LinkedList();
        LinkedList foreignKeys2 = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsH, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_HISTORIA);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_HISTORIA);
                this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys);
                foreignKeys2.add(this.ENTITY_RELATION_NAME);
                foreignKeys2.add(nameAttributePK + "_2");
                foreignKeys2.add(this.ENTITY_TIPE);
                foreignKeys2.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK + "_2");
                foreignKeys2.add(this.NAME_ENTITY_HISTORIA);
                this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys2);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        //ATRIBUTOS DE LA ENTIDAD HISTORIA:
        LinkedList attributes = (LinkedList) listH.get(1);
        int jumpLineH = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineH);
                g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineH);
                jumpLineH = jumpLineH + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD HISTORIA.............................
//        for (int i = 0; i < listFKsH.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsH.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_HISTORIA)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineH);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineH);
//                    jumpLineH = jumpLineH + 18;
//                }
//            }
//        }
        //GRAFICA DE LA TABLA HISTORIA:
        g2.drawRoundRect(this.X_E3, this.Y_E3, this.WIDTH_E3, this.HEIGHT_E3, 6, 6);
        g2.drawLine(this.X_E3, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 20);
        //RELACION SEMEJANTE-HISTORIA:
        //FLECHA DE LA PRIMERA RELACION FINAL:
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 10, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 30);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 10);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 20);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 20, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 30);
        //FLECHA DE LA SEGUNDA RELACION FINAL:
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 50, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 70);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 60, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 50);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 60, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 60);
        g2.drawLine(this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 60, this.X_E3 + this.WIDTH_E3, this.Y_E3 + 70);
        g2.setStroke(flechaRelacion());
        //LINEA DE LA PRIMERA RELACION DE INICIAL:
        g2.drawLine(this.X, this.Y + 20, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 20);
        //LINEA DE LA SEGUNDA RELACION DE INICIAL:
        g2.drawLine(this.X, this.Y + 60, this.X_E3 + this.WIDTH_E3 + 20, this.Y_E3 + 60);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarHistoriaRight(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getHistoria(), "Historia_" + this.CONTADOR_E)) {
            if (this.ENTITY_H_NAME.equals("")) {
                agregarEstructuraPorDefecto("Historia");
                this.ENTITY_H_NAME = "Historia_" + this.CONTADOR_E;
            }
        }
        int positionEntityH = returnPosition(this.ENTITY_CONTAINER.getHistoria(), this.ENTITY_H_NAME);
        LinkedList listH = (LinkedList) this.ENTITY_CONTAINER.getHistoria().get(positionEntityH);
        LinkedList listFKsH = (LinkedList) this.ENTITY_CONTAINER.getHLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD HISTORIA:
        this.NAME_ENTITY_HISTORIA = (String) listH.get(0);
        g2.drawString(this.NAME_ENTITY_HISTORIA, this.X_E2 + 10, this.Y_E2 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD HISTORIA..............................
        LinkedList foreignKeys = new LinkedList();
        LinkedList foreignKeys2 = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsH, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_HISTORIA);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_HISTORIA);
                this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys);
                foreignKeys2.add(this.ENTITY_RELATION_NAME);
                foreignKeys2.add(nameAttributePK + "_2");
                foreignKeys2.add(this.ENTITY_TIPE);
                foreignKeys2.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK + "_2");
                foreignKeys2.add(this.NAME_ENTITY_HISTORIA);
                this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys2);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        //ATRIBUTOS DE LA ENTIDAD HISTORIA:
        LinkedList attributes = (LinkedList) listH.get(1);
        int jumpLineH = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X_E2 + 10, this.Y_E2 + jumpLineH);
                g2.drawString(nameAttribute, this.X_E2 + 20, this.Y_E2 + jumpLineH);
                jumpLineH = jumpLineH + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD HISTORIA.............................
//        for (int i = 0; i < listFKsH.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsH.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_HISTORIA)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineH);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineH);
//                    jumpLineH = jumpLineH + 18;
//                }
//            }
//        }
        //GRAFICA DE LA TABLA HISTORIA:
        g2.drawRoundRect(this.X_E2, this.Y_E2, this.WIDTH_E2, this.HEIGHT_E2, 6, 6);
        g2.drawLine(this.X_E2, this.Y_E2 + 20, this.X_E2 + this.WIDTH_E2, this.Y_E2 + 20);
        //RELACION SEMEJANTE-HISTORIA:
        //FLECHA DE LA PRIMERA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 10, this.X_E2 - 20, this.Y_E2 + 30);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 20, this.X_E2, this.Y_E2 + 10);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 20, this.X_E2, this.Y_E2 + 20);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 20, this.X_E2, this.Y_E2 + 30);
        //FLECHA DE LA SEGUNDA RELACION FINAL:
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 50, this.X_E2 - 20, this.Y_E2 + 70);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 60, this.X_E2, this.Y_E2 + 50);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 60, this.X_E2, this.Y_E2 + 60);
        g2.drawLine(this.X_E2 - 20, this.Y_E2 + 60, this.X_E2, this.Y_E2 + 70);
        g2.setStroke(flechaRelacion());
        //LINEA DE LA PRIMERA RELACION DE INICIAL:
        g2.drawLine(this.X + this.WIDTH, this.Y + 20, this.X_E2 - 20, this.Y_E2 + 20);
        //LINEA DE LA SEGUNDA RELACION DE INICIAL:
        g2.drawLine(this.X + this.WIDTH, this.Y + 60, this.X_E2 - 20, this.Y_E2 + 60);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarHistoriaUp(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getHistoria(), "Historia_" + this.CONTADOR_E)) {
            if (this.ENTITY_H_NAME.equals("")) {
                agregarEstructuraPorDefecto("Historia");
                this.ENTITY_H_NAME = "Historia_" + this.CONTADOR_E;
            }
        }
        int positionEntityH = returnPosition(this.ENTITY_CONTAINER.getHistoria(), this.ENTITY_H_NAME);
        LinkedList listH = (LinkedList) this.ENTITY_CONTAINER.getHistoria().get(positionEntityH);
        LinkedList listFKsH = (LinkedList) this.ENTITY_CONTAINER.getHLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD HISTORIA:
        this.NAME_ENTITY_HISTORIA = (String) listH.get(0);
        g2.drawString(this.NAME_ENTITY_HISTORIA, this.X_E5 + 10, this.Y_E5 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD HISTORIA..............................
        LinkedList foreignKeys = new LinkedList();
        LinkedList foreignKeys2 = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsH, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_HISTORIA);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_HISTORIA);
                this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys);
                foreignKeys2.add(this.ENTITY_RELATION_NAME);
                foreignKeys2.add(nameAttributePK + "_2");
                foreignKeys2.add(this.ENTITY_TIPE);
                foreignKeys2.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK + "_2");
                foreignKeys2.add(this.NAME_ENTITY_HISTORIA);
                this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys2);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        //ATRIBUTOS DE LA ENTIDAD HISTORIA:
        LinkedList attributes = (LinkedList) listH.get(1);
        int jumpLineH = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X_E5 + 10, this.Y_E5 + jumpLineH);
                g2.drawString(nameAttribute, this.X_E5 + 20, this.Y_E5 + jumpLineH);
                jumpLineH = jumpLineH + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD HISTORIA.............................
//        for (int i = 0; i < listFKsH.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsH.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_HISTORIA)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineH);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineH);
//                    jumpLineH = jumpLineH + 18;
//                }
//            }
//        }
        //GRAFICA DE LA TABLA HISTORIA:
        g2.drawRoundRect(this.X_E5, this.Y_E5, this.WIDTH_E5, this.HEIGHT_E5, 6, 6);
        g2.drawLine(this.X_E5, this.Y_E5 + 20, this.X_E5 + this.WIDTH_E5, this.Y_E5 + 20);
        //RELACION SEMEJANTE-HISTORIA:
        //FLECHA DE LA PRIMERA RELACION FINAL:
        g2.drawLine(this.X_E5 + 10, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 30, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.drawLine(this.X_E5 + 20, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 10, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 20, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 20, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 20, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 30, this.Y_E5 + this.HEIGHT_E5);
        //FLECHA DE LA SEGUNDA RELACION FINAL:
        g2.drawLine(this.X_E5 + 70, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 90, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.drawLine(this.X_E5 + 80, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 70, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 80, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 80, this.Y_E5 + this.HEIGHT_E5);
        g2.drawLine(this.X_E5 + 80, this.Y_E5 + this.HEIGHT_E5 + 20, this.X_E5 + 90, this.Y_E5 + this.HEIGHT_E5);
        g2.setStroke(flechaRelacion());
        //LINEA DE LA PRIMERA RELACION DE INICIAL:
        g2.drawLine(this.X + 20, this.Y, this.X_E5 + 20, this.Y_E5 + this.HEIGHT_E5 + 20);
        //LINEA DE LA SEGUNDA RELACION DE INICIAL:
        g2.drawLine(this.X + 80, this.Y, this.X_E5 + 80, this.Y_E5 + this.HEIGHT_E5 + 20);
        g2.setStroke(restaurarStroke());
    }

    private void diseniarHistoriaBelow(Graphics2D g2) {
        if (!estadoExistenciaEntidad(this.ENTITY_CONTAINER.getHistoria(), "Historia_" + this.CONTADOR_E)) {
            if (this.ENTITY_H_NAME.equals("")) {
                agregarEstructuraPorDefecto("Historia");
                this.ENTITY_H_NAME = "Historia_" + this.CONTADOR_E;
            }
        }
        int positionEntityH = returnPosition(this.ENTITY_CONTAINER.getHistoria(), this.ENTITY_H_NAME);
        LinkedList listH = (LinkedList) this.ENTITY_CONTAINER.getHistoria().get(positionEntityH);
        LinkedList listFKsH = (LinkedList) this.ENTITY_CONTAINER.getHLlaveSegundaria();
        g2.setColor(Color.black);
        g2.setFont(FUENTE);
        //NOMBRE DE LA ENTIDAD HISTORIA:
        this.NAME_ENTITY_HISTORIA = (String) listH.get(0);
        g2.drawString(this.NAME_ENTITY_HISTORIA, this.X_E4 + 10, this.Y_E4 + 17);
        //LLAVES FORANEAS PARA LA ENTIDAD HISTORIA..............................
        LinkedList foreignKeys = new LinkedList();
        LinkedList foreignKeys2 = new LinkedList();
        this.STATE_EXIST_FK = estadoExistenciaAtributoFK(listFKsH, this.ENTITY_RELATION_NAME, this.NAME_ENTITY_HISTORIA);
        LinkedList myListEntityAttribute = retornarLista(this.ENTITY_CONTAINER, this.ENTITY_RELATION_NAME, this.ENTITY_TIPE);
        if (0 != myListEntityAttribute.size()) {
            String nameAttributePK = (String) myListEntityAttribute.get(0);
            if (this.STATE_EXIST_FK == false) {
                foreignKeys.add(this.ENTITY_RELATION_NAME);
                foreignKeys.add(nameAttributePK);
                foreignKeys.add(this.ENTITY_TIPE);
                foreignKeys.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK);
                foreignKeys.add(this.NAME_ENTITY_HISTORIA);
                this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys);
                foreignKeys2.add(this.ENTITY_RELATION_NAME);
                foreignKeys2.add(nameAttributePK + "_2");
                foreignKeys2.add(this.ENTITY_TIPE);
                foreignKeys2.add(this.ENTITY_RELATION_NAME + "_" + nameAttributePK + "_2");
                foreignKeys2.add(this.NAME_ENTITY_HISTORIA);
                this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys2);
            }
        }
        if(this.ENTITY_TIPE.equals("Maestro")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getMLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Detalle")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getDLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Clasificador")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCORLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Clasificado")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCDOLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Componedor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Composicion")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getCLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("General")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getGLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Isa")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getILlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Actor")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getALlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        if(this.ENTITY_TIPE.equals("Historia")){
            agregarNuevosFKsAlaEntidad(this.ENTITY_CONTAINER.getHLlaveSegundaria(), this.ENTITY_RELATION_NAME, listFKsH, this.NAME_ENTITY_HISTORIA);
        }
        //ATRIBUTOS DE LA ENTIDAD HISTORIA:
        LinkedList attributes = (LinkedList) listH.get(1);
        int jumpLineH = 35;
        for (int i = 0; i < attributes.size(); i = i + 5) {
            String statePK = (String) attributes.get(i + 2);
            if (statePK.equals("true") && statePK != null) {
                String nameAttribute = (String) attributes.get(i);
                g2.drawString("#", this.X_E4 + 10, this.Y_E4 + jumpLineH);
                g2.drawString(nameAttribute, this.X_E4 + 20, this.Y_E4 + jumpLineH);
                jumpLineH = jumpLineH + 18;
            }
        }
        //LLAVES SEGUNDARIAS DE LA ENTIDAD HISTORIA.............................
//        for (int i = 0; i < listFKsH.size(); i++) {
//            LinkedList listFK = (LinkedList) listFKsH.get(i);
//            String nameEntity = (String) listFK.get(4);
//            if (nameEntity.equals(this.NAME_ENTITY_HISTORIA)) {
//                for (int j = 0; j < listFK.size(); j = j + 5) {
//                    String nameAttribute = (String) listFK.get(j + 1);
//                    g2.drawString("#", this.X_E3 + 10, this.Y_E3 + jumpLineH);
//                    g2.drawString(nameAttribute, this.X_E3 + 20, this.Y_E3 + jumpLineH);
//                    jumpLineH = jumpLineH + 18;
//                }
//            }
//        }
        //GRAFICA DE LA TABLA HISTORIA:
        g2.drawRoundRect(this.X_E4, this.Y_E4, this.WIDTH_E4, this.HEIGHT_E4, 6, 6);
        g2.drawLine(this.X_E4, this.Y_E4 + 20, this.X_E4 + this.WIDTH_E4, this.Y_E4 + 20);
        //RELACION SEMEJANTE-HISTORIA:
        //FLECHA DE LA PRIMERA RELACION FINAL:
        g2.drawLine(this.X_E4 + 10, this.Y_E4 - 20, this.X_E4 + 30, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 20, this.Y_E4 - 20, this.X_E4 + 10, this.Y_E4);
        g2.drawLine(this.X_E4 + 20, this.Y_E4 - 20, this.X_E4 + 20, this.Y_E4);
        g2.drawLine(this.X_E4 + 20, this.Y_E4 - 20, this.X_E4 + 30, this.Y_E4);
        //FLECHA DE LA SEGUNDA RELACION FINAL:
        g2.drawLine(this.X_E4 + 70, this.Y_E4 - 20, this.X_E4 + 90, this.Y_E4 - 20);
        g2.drawLine(this.X_E4 + 80, this.Y_E4 - 20, this.X_E4 + 70, this.Y_E4);
        g2.drawLine(this.X_E4 + 80, this.Y_E4 - 20, this.X_E4 + 80, this.Y_E4);
        g2.drawLine(this.X_E4 + 80, this.Y_E4 - 20, this.X_E4 + 90, this.Y_E4);
        g2.setStroke(flechaRelacion());
        //LINEA DE LA PRIMERA RELACION DE INICIAL:
        g2.drawLine(this.X + 20, this.Y + this.HEIGHT_E4, this.X_E4 + 20, this.Y_E4 - 20);
        //LINEA DE LA SEGUNDA RELACION DE INICIAL:
        g2.drawLine(this.X + 80, this.Y + this.HEIGHT_E4, this.X_E4 + 80, this.Y_E4 - 20);
        g2.setStroke(restaurarStroke());
    }
//------------------------------------------------------------------------------    

    public BasicStroke flechaRelacion() {
        BasicStroke stroke = new BasicStroke(
                1f, // grosor: 10 píxels
                BasicStroke.CAP_BUTT, // terminación: recta
                BasicStroke.JOIN_MITER, // unión: redondeada 
                1f, // ángulo: 1 grado
                new float[]{10, 10, 10, 10, 10, 10, 3000}, // línea para la sepacion
                2 // fase
        );
        return stroke;
    }

    public BasicStroke restaurarStroke() {
        BasicStroke stroke = new BasicStroke(1f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND);
        return stroke;
    }

    public String getIdiomsType() {
        return IDIOMS_TYPE;
    }

    public String getIdiomsPosition() {
        return IDIOMS_POSITION;
    }

    //GET AND SET ENTITY 1:
    public int getX() {
        return this.X;
    }

    public int getY() {
        return this.Y;
    }

    public void setX(int newX) {
        this.X = newX;
    }

    public void setY(int newY) {
        this.Y = newY;
    }

    //GET AND SET ENTITY 2:
    public void setX_E2(int newX) {
        this.X_E2 = newX;
    }

    public void setY_E2(int newY) {
        this.Y_E2 = newY;
    }

    public int getX_E2() {
        return this.X_E2;
    }

    public int getY_E2() {
        return this.Y_E2;
    }

    //GET AND SET ENTITY 3:
    public void setX_E3(int newX) {
        this.X_E3 = newX;
    }

    public void setY_E3(int newY) {
        this.Y_E3 = newY;
    }

    public int getX_E3() {
        return this.X_E3;
    }

    public int getY_E3() {
        return this.Y_E3;
    }

    //GET AND SET ENTITY 4:
    public void setX_E4(int newX) {
        this.X_E4 = newX;
    }

    public void setY_E4(int newY) {
        this.Y_E4 = newY;
    }

    public int getX_E4() {
        return this.X_E4;
    }

    public int getY_E4() {
        return this.Y_E4;
    }

    //GET AND SET ENTITY 5:
    public void setX_E5(int newX) {
        this.X_E5 = newX;
    }

    public void setY_E5(int newY) {
        this.Y_E5 = newY;
    }

    public int getX_E5() {
        return this.X_E5;
    }

    public int getY_E5() {
        return this.Y_E5;
    }

    private boolean estadoExistenciaEntidad(LinkedList list, String newEntityName) {
        boolean res = false;
        for (int i = 0; i < list.size(); i++) {
            LinkedList entity = (LinkedList) list.get(i);
            String entityName = (String) entity.get(0);
            if (entityName.equals(newEntityName)) {
                res = true;
                break;
            }
        }
        return res;
    }

    private void agregarEstructuraPorDefecto(String entityTipe) {
        LinkedList list1 = new LinkedList();
        LinkedList list2 = new LinkedList();
        LinkedList list3 = new LinkedList();
        if (entityTipe.equals("MD")) {
            LinkedList attributesM = new LinkedList();
            list1.add("Maestro" + this.CONTADOR_E); // 0
            attributesM.add("Cod_M" + this.CONTADOR_E); // 1 -> 0
            attributesM.add("INTEGER"); // 1 -> 1
            attributesM.add("true"); // 1 -> 2
            attributesM.add("true"); // 1 -> 3
            attributesM.add("false"); // 1 -> 4
            list1.add(attributesM);
            this.ENTITY_CONTAINER.getMaestro().add(list1);
            LinkedList attributesD = new LinkedList();
            list2.add("Detalle" + (this.CONTADOR_E + 1)); // 0
            attributesD.add("Cod_D" + (this.CONTADOR_E + 1)); // 1 -> 0
            attributesD.add("INTEGER"); // 1 -> 1
            attributesD.add("true"); // 1 -> 2
            attributesD.add("true"); // 1 -> 3
            attributesD.add("false"); // 1 -> 4
            list2.add(attributesD);
            this.ENTITY_CONTAINER.getDetalle().add(list2);
        }
        if (entityTipe.equals("CC")) {
            LinkedList attributesCOR = new LinkedList();
            list1.add("Clasificador" + this.CONTADOR_E); // 0
            attributesCOR.add("Cod_CLR" + this.CONTADOR_E); // 1 -> 0
            attributesCOR.add("INTEGER"); // 1 -> 1
            attributesCOR.add("true"); // 1 -> 2
            attributesCOR.add("true"); // 1 -> 3
            attributesCOR.add("false"); // 1 -> 4
            list1.add(attributesCOR);
            this.ENTITY_CONTAINER.getClasificador().add(list1);
            LinkedList attributesCDO = new LinkedList();
            list2.add("Clasificado" + (this.CONTADOR_E + 1)); // 0
            attributesCDO.add("Cod_CLO" + (this.CONTADOR_E + 1)); // 1 -> 0
            attributesCDO.add("INTEGER"); // 1 -> 1
            attributesCDO.add("true"); // 1 -> 2
            attributesCDO.add("true"); // 1 -> 3
            attributesCDO.add("false"); // 1 -> 4
            list2.add(attributesCDO);
            this.ENTITY_CONTAINER.getClasificado().add(list2);
        }
        if (entityTipe.equals("C")) {
            LinkedList attributesC = new LinkedList();
            list1.add("Composicion" + this.CONTADOR_E); // 0
            attributesC.add("fecha"); // 1 -> 0
            attributesC.add("DATE"); // 1 -> 1
            attributesC.add("false"); // 1 -> 2
            attributesC.add("true"); // 1 -> 3
            attributesC.add("false"); // 1 -> 4
            list1.add(attributesC);
            this.ENTITY_CONTAINER.getComposicion().add(list1);
            LinkedList attributesC1 = new LinkedList();
            list2.add("Componedor" + (this.CONTADOR_E + 1)); // 0
            attributesC1.add("Cod_COR" + (this.CONTADOR_E + 1)); // 1 -> 0
            attributesC1.add("INTEGER"); // 1 -> 1
            attributesC1.add("true"); // 1 -> 2
            attributesC1.add("true"); // 1 -> 3
            attributesC1.add("false"); // 1 -> 4
            list2.add(attributesC1);
            this.ENTITY_CONTAINER.getComponedor().add(list2);
            LinkedList attributesC2 = new LinkedList();
            list3.add("Componedor" + (this.CONTADOR_E + 2)); // 0
            attributesC2.add("Cod_COR" + (this.CONTADOR_E + 2)); // 1 -> 0
            attributesC2.add("INTEGER"); // 1 -> 1
            attributesC2.add("true"); // 1 -> 2
            attributesC2.add("true"); // 1 -> 3
            attributesC2.add("false"); // 1 -> 4
            list3.add(attributesC2);
            this.ENTITY_CONTAINER.getComponedor().add(list3);
        }
        if (entityTipe.equals("ISA")) {
            LinkedList attributesG = new LinkedList();
            list1.add("General" + this.CONTADOR_E); // 0
            attributesG.add("Cod_G" + this.CONTADOR_E); // 1 -> 0
            attributesG.add("INTEGER"); // 1 -> 1
            attributesG.add("true"); // 1 -> 2
            attributesG.add("true"); // 1 -> 3
            attributesG.add("false"); // 1 -> 4
            list1.add(attributesG);
            this.ENTITY_CONTAINER.getGeneral().add(list1);
            LinkedList attributesI = new LinkedList();
            list2.add("Isa" + (this.CONTADOR_E + 1)); // 0
//            attributesI.add("Cod_I" + (this.CONTADOR_E + 1)); // 1 -> 0
//            attributesI.add("INTEGER"); // 1 -> 1
//            attributesI.add("true"); // 1 -> 2
//            attributesI.add("true"); // 1 -> 3
//            attributesI.add("false"); // 1 -> 4
            list2.add(attributesI);
            this.ENTITY_CONTAINER.getIsa().add(list2);
        }
        if (entityTipe.equals("RS")) {
            LinkedList attributesRS = new LinkedList();
            list1.add("RSimple" + this.CONTADOR_E); // 0
            attributesRS.add("Cod_RS" + this.CONTADOR_E); // 1 -> 0
            attributesRS.add("INTEGER"); // 1 -> 1
            attributesRS.add("true"); // 1 -> 2
            attributesRS.add("true"); // 1 -> 3
            attributesRS.add("false"); // 1 -> 4
            list1.add(attributesRS);
            this.ENTITY_CONTAINER.getRSimple().add(list1);
        }
        if (entityTipe.equals("RH")) {
            LinkedList attributesA = new LinkedList();
            list1.add("Actor" + this.CONTADOR_E); // 0
            attributesA.add("Cod_A" + this.CONTADOR_E); // 1 -> 0
            attributesA.add("INTEGER"); // 1 -> 1
            attributesA.add("true"); // 1 -> 2
            attributesA.add("true"); // 1 -> 3
            attributesA.add("false"); // 1 -> 4
            list1.add(attributesA);
            this.ENTITY_CONTAINER.getActor().add(list1);
            LinkedList attributesH = new LinkedList();
            list2.add("Historia" + (this.CONTADOR_E + 1)); // 0
            attributesH.add("Cod_H" + (this.CONTADOR_E + 1)); // 1 -> 0
            attributesH.add("INTEGER"); // 1 -> 1
            attributesH.add("true"); // 1 -> 2
            attributesH.add("true"); // 1 -> 3
            attributesH.add("false"); // 1 -> 4
            list2.add(attributesH);
            this.ENTITY_CONTAINER.getHistoria().add(list2);
        }
        if (entityTipe.equals("B")) {
            LinkedList attributesB = new LinkedList();
            list1.add("Basico" + this.CONTADOR_E); // 0
            attributesB.add("Cod_B" + this.CONTADOR_E); // 1 -> 0
            attributesB.add("INTEGER"); // 1 -> 1
            attributesB.add("true"); // 1 -> 2
            attributesB.add("true"); // 1 -> 3
            attributesB.add("false"); // 1 -> 4
            list1.add(attributesB);
            this.ENTITY_CONTAINER.getBasico().add(list1);
        }
//------------------------------------------------------------------------------        
        if (entityTipe.equals("Maestro")) {
            LinkedList attributes = new LinkedList();
            list1.add("Maestro_" + this.CONTADOR_E); // 0
            attributes.add("Cod_M_" + this.CONTADOR_E); // 1 -> 0
            attributes.add("INTEGER"); // 1 -> 1
            attributes.add("true"); // 1 -> 2
            attributes.add("true"); // 1 -> 3
            attributes.add("false"); // 1 -> 4
            list1.add(attributes);
            this.ENTITY_CONTAINER.getMaestro().add(list1);
        }
        if (entityTipe.equals("Detalle")) {
            LinkedList attributes = new LinkedList();
            list1.add("Detalle_" + this.CONTADOR_E); // 0
            attributes.add("Cod_D_" + this.CONTADOR_E); // 1 -> 0
            attributes.add("INTEGER"); // 1 -> 1
            attributes.add("true"); // 1 -> 2
            attributes.add("true"); // 1 -> 3
            attributes.add("false"); // 1 -> 4
            list1.add(attributes);
            this.ENTITY_CONTAINER.getDetalle().add(list1);
        }
        if (entityTipe.equals("Clasificador")) {
            LinkedList attributes = new LinkedList();
            list1.add("Clasificador_" + this.CONTADOR_E); // 0
            attributes.add("Cod_CLR_" + this.CONTADOR_E); // 1 -> 0
            attributes.add("INTEGER"); // 1 -> 1
            attributes.add("true"); // 1 -> 2
            attributes.add("true"); // 1 -> 3
            attributes.add("false"); // 1 -> 4
            list1.add(attributes);
            this.ENTITY_CONTAINER.getClasificador().add(list1);
        }
        if (entityTipe.equals("Clasificado")) {
            LinkedList attributes = new LinkedList();
            list1.add("Clasificado_" + this.CONTADOR_E); // 0
            attributes.add("Cod_CLO_" + this.CONTADOR_E); // 1 -> 0
            attributes.add("INTEGER"); // 1 -> 1
            attributes.add("true"); // 1 -> 2
            attributes.add("true"); // 1 -> 3
            attributes.add("false"); // 1 -> 4
            list1.add(attributes);
            this.ENTITY_CONTAINER.getClasificado().add(list1);
        }
        if (entityTipe.equals("Composicion")) {
            LinkedList attributes = new LinkedList();
            list1.add("Composicion_" + this.CONTADOR_E); // 0
            attributes.add("fecha"); // 1 -> 0
            attributes.add("DATE"); // 1 -> 1
            attributes.add("false"); // 1 -> 2
            attributes.add("true"); // 1 -> 3
            attributes.add("false"); // 1 -> 4
            list1.add(attributes);
            this.ENTITY_CONTAINER.getComposicion().add(list1);
        }
        if (entityTipe.equals("Componedor")) {
            LinkedList attributes = new LinkedList();
            list1.add("Componedor_" + this.CONTADOR_E); // 0
            attributes.add("Cod_COR_" + this.CONTADOR_E); // 1 -> 0
            attributes.add("INTEGER"); // 1 -> 1
            attributes.add("true"); // 1 -> 2
            attributes.add("true"); // 1 -> 3
            attributes.add("false"); // 1 -> 4
            list1.add(attributes);
            this.ENTITY_CONTAINER.getComponedor().add(list1);
        }
        if (entityTipe.equals("General")) {
            LinkedList attributes = new LinkedList();
            list1.add("General_" + this.CONTADOR_E); // 0
            attributes.add("Cod_G_" + this.CONTADOR_E); // 1 -> 0
            attributes.add("INTEGER"); // 1 -> 1
            attributes.add("true"); // 1 -> 2
            attributes.add("true"); // 1 -> 3
            attributes.add("false"); // 1 -> 4
            list1.add(attributes);
            this.ENTITY_CONTAINER.getGeneral().add(list1);
        }
        if (entityTipe.equals("Isa")) {
            LinkedList attributes = new LinkedList();
            list1.add("Isa_" + this.CONTADOR_E); // 0
//            attributes.add("Cod_I_" + this.CONTADOR_E); // 1 -> 0
//            attributes.add("INTEGER"); // 1 -> 1
//            attributes.add("true"); // 1 -> 2
//            attributes.add("true"); // 1 -> 3
//            attributes.add("false"); // 1 -> 4
            list1.add(attributes);
            this.ENTITY_CONTAINER.getIsa().add(list1);
        }
        if (entityTipe.equals("Actor")) {
            LinkedList attributes = new LinkedList();
            list1.add("Actor_" + this.CONTADOR_E); // 0
            attributes.add("Cod_A_" + this.CONTADOR_E); // 1 -> 0
            attributes.add("INTEGER"); // 1 -> 1
            attributes.add("true"); // 1 -> 2
            attributes.add("true"); // 1 -> 3
            attributes.add("false"); // 1 -> 4
            list1.add(attributes);
            this.ENTITY_CONTAINER.getActor().add(list1);
        }
        if (entityTipe.equals("Historia")) {
            LinkedList attributes = new LinkedList();
            list1.add("Historia_" + this.CONTADOR_E); // 0
            attributes.add("Cod_H_" + this.CONTADOR_E); // 1 -> 0
            attributes.add("INTEGER"); // 1 -> 1
            attributes.add("true"); // 1 -> 2
            attributes.add("true"); // 1 -> 3
            attributes.add("false"); // 1 -> 4
            list1.add(attributes);
            this.ENTITY_CONTAINER.getHistoria().add(list1);
        }
    }

    private boolean estadoExistenciaAtributoFK(LinkedList listfks, String nameEntity, String nameEntity2) {
        boolean res = false;
        for (int i = 0; i < listfks.size(); i++) {
            LinkedList listFK = (LinkedList) listfks.get(i);
            String valueDate = (String) listFK.get(0);
            String valueDate2 = (String) listFK.get(4);
            if (nameEntity.equals(valueDate) && nameEntity2.equals(valueDate2)) {
                res = true;
                break;
            }
        }
        return res;
    }

// SET AND GET NAME_ENTITY_MAESTRO..............................................
    public String getNameEntityMaestro() {
        return this.NAME_ENTITY_MAESTRO;
    }
// SET AND GET NAME_ENTITY_DETALLE..............................................

    public String getNameEntityDetalle() {
        return this.NAME_ENTITY_DETALLE;
    }
// SET AND GET NAME_ENTITY_CLASIFICADOR.........................................

    public String getNameEntityClasificador() {
        return this.NAME_ENTITY_CLASIFICADOR;
    }
// SET AND GET NAME_ENTITY_CLASIFICADO..........................................

    public String getNameEntityClasificado() {
        return this.NAME_ENTITY_CLASIFICADO;
    }
// SET AND GET NAME_ENTITY_COMPOSICION..........................................

    public String getNameEntityComposicion() {
        return this.NAME_ENTITY_COMPOSICION;
    }
// SET AND GET NAME_ENTITY_COMPONEDOR...........................................

    public String getNameEntityComponedor() {
        return this.NAME_ENTITY_COMPONEDOR;
    }
// SET AND GET NAME_ENTITY_COMPONEDOR1..........................................

    public String getNameEntityComponedor1() {
        return this.NAME_ENTITY_COMPONEDOR1;
    }
// SET AND GET NAME_ENTITY_GENERAL..............................................

    public String getNameEntityGeneral() {
        return this.NAME_ENTITY_GENERAL;
    }
// SET AND GET NAME_ENTITY_ISA..................................................

    public String getNameEntityIsa() {
        return this.NAME_ENTITY_ISA;
    }
// SET AND GET NAME_ENTITY_RSIMPLE..............................................

    public String getNameEntityRSimple() {
        return this.NAME_ENTITY_RSIMPLE;
    }
// SET AND GET NAME_ENTITY_ACTOR................................................

    public String getNameEntityActor() {
        return this.NAME_ENTITY_ACTOR;
    }
// SET AND GET NAME_ENTITY_HISTORIA.............................................

    public String getNameEntityHistoria() {
        return this.NAME_ENTITY_HISTORIA;
    }
// SET AND GET NAME_ENTITY_BASICO...............................................

    public String getNameEntityBasico() {
        return this.NAME_ENTITY_BASICO;
    }
// SET AND GET COUNT_ENTITY_MAESTRO.............................................

    public int getContadorEntidad() {
        return this.CONTADOR_E;
    }

    private int returnPosition(LinkedList list, String nameE) {
        int res = 0;
        for (int i = 0; i < list.size(); i++) {
            LinkedList myList = (LinkedList) list.get(i);
            String nameEntity = (String) myList.get(0);
            if (nameE.equals(nameEntity)) {
                res = i;
            }
        }
        return res;
    }

    private LinkedList retornarLista(EntityContainer ENTITY_CONTAINER, String ENTITY_RELATION_NAME, String ENTITY_TIPE) {
        LinkedList res = new LinkedList();
        LinkedList auxList = new LinkedList();
        if (ENTITY_TIPE.equals("Maestro")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getMaestro();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("Detalle")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getDetalle();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("Clasificador")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getClasificador();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("Clasificado")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getClasificado();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("Composicion")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getComposicion();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("Componedor")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getComponedor();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("General")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getGeneral();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("Isa")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getIsa();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("RSimple")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getRSimple();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("Actor")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getActor();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("Historia")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getHistoria();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        if (ENTITY_TIPE.equals("Basico")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getBasico();
            res = retornarListaAtributos(auxList, ENTITY_RELATION_NAME);
        }
        return res;
    }

    private LinkedList retornarListaAtributos(LinkedList auxList, String ENTITY_RELATION_NAME) {
        LinkedList res = new LinkedList();
        for (int i = 0; i < auxList.size(); i++) {
            LinkedList myList = (LinkedList) auxList.get(i);
            String nameEntity = (String) myList.get(0);
            if (nameEntity.equals(ENTITY_RELATION_NAME)) {
                res = (LinkedList) myList.get(1);
            }
        }
        return res;
    }

    private boolean estadoExistenciaFK(EntityContainer ENTITY_CONTAINER, String NAME_ENTITY_MAESTRO, String ENTITY_RELATION_NAME, String ENTITY_TIPE) {
        boolean res = false;
        LinkedList auxList = new LinkedList();
        if (ENTITY_TIPE.equals("Maestro")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getMLlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("Detalle")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getDLlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("Clasificador")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getCORLlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("Clasificado")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getCDOLlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("Composicion")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getCLlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("Componedor")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getCCLlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("General")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getGLlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("Isa")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getILlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("RSimple")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getRSLlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("Actor")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getALlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("Historia")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getHLlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        if (ENTITY_TIPE.equals("Basico")) {
            auxList = (LinkedList) ENTITY_CONTAINER.getBLlaveSegundaria();
            for (int i = 0; i < auxList.size(); i++) {
                LinkedList listFK = (LinkedList) auxList.get(i);
                String valueDate = (String) listFK.get(0);
                String valueDate2 = (String) listFK.get(4);
                if (NAME_ENTITY_MAESTRO.equals(valueDate) && ENTITY_RELATION_NAME.equals(valueDate2)) {
                    res = true;
                    break;
                }
            }
        }
        return res;
    }

    private void addListaLlavesSegundarias(LinkedList foreignKeys) {
        if (this.ENTITY_TIPE.equals("Maestro")) {
            this.ENTITY_CONTAINER.getMLlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("Detalle")) {
            this.ENTITY_CONTAINER.getDLlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("Clasificador")) {
            this.ENTITY_CONTAINER.getCORLlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("Clasificado")) {
            this.ENTITY_CONTAINER.getCDOLlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("Composicion")) {
            this.ENTITY_CONTAINER.getCLlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("Componedor")) {
            this.ENTITY_CONTAINER.getCCLlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("General")) {
            this.ENTITY_CONTAINER.getGLlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("Isa")) {
            this.ENTITY_CONTAINER.getILlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("RSimple")) {
            this.ENTITY_CONTAINER.getRSLlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("Actor")) {
            this.ENTITY_CONTAINER.getALlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("Historia")) {
            this.ENTITY_CONTAINER.getHLlaveSegundaria().add(foreignKeys);
        }
        if (this.ENTITY_TIPE.equals("Basico")) {
            this.ENTITY_CONTAINER.getBLlaveSegundaria().add(foreignKeys);
        }
    }

// SET AND GET ENTITY_MAESTRO_NAME..............................................
    public void setEntityMaestroName(String newName) {
        this.ENTITY_M_NAME = newName;
    }
// SET AND GET ENTITY_DETALLE_NAME..............................................

    public void setEntityDetalleName(String newName) {
        this.ENTITY_D_NAME = newName;
    }
// SET AND GET ENTITY_CLASIFICADOR_NAME.........................................

    public void setEntityClasificadorName(String newName) {
        this.ENTITY_CLR_NAME = newName;
    }
// SET AND GET ENTITY_CLASIFICADO_NAME..........................................

    public void setEntityClasificadoName(String newName) {
        this.ENTITY_CLO_NAME = newName;
    }
// SET AND GET ENTITY_COMPOSICION_NAME..........................................

    public void setEntityComposicionName(String newName) {
        this.ENTITY_C_NAME = newName;
    }
// SET AND GET ENTITY_COMPONEDOR_NAME...........................................

    public void setEntityComponedorName(String newName) {
        this.ENTITY_C1_NAME = newName;
    }
// SET AND GET ENTITY_COMPONEDOR1_NAME..........................................

    public void setEntityComponedor1Name(String newName) {
        this.ENTITY_C2_NAME = newName;
    }
// SET AND GET ENTITY_GENERAL_NAME..............................................

    public void setEntityGeneralName(String newName) {
        this.ENTITY_G_NAME = newName;
    }
// SET AND GET ENTITY_ISA_NAME..................................................

    public void setEntityIsaName(String newName) {
        this.ENTITY_I_NAME = newName;
    }
// SET AND GET ENTITY_RSIMPLE_NAME..............................................

    public void setEntityRSimpleName(String newName) {
        this.ENTITY_RS_NAME = newName;
    }
// SET AND GET ENTITY_ACTOR_NAME................................................

    public void setEntityActorName(String newName) {
        this.ENTITY_A_NAME = newName;
    }
// SET AND GET ENTITY_HISTORIA_NAME.............................................

    public void setEntityHistoriaName(String newName) {
        this.ENTITY_H_NAME = newName;
    }
// SET AND GET ENTITY_BASICO_NAME...............................................

    public void setEntityBasicoName(String newName) {
        this.ENTITY_B_NAME = newName;
    }

    private void agregarNuevosFKsAlaEntidadCCRS(LinkedList listFKs, String entityRelationName, LinkedList listFKsD, String entityDesignateName, String entityType) {
        LinkedList fksMList = retornarListaFKsDeLaEntidadCCRS(listFKs, entityRelationName, entityType);
        for (int i = 0; i < fksMList.size(); i++) {
            LinkedList newFK = new LinkedList();
            LinkedList fkList = (LinkedList) fksMList.get(i);
            String attribute = (String) fkList.get(3);
            newFK.add(entityRelationName);
            newFK.add(attribute);
            newFK.add("true");
            newFK.add(entityRelationName + "_" + attribute);
            newFK.add(entityDesignateName);
            if (estadoExistenciaFK(listFKsD, entityRelationName , entityDesignateName, entityRelationName + "_" + attribute) == false) {
                listFKsD.add(newFK);
            }
        }
    }
    
    private void agregarNuevosFKsAlaEntidad(LinkedList listFKs, String entityRelationName, LinkedList listFKsD, String entityDesignateName) {
        LinkedList fksMList = retornarListaFKsDeLaEntidad(listFKs, entityRelationName);
        for (int i = 0; i < fksMList.size(); i++) {
            LinkedList newFK = new LinkedList();
            LinkedList fkList = (LinkedList) fksMList.get(i);
            String attribute = (String) fkList.get(3);
            newFK.add(entityRelationName);
            newFK.add(attribute);
            newFK.add("true");
            newFK.add(entityRelationName + "_" + attribute);
            newFK.add(entityDesignateName);
            if (estadoExistenciaFK(listFKsD, entityRelationName , entityDesignateName, entityRelationName + "_" + attribute) == false) {
                listFKsD.add(newFK);
            }
        }
    }
    
    private void agregarNuevosFKsAlaEntidadFuerte(LinkedList listFKs, String entityRelationName, LinkedList listFKsD, String entityDesignateName) {
        LinkedList fksMList = retornarListaFKsDeLaEntidad(listFKs, entityDesignateName);
        for (int i = 0; i < fksMList.size(); i++) {
            LinkedList newFK = new LinkedList();
            LinkedList fkList = (LinkedList) fksMList.get(i);
            String attribute = (String) fkList.get(3);
            newFK.add(entityDesignateName);
            newFK.add(attribute);
            newFK.add("true");
            newFK.add(entityDesignateName + "_" + attribute);
            newFK.add(entityRelationName);
            if (estadoExistenciaFK(listFKsD, entityDesignateName , entityRelationName , entityDesignateName + "_" + attribute) == false) {
                listFKsD.add(newFK);
            }
        }
    }

    private LinkedList retornarListaFKsDeLaEntidadCCRS(LinkedList listFKs, String entityName, String entityType) {
        LinkedList res = new LinkedList();
        for (int i = 0; i < listFKs.size(); i++) {
            LinkedList myAuxList = (LinkedList) listFKs.get(i);
            String valueName1 = (String) myAuxList.get(2);
            String valueName2 = (String) myAuxList.get(4);
            if(!valueName1.equals("Clasificador") && valueName2.equals(entityName) && !valueName1.equals("RSimple")){
                res.add(myAuxList);
            }
        }
        return res;
    }

    private LinkedList retornarListaFKsDeLaEntidad(LinkedList listFKs, String entityName) {
        LinkedList res = new LinkedList();
        for (int i = 0; i < listFKs.size(); i++) {
            LinkedList myAuxList = (LinkedList) listFKs.get(i);
            String valueName1 = (String) myAuxList.get(2);
            String valueName2 = (String) myAuxList.get(4);
            if(!valueName1.equals("Clasificador") && valueName2.equals(entityName) && !valueName1.equals("RSimple")){
                res.add(myAuxList);
            }
        }
        return res;
    }

    private boolean estadoExistenciaFK(LinkedList listFKs, String entityName1, String entityName2, String codeName) {
        boolean res = false;
        for (int i = 0; i < listFKs.size(); i++) {
            LinkedList listFK = (LinkedList) listFKs.get(i);
            String valueDate1 = (String) listFK.get(0);
            String valueDate2 = (String) listFK.get(3);
            String valueDate3 = (String) listFK.get(4);
            if (entityName1.equals(valueDate1) && codeName.equals(valueDate2) && entityName2.equals(valueDate3)) {
                res = true;
                //break;
            }
        }
        return res;
    }
}
