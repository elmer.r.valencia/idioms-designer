package Export;

import Container.EntityContainer;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;

public class ExportPgSQL {

    private static final String CREATE_TABLE = "CREATE TABLE";

    private static final String NOT_NULL = "NOT NULL";

    private static final String NULL = "NULL";

    private static final String PRIMARY_KEY = "PRIMARY KEY";

    private static final String FOREIGN_KEY = "FOREIGN KEY";

    private static final String REFERENCES = "REFERENCES";

    private static final String ON_DELETE = "ON DELETE";

    private static final String ON_UPDATE = "ON UPDATE";

    private static final String SPACE = " ";

    private static final String INDENT = "  ";

    private static final String COMMA = ",";

    private static final String OPEN_BRACKET = "(";

    private static final String CLOSE_BRACKET = ")";

    private static final String SEMICOLON = ";";

    private static final String SERIAL = "SERIAL";

    private static final String COMMENT = "--";

    private LinkedList ENTITY_0_LIST, ENTITY_1_LIST, ENTITY_2_LIST, ENTITY_3_LIST, ENTITY_4_LIST, ENTITY_5_LIST, ENTITY_6_LIST, ENTITY_7_LIST, ENTITY_8_LIST, ENTITY_9_LIST, ENTITY_10_LIST;

    private int COUNTER = 0;
    
    private String VALUE_ATTRIBUTE;

    public ExportPgSQL() {
    }

    public boolean exportar(EntityContainer entityContainer, File file) throws IOException {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file));
            writer.write(COMMENT);
            writer.write(SPACE);
            writer.write("-------------------------------------------------------------------------");
            writer.newLine();
            writer.write(COMMENT);
            writer.write(SPACE);
            writer.write("PostgreSQL SQL");
            writer.newLine();
            writer.write(COMMENT);
            writer.write(SPACE);
            Date dt = new Date();
            writer.write("Exported at " + dt + " with IdiomDesigner");
            writer.newLine();
            writer.write(COMMENT);
            writer.write(SPACE);
            writer.write("-------------------------------------------------------------------------");
            writer.newLine();
            writer.newLine();
            generarSQLDeLaLista(writer, entityContainer);
        } catch (IOException exc) {
            throw exc;
        } finally {
            try {
                writer.close();
            } catch (IOException exc) {
                throw exc;
            }
        }
        return true;
    }

    private void generarSQLDeLaLista(BufferedWriter writer, EntityContainer entityContainer) throws IOException {
        //(FKs = 0) ENTITYLIST..................................................
        ENTITY_0_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 0);
        //(FKs = 1) ENTITYLIST..................................................
        ENTITY_1_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 1);
        //(FKs = 2) ENTITYLIST..................................................
        ENTITY_2_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 2);
        //(FKs = 3) ENTITYLIST..................................................
        ENTITY_3_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 3);
        //(FKs = 4) ENTITYLIST..................................................
        ENTITY_4_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 4);
        //(FKs = 5) ENTITYLIST..................................................
        ENTITY_5_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 5);
        //(FKs = 6) ENTITYLIST..................................................
        ENTITY_6_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 6);
        //(FKs = 7) ENTITYLIST..................................................
        ENTITY_7_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 7);
        //(FKs = 8) ENTITYLIST..................................................
        ENTITY_8_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 8);
        //(FKs = 9) ENTITYLIST..................................................
        ENTITY_9_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 9);
        //(FKs = 10) ENTITYLIST.................................................
        ENTITY_10_LIST = retornarListaDeEntidadesPorNumeroDeFKs(entityContainer, 10);
        //CODE WRITE (FK = 0)...................................................
        generaCodigoSQLDeLaLista(writer, ENTITY_0_LIST);
        //CODE WRITE (FK = 1)...................................................
        generaCodigoSQLDeLaListaConFKs(writer, ENTITY_1_LIST);
        //CODE WRITE (FK = 2)...................................................
        generaCodigoSQLDeLaListaConFKs(writer, ENTITY_2_LIST);
        //CODE WRITE (FK = 3)...................................................
        generaCodigoSQLDeLaListaConFKs(writer, ENTITY_3_LIST);
        //CODE WRITE (FK = 4)...................................................
        generaCodigoSQLDeLaListaConFKs(writer, ENTITY_4_LIST);
        //CODE WRITE (FK = 5)...................................................
        generaCodigoSQLDeLaListaConFKs(writer, ENTITY_5_LIST);
        //CODE WRITE (FK = 6)...................................................
        generaCodigoSQLDeLaListaConFKs(writer, ENTITY_6_LIST);
        //CODE WRITE (FK = 7)...................................................
        generaCodigoSQLDeLaListaConFKs(writer, ENTITY_7_LIST);
        //CODE WRITE (FK = 8)...................................................
        generaCodigoSQLDeLaListaConFKs(writer, ENTITY_8_LIST);
        //CODE WRITE (FK = 9)...................................................
        generaCodigoSQLDeLaListaConFKs(writer, ENTITY_9_LIST);
        //CODE WRITE (FK = 10)...................................................
        generaCodigoSQLDeLaListaConFKs(writer, ENTITY_10_LIST);
    }

    private LinkedList retornarListaDeEntidadesPorNumeroDeFKs(EntityContainer entityContainer, int numberFks) {
        LinkedList res = new LinkedList();
        //ENTITY TYPE LIST......................................................
        LinkedList maestroList = (LinkedList) entityContainer.getMaestro();
        LinkedList detalleList = (LinkedList) entityContainer.getDetalle();
        LinkedList clasificadorList = (LinkedList) entityContainer.getClasificador();
        LinkedList clasificadoList = (LinkedList) entityContainer.getClasificado();
        LinkedList composicionList = (LinkedList) entityContainer.getComposicion();
        LinkedList componedorList = (LinkedList) entityContainer.getComponedor();
        LinkedList generalList = (LinkedList) entityContainer.getGeneral();
        LinkedList isaList = (LinkedList) entityContainer.getIsa();
        LinkedList rSimpleList = (LinkedList) entityContainer.getRSimple();
        LinkedList actorList = (LinkedList) entityContainer.getActor();
        LinkedList historiaList = (LinkedList) entityContainer.getHistoria();
        LinkedList basicoList = (LinkedList) entityContainer.getBasico();
        //FKs LIST..............................................................
        LinkedList maestroFKsList = (LinkedList) entityContainer.getMLlaveSegundaria();
        LinkedList detalleFKsList = (LinkedList) entityContainer.getDLlaveSegundaria();
        LinkedList clasificadorFKsList = (LinkedList) entityContainer.getCORLlaveSegundaria();
        LinkedList clasificadoFKsList = (LinkedList) entityContainer.getCDOLlaveSegundaria();
        LinkedList composicionFKsList = (LinkedList) entityContainer.getCLlaveSegundaria();
        LinkedList componedorFKsList = (LinkedList) entityContainer.getCCLlaveSegundaria();
        LinkedList generalFKsList = (LinkedList) entityContainer.getGLlaveSegundaria();
        LinkedList isaFKsList = (LinkedList) entityContainer.getILlaveSegundaria();
        LinkedList rSimpleFKsList = (LinkedList) entityContainer.getRSLlaveSegundaria();
        LinkedList actorFKsList = (LinkedList) entityContainer.getALlaveSegundaria();
        LinkedList historiaFKsList = (LinkedList) entityContainer.getHLlaveSegundaria();
        LinkedList basicoFKsList = (LinkedList) entityContainer.getBLlaveSegundaria();
        //METODOS...............................................................
        rellenarLista(res, maestroList, maestroFKsList, numberFks);
        rellenarLista(res, detalleList, detalleFKsList, numberFks);
        rellenarLista(res, clasificadorList, clasificadorFKsList, numberFks);
        rellenarLista(res, clasificadoList, clasificadoFKsList, numberFks);
        rellenarLista(res, composicionList, composicionFKsList, numberFks);
        rellenarLista(res, componedorList, componedorFKsList, numberFks);
        rellenarLista(res, generalList, generalFKsList, numberFks);
        rellenarLista(res, isaList, isaFKsList, numberFks);
        rellenarLista(res, rSimpleList, rSimpleFKsList, numberFks);
        rellenarLista(res, actorList, actorFKsList, numberFks);
        rellenarLista(res, historiaList, historiaFKsList, numberFks);
        rellenarLista(res, basicoList, basicoFKsList, numberFks);
        return res;
    }

    private void rellenarLista(LinkedList res, LinkedList List, LinkedList fKsList, int numberFks) {
        for (int i = 0; i < List.size(); i++) {
            LinkedList myAuxList = (LinkedList) List.get(i);
            LinkedList myFKsList = new LinkedList();
            String valueAttribute = (String) myAuxList.get(0);
            for (int j = 0; j < fKsList.size(); j++) {
                LinkedList fkList = (LinkedList) fKsList.get(j);
                String valueAttributeEnd = (String) fkList.get(4);
                if (valueAttribute.equals(valueAttributeEnd)) {
                    myFKsList.add(fkList);
                    COUNTER++;
                }
            }
            if (COUNTER == numberFks) {
                myAuxList.add(myFKsList);
                res.add(myAuxList);
            }
            COUNTER = 0;
        }
    }

    private void generaCodigoSQLDeLaLista(BufferedWriter writer, LinkedList entityList) throws IOException {
        //CODE WRITE............................................................
        for (int i = 0; i < entityList.size(); i++) {
            LinkedList myList = (LinkedList) entityList.get(i);
            String entityName = (String) myList.get(0);
            //GRATE TABLE ENTITYNAME (..........................................
            writer.write(CREATE_TABLE);
            writer.write(SPACE);
            writer.write(entityName);
            writer.write(SPACE);
            writer.write(OPEN_BRACKET);
            writer.newLine();
            //ATTRIBUTES........................................................
            LinkedList entityAttributeList = (LinkedList) myList.get(1);
            for (int j = 0; j < entityAttributeList.size(); j = j + 5) {
                //ATTRIBUTE NAME(COLUMN ATTRIBUTE)..............................
                String valueAttribute = (String) entityAttributeList.get(j);
                writer.write(INDENT);
                writer.write(valueAttribute);
                writer.write(SPACE);
                //AUTO IMCREMENT OR SERIAL (COLUMN DATE TYPE)...................
                String stateAI = (String) entityAttributeList.get(j + 4);
                String statePK = (String) entityAttributeList.get(j + 2);
                String stateDataType = (String) entityAttributeList.get(j + 1);
                if (stateAI.equals("true") && statePK.equals("true")) {
                    writer.write(SERIAL);
                    writer.write(SPACE);
                } else {
                    writer.write(stateDataType);
                    writer.write(SPACE);
                }
                //NULL OR NOT NULL (COLUMN NOT NULL)............................
                String stateNotNull = (String) entityAttributeList.get(j + 3);
                if (stateNotNull.equals("true")) {
                    writer.write(NOT_NULL);
                } else {
                    writer.write(NULL);
                }
                //COMMA.........................................................
                if (j < (entityAttributeList.size() - 1)) {
                    writer.write(COMMA);
                    writer.newLine();
                }
            }
            //PRIMARY_KEY (.....................................................
            String statePK = (String) entityAttributeList.get(2);
            String valueAttribute = (String) entityAttributeList.get(0);
            if (statePK.equals("true")) {
                writer.write(INDENT);
                writer.write(PRIMARY_KEY);
                writer.write(OPEN_BRACKET);
                writer.write(valueAttribute);
                writer.write(CLOSE_BRACKET);
                writer.newLine();
            }
            writer.write(CLOSE_BRACKET);
            writer.write(SEMICOLON);
            writer.newLine();
            writer.newLine();
        }
    }

    private void generaCodigoSQLDeLaListaConFKs(BufferedWriter writer, LinkedList entityList) throws IOException {
        //CODE WRITE............................................................
        for (int i = 0; i < entityList.size(); i++) {
            LinkedList FOREIGN_KEY_LIST = new LinkedList();
            LinkedList REFERENCE_LIST = new LinkedList();
            boolean optionFK = true;
            boolean optionPK = true;
            boolean optionPK2 = true;
            boolean optionPK3 = true;
            LinkedList myList = (LinkedList) entityList.get(i);
            String entityName = (String) myList.get(0);
            //GRATE TABLE ENTITYNAME............................................
            writer.write(CREATE_TABLE);
            writer.write(SPACE);
            writer.write(entityName);
            writer.write(SPACE);
            writer.write(OPEN_BRACKET);
            writer.newLine();
            //ATTRIBUTES........................................................
            LinkedList entityAttributeList = (LinkedList) myList.get(1);
            LinkedList primaryIndexList = (LinkedList) myList.get(2);
            String valueAtributePK = (String) entityAttributeList.get(2);
            if (!valueAtributePK.equals("true")) {
                //LLAVES FORANEAS...............................................
                for (int k = 0; k < primaryIndexList.size(); k++) {
                    LinkedList myFKList = (LinkedList) primaryIndexList.get(k);
                    String valueAttributeFK = (String) myFKList.get(3);
                    writer.write(INDENT);
                    writer.write(valueAttributeFK);
                    String valueAttributeEntitype = (String) myFKList.get(2);
                    String valueCodeAttribute = (String) myFKList.get(1);
                    if (!valueAttributeEntitype.equals("Clasificador") && !valueAttributeEntitype.equals("RSimple")) {
                        FOREIGN_KEY_LIST.add(valueAttributeFK);
                        REFERENCE_LIST.add(valueCodeAttribute);
                    }
                    writer.write(SPACE);
                    writer.write("INTEGER");
                    writer.write(SPACE);
                    writer.write(NOT_NULL);
                    if (k < (primaryIndexList.size() - 1)) {
                        writer.write(COMMA);
                        writer.newLine();
                    }
                }
                writer.write(COMMA);
                writer.newLine();
                if (0 != entityAttributeList.size()) {
                    for (int j = 0; j < entityAttributeList.size(); j = j + 5) {
                        //ATTRIBUTE NAME(COLUMN ATTRIBUTE)..............................
                        String valueAttribute = (String) entityAttributeList.get(j);
                        String statePK = (String) entityAttributeList.get(j + 2);
                        if (optionPK == true) {
                            VALUE_ATTRIBUTE = "";
                            optionPK = false;
                        }
                        writer.write(INDENT);
                        writer.write(valueAttribute);
                        writer.write(SPACE);
                        //AUTO IMCREMENT OR SERIAL (COLUMN DATE TYPE)...................
                        String stateAI = (String) entityAttributeList.get(j + 4);
                        String stateDataType = (String) entityAttributeList.get(j + 1);
                        String statePKs = (String) entityAttributeList.get(j + 2);
                        if (stateAI.equals("true") && statePKs.equals("true")) {
                            writer.write(SERIAL);
                            writer.write(SPACE);
                        } else {
                            writer.write(stateDataType);
                            writer.write(SPACE);
                        }
                        //NULL OR NOT NULL (COLUMN NOT NULL)............................
                        String stateNotNull = (String) entityAttributeList.get(j + 3);
                        if (stateNotNull.equals("true")) {
                            writer.write(NOT_NULL);
                        } else {
                            writer.write(NULL);
                        }
                        //COMMA.....................................................
                        if (statePK.equals("false")) {
                            writer.write(COMMA);
                            writer.newLine();
                        } else {
                            if (j < (entityAttributeList.size() - 5)) {
                                writer.write(COMMA);
                                writer.newLine();
                            }
                        }
                    }
                }
            } else {
                for (int j = 0; j < entityAttributeList.size(); j = j + 5) {
                    //ATTRIBUTE NAME(COLUMN ATTRIBUTE)..............................
                    String valueAttribute = (String) entityAttributeList.get(j);
                    String statePK = (String) entityAttributeList.get(j + 2);
                    if (optionPK3 == true) {
                        VALUE_ATTRIBUTE = valueAttribute;
                        optionPK3 = false;
                    }
                    writer.write(INDENT);
                    writer.write(valueAttribute);
                    writer.write(SPACE);
                    //AUTO IMCREMENT OR SERIAL (COLUMN DATE TYPE)...................
                    String stateAI = (String) entityAttributeList.get(j + 4);
                    String stateDataType = (String) entityAttributeList.get(j + 1);
                    String statePKss = (String) entityAttributeList.get(j + 2);
                    if (stateAI.equals("true") && statePKss.equals("true")) {
                        writer.write(SERIAL);
                        writer.write(SPACE);
                    } else {
                        writer.write(stateDataType);
                        writer.write(SPACE);
                    }
                    //NULL OR NOT NULL (COLUMN NOT NULL)............................
                    String stateNotNull = (String) entityAttributeList.get(j + 3);
                    if (stateNotNull.equals("true")) {
                        writer.write(NOT_NULL);
                    } else {
                        writer.write(NULL);
                    }
                    //ATTRIBUTES FOREINGN KEYS......................................
                    if (optionFK == true) {
                        writer.write(COMMA);
                        writer.newLine();
                        for (int k = 0; k < primaryIndexList.size(); k++) {
                            LinkedList myFKList = (LinkedList) primaryIndexList.get(k);
                            String valueAttributeFK = (String) myFKList.get(3);
                            writer.write(INDENT);
                            writer.write(valueAttributeFK);
                            String valueAttributeEntitype = (String) myFKList.get(2);
                            String valueCodeAttribute = (String) myFKList.get(1);
                            if (!valueAttributeEntitype.equals("Clasificador") && !valueAttributeEntitype.equals("RSimple")) {
                                FOREIGN_KEY_LIST.add(valueAttributeFK);
                                REFERENCE_LIST.add(valueCodeAttribute);
                            }
                            writer.write(SPACE);
                            writer.write("INTEGER");
                            writer.write(SPACE);
                            writer.write(NOT_NULL);
                            if (k < (primaryIndexList.size() - 1)) {
                                writer.write(COMMA);
                                writer.newLine();
                            }
                        }
                        optionFK = false;
                    }
                    //COMMA.....................................................
                    if (statePK.equals("true")) {
                        writer.write(COMMA);
                        writer.newLine();
                    } else {
                        if (j < (entityAttributeList.size() - 5)) {
                            writer.write(COMMA);
                            writer.newLine();
                            optionPK2 = false;
                        }
                    }
                }
            }
            //PRIMARY KEY.......................................................
            if (!primaryIndexList.isEmpty()) {
                if (optionPK2 == false) {
                    writer.write(COMMA);
                    writer.newLine();
                }
                writer.write(INDENT);
                writer.write(PRIMARY_KEY);
                writer.write(SPACE);
                writer.write(OPEN_BRACKET);
                if (!VALUE_ATTRIBUTE.equals("")) {
                    writer.write(VALUE_ATTRIBUTE);
                    if (0 != FOREIGN_KEY_LIST.size()) {
                        writer.write(COMMA);
                        writer.write(SPACE);
                    }
                }
                for (int k = 0; k < FOREIGN_KEY_LIST.size(); k++) {
                    String valueAttributeFK2 = (String) FOREIGN_KEY_LIST.get(k);
                    writer.write(valueAttributeFK2);
                    if (k < (FOREIGN_KEY_LIST.size() - 1)) {
                        writer.write(COMMA);
                        writer.write(SPACE);
                    }
                }
                writer.write(CLOSE_BRACKET);
                writer.write(COMMA);
                writer.newLine();
            }
            LinkedList ADD_FK_LIST = retornarAddFKList(primaryIndexList);
            //FOREINGN KEYS.....................................................
            for (int j = 0; j < ADD_FK_LIST.size(); j++) {
                //FOREIGN_KEY...................................................
                LinkedList myFKList = (LinkedList) ADD_FK_LIST.get(j);
                writer.write(INDENT);
                writer.write(FOREIGN_KEY);
                writer.write(SPACE);
                writer.write(OPEN_BRACKET);
                String valueAttributeFK = (String) myFKList.get(3);
                writer.write(valueAttributeFK);
                //COMMA.........................................................
                if (j == (ADD_FK_LIST.size())) {
                    writer.write(COMMA);
                    writer.write(SPACE);
                }
                //REFERENCES....................................................
                String entityNameFK = (String) myFKList.get(0);
                writer.write(CLOSE_BRACKET);
                writer.newLine();
                writer.write(INDENT);
                writer.write(INDENT);
                writer.write(REFERENCES);
                writer.write(SPACE);
                writer.write(entityNameFK);
                writer.write(SPACE);
                writer.write(OPEN_BRACKET);
                String valueAttributeCodeFK = (String) myFKList.get(1);
                writer.write(valueAttributeCodeFK);
                //COMMA.........................................................
                if (j == (ADD_FK_LIST.size())) {
                    writer.write(COMMA);
                    writer.write(SPACE);
                }
                writer.write(CLOSE_BRACKET);
                writer.newLine();
                //ON DELETE NO ACTION...........................................
                writer.write(INDENT);
                writer.write(INDENT);
                writer.write(INDENT);
                writer.write(ON_DELETE);
                writer.write(SPACE);
                writer.write("NO ACTION");
                writer.newLine();
                //ON UPDATE NO ACTION...........................................
                writer.write(INDENT);
                writer.write(INDENT);
                writer.write(INDENT);
                writer.write(ON_UPDATE);
                writer.write(SPACE);
                writer.write("NO ACTION");
                //COMMA.........................................................
                if (j < (ADD_FK_LIST.size() - 1)) {
                    writer.write(COMMA);
                    writer.newLine();
                }
            }
            writer.newLine();
            writer.write(CLOSE_BRACKET);
            writer.write(SEMICOLON);
            writer.newLine();
            writer.newLine();
        }
    }

    private LinkedList retornarAddFKList(LinkedList primaryIndexList) {
        LinkedList res = new LinkedList();
        for (int i = 0; i < primaryIndexList.size(); i++) {
            LinkedList myAuxList = (LinkedList) primaryIndexList.get(i);
            String valueEntityName = (String) myAuxList.get(0);
            if (existevalorDeLaEntidad(valueEntityName, res) == false) {
                res.add(myAuxList);
            }
            for (int j = 0; j < primaryIndexList.size(); j++) {
                LinkedList myAuxList2 = (LinkedList) primaryIndexList.get(i);
                String valueEntityName2 = (String) myAuxList2.get(0);
                if (!valueEntityName.equals(valueEntityName2)) {
                    res.add(myAuxList2);
                }
            }
        }
        return res;
    }

    private boolean existevalorDeLaEntidad(String entityName, LinkedList list) {
        boolean res = false;
        for (int i = 0; i < list.size(); i++) {
            LinkedList myList = (LinkedList) list.get(i);
            String valueEntityName = (String) myList.get(0);
            if (valueEntityName.equals(entityName)) {
                res = true;
            }
        }
        return res;
    }
}
