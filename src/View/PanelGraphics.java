/**
 * @author Elmer Valencia Ledezmas / gmail: elmer.r.valencia@gmail.com / cel:
 * 71413596
 */
package View;

import Container.EntityContainer;
import Model.IdiomsModel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JPanel;

public class PanelGraphics extends JPanel implements MouseListener, MouseMotionListener {

    //ENTIDAD MD1:
    private int X_MD1, X_MD2;
    private int Y_MD1, Y_MD2;
    private int WIDTH_MD1 = 100, WIDTH_MD2 = 100;
    private int HEIGHT_MD1 = 80, HEIGHT_MD2 = 80;
    //ENTIDAD MD2:
    private int X_MD3, X_MD4;
    private int Y_MD3, Y_MD4;
    private int WIDTH_MD3 = 100, WIDTH_MD4 = 100;
    private int HEIGHT_MD3 = 80, HEIGHT_MD4 = 80;
    //ENTIDAD CC1:
    private int X_CC1, X_CC2;
    private int Y_CC1, Y_CC2;
    private int WIDTH_CC1 = 100, WIDTH_CC2 = 100;
    private int HEIGHT_CC1 = 80, HEIGHT_CC2 = 80;
    //ENTIDAD CC2:
    private int X_CC3, X_CC4;
    private int Y_CC3, Y_CC4;
    private int WIDTH_CC3 = 100, WIDTH_CC4 = 100;
    private int HEIGHT_CC3 = 80, HEIGHT_CC4 = 80;
    //ENTIDAD C1:
    private int X_C1, X_C2, X_C3;
    private int Y_C1, Y_C2, Y_C3;
    private int WIDTH_C1 = 100, WIDTH_C2 = 100, WIDTH_C3 = 100;
    private int HEIGHT_C1 = 80, HEIGHT_C2 = 80, HEIGHT_C3 = 80;
    //ENTIDAD C2:
    private int X_C4, X_C5, X_C6;
    private int Y_C4, Y_C5, Y_C6;
    private int WIDTH_C4 = 100, WIDTH_C5 = 100, WIDTH_C6 = 100;
    private int HEIGHT_C4 = 80, HEIGHT_C5 = 80, HEIGHT_C6 = 80;
    //ENTIDAD ISA1:
    private int X_ISA1, X_ISA2;
    private int Y_ISA1, Y_ISA2;
    private int WIDTH_ISA1 = 100, WIDTH_ISA2 = 100;
    private int HEIGHT_ISA1 = 80, HEIGHT_ISA2 = 80;
    //ENTIDAD ISA2:
    private int X_ISA3, X_ISA4;
    private int Y_ISA3, Y_ISA4;
    private int WIDTH_ISA3 = 100, WIDTH_ISA4 = 100;
    private int HEIGHT_ISA3 = 80, HEIGHT_ISA4 = 80;
    //ENTIDAD RS1:
    private int X_RS1;
    private int Y_RS1;
    private int WIDTH_RS1 = 100;
    private int HEIGHT_RS1 = 80;
    //ENTIDAD RS2:
    private int X_RS2;
    private int Y_RS2;
    private int WIDTH_RS2 = 100;
    private int HEIGHT_RS2 = 80;
    //ENTIDAD RH1:
    private int X_RH1, X_RH2;
    private int Y_RH1, Y_RH2;
    private int WIDTH_RH1 = 100, WIDTH_RH2 = 100;
    private int HEIGHT_RH1 = 80, HEIGHT_RH2 = 80;
    //ENTIDAD RH2:
    private int X_RH3, X_RH4;
    private int Y_RH3, Y_RH4;
    private int WIDTH_RH3 = 100, WIDTH_RH4 = 100;
    private int HEIGHT_RH3 = 80, HEIGHT_RH4 = 80;
    //ENTIDAD B1:
    private int X_B;
    private int Y_B;
    private int WIDTH_B = 100;
    private int HEIGHT_B = 80;
    //ENTIDAD B2:
    private int X_B1;
    private int Y_B1;
    private int WIDTH_B1 = 100;
    private int HEIGHT_B1 = 80;
    //ENTIDAD 1 MAESTROS:
    private int X_ML, X_MR, X_MU, X_MB;
    private int Y_ML, Y_MR, Y_MU, Y_MB;
    private int WIDTH_ML = 100, WIDTH_MR = 100, WIDTH_MU = 100, WIDTH_MB = 100;
    private int HEIGHT_ML = 80, HEIGHT_MR = 80, HEIGHT_MU = 80, HEIGHT_MB = 80;
    //ENTIDAD 2 DETALLES:
    private int X_DL, X_DR, X_DU, X_DB;
    private int Y_DL, Y_DR, Y_DU, Y_DB;
    private int WIDTH_DL = 100, WIDTH_DR = 100, WIDTH_DU = 100, WIDTH_DB = 100;
    private int HEIGHT_DL = 80, HEIGHT_DR = 80, HEIGHT_DU = 80, HEIGHT_DB = 80;
    //ENTIDAD 3 CLASIFICADORES:
    private int X_CLRL, X_CLRR, X_CLRU, X_CLRB;
    private int Y_CLRL, Y_CLRR, Y_CLRU, Y_CLRB;
    private int WIDTH_CLRL = 100, WIDTH_CLRR = 100, WIDTH_CLRU = 100, WIDTH_CLRB = 100;
    private int HEIGHT_CLRL = 80, HEIGHT_CLRR = 80, HEIGHT_CLRU = 80, HEIGHT_CLRB = 80;
    //ENTIDAD 4 CLASIFICADORES:
    private int X_CLOL, X_CLOR, X_CLOU, X_CLOB;
    private int Y_CLOL, Y_CLOR, Y_CLOU, Y_CLOB;
    private int WIDTH_CLOL = 100, WIDTH_CLOR = 100, WIDTH_CLOU = 100, WIDTH_CLOB = 100;
    private int HEIGHT_CLOL = 80, HEIGHT_CLOR = 80, HEIGHT_CLOU = 80, HEIGHT_CLOB = 80;
    //ENTIDAD 5 COMPOSICIONES:
    private int X_CONL, X_CONR, X_CONU, X_CONB;
    private int Y_CONL, Y_CONR, Y_CONU, Y_CONB;
    private int WIDTH_CONL = 100, WIDTH_CONR = 100, WIDTH_CONU = 100, WIDTH_CONB = 100;
    private int HEIGHT_CONL = 80, HEIGHT_CONR = 80, HEIGHT_CONU = 80, HEIGHT_CONB = 80;
    //ENTIDAD 6 COMPONENTES:
    private int X_CORL, X_CORR, X_CORU, X_CORB;
    private int Y_CORL, Y_CORR, Y_CORU, Y_CORB;
    private int WIDTH_CORL = 100, WIDTH_CORR = 100, WIDTH_CORU = 100, WIDTH_CORB = 100;
    private int HEIGHT_CORL = 80, HEIGHT_CORR = 80, HEIGHT_CORU = 80, HEIGHT_CORB = 80;
    //ENTIDAD 7 GENERALES:
    private int X_GL, X_GR, X_GU, X_GB;
    private int Y_GL, Y_GR, Y_GU, Y_GB;
    private int WIDTH_GL = 100, WIDTH_GR = 100, WIDTH_GU = 100, WIDTH_GB = 100;
    private int HEIGHT_GL = 80, HEIGHT_GR = 80, HEIGHT_GU = 80, HEIGHT_GB = 80;
    //ENTIDAD 8 ISAS:
    private int X_IL, X_IR, X_IU, X_IB;
    private int Y_IL, Y_IR, Y_IU, Y_IB;
    private int WIDTH_IL = 100, WIDTH_IR = 100, WIDTH_IU = 100, WIDTH_IB = 100;
    private int HEIGHT_IL = 80, HEIGHT_IR = 80, HEIGHT_IU = 80, HEIGHT_IB = 80;
    //ENTIDAD 9 REFLEXSIVOS SIMPLES:
    private int X_SL, X_SR, X_SU, X_SB;
    private int Y_SL, Y_SR, Y_SU, Y_SB;
    private int WIDTH_SL = 100, WIDTH_SR = 100, WIDTH_SU = 100, WIDTH_SB = 100;
    private int HEIGHT_SL = 80, HEIGHT_SR = 80, HEIGHT_SU = 80, HEIGHT_SB = 80;
    //ENTIDAD 10 ACTOR:
    private int X_AL, X_AR, X_AU, X_AB;
    private int Y_AL, Y_AR, Y_AU, Y_AB;
    private int WIDTH_AL = 100, WIDTH_AR = 100, WIDTH_AU = 100, WIDTH_AB = 100;
    private int HEIGHT_AL = 80, HEIGHT_AR = 80, HEIGHT_AU = 80, HEIGHT_AB = 80;
    //ENTIDAD 11 HISTORIAS:
    private int X_HL, X_HR, X_HU, X_HB;
    private int Y_HL, Y_HR, Y_HU, Y_HB;
    private int WIDTH_HL = 100, WIDTH_HR = 100, WIDTH_HU = 100, WIDTH_HB = 100;
    private int HEIGHT_HL = 80, HEIGHT_HR = 80, HEIGHT_HU = 80, HEIGHT_HB = 80;

    //ESTADO DE LAS ENTIDADES:
    private boolean MOVED_MD1 = false, PERMISSION_MD = true;
    private boolean MOVED_MD2 = false;
    private boolean MOVED_CC1 = false, PERMISSION_CC = true;
    private boolean MOVED_CC2 = false;
    private boolean MOVED_C1 = false, PERMISSION_C = true;
    private boolean MOVED_C2 = false;
    private boolean MOVED_C3 = false;
    private boolean MOVED_ISA1 = false, PERMISSION_ISA = true;
    private boolean MOVED_ISA2 = false;
    private boolean MOVED_RS1 = false, PERMISSION_RS = true;
    private boolean MOVED_RH1 = false, PERMISSION_RH = true;
    private boolean MOVED_RH2 = false;
    private boolean MOVED_B = false, PERMISSION_B = true;

    private List<IdiomsModel> LIST_IDIOMS;

    private IdiomsModel IDIOMS_MODEL;
    private IdiomsModel IM_MD, IM_CC, IM_C, IM_ISA, IM_RS, IM_RH, IM_B;
    private IdiomsModel IM_MD1, IM_CC1, IM_C1, IM_ISA1, IM_RS1, IM_RH1, IM_B1;
    private IdiomsModel IM_MAESTRO_LEFT, IM_MAESTRO_RIGHT, IM_MAESTRO_UP, IM_MAESTRO_BELOW;
    private IdiomsModel IM_DETALLE_LEFT, IM_DETALLE_RIGHT, IM_DETALLE_UP, IM_DETALLE_BELOW;
    private IdiomsModel IM_CLASIFICADOR_LEFT, IM_CLASIFICADOR_RIGHT, IM_CLASIFICADOR_UP, IM_CLASIFICADOR_BELOW;
    private IdiomsModel IM_CLASIFICADO_LEFT, IM_CLASIFICADO_RIGHT, IM_CLASIFICADO_UP, IM_CLASIFICADO_BELOW;
    private IdiomsModel IM_COMPOSICION_LEFT, IM_COMPOSICION_RIGHT, IM_COMPOSICION_UP, IM_COMPOSICION_BELOW;
    private IdiomsModel IM_COMPONEDOR_LEFT, IM_COMPONEDOR_RIGHT, IM_COMPONEDOR_UP, IM_COMPONEDOR_BELOW;
    private IdiomsModel IM_GENERAL_LEFT, IM_GENERAL_RIGHT, IM_GENERAL_UP, IM_GENERAL_BELOW;
    private IdiomsModel IM_IS_A_LEFT, IM_IS_A_RIGHT, IM_IS_A_UP, IM_IS_A_BELOW;
    private IdiomsModel IM_RSIMPLE_LEFT, IM_RSIMPLE_RIGHT, IM_RSIMPLE_UP, IM_RSIMPLE_BELOW;
    private IdiomsModel IM_ACTOR_LEFT, IM_ACTOR_RIGHT, IM_ACTOR_UP, IM_ACTOR_BELOW;
    private IdiomsModel IM_HISTORIA_LEFT, IM_HISTORIA_RIGHT, IM_HISTORIA_UP, IM_HISTORIA_BELOW;

    private JPopupMenuEntityType JPPPMenu;
    private PanelGraphics PG;

//CONTADOR 
    private int CONTADOR_E;

    private EntityContainer ENTITY_CONTAINER;

    public PanelGraphics(EntityContainer eContainer) {
        initPanelGraphics();
        this.ENTITY_CONTAINER = eContainer;
        addMouseListener(new PopupTriggerListener());
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    private void initPanelGraphics() {
        this.LIST_IDIOMS = new ArrayList<IdiomsModel>();
        this.setBackground(Color.WHITE);
        this.PG = this;
    }

//CLASS POPUPTRIGGERLISTENER...................................................................................................
    class PopupTriggerListener extends MouseAdapter {

        public PopupTriggerListener() {
        }

        @Override
        public void mousePressed(MouseEvent ev) {
            if (ev.isPopupTrigger()) {
                JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
            }
        }

        @Override
        public void mouseReleased(MouseEvent ev) {
            if (ev.isPopupTrigger()) {
                try {              
                if (estaDentro(ev, X_ML, Y_ML, WIDTH_ML, HEIGHT_ML)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_ML, Y_ML, WIDTH_ML, HEIGHT_ML, CONTADOR_E, ENTITY_CONTAINER, IM_MAESTRO_LEFT.getNameEntityMaestro(), "Maestro");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_MR, Y_MR, WIDTH_MR, HEIGHT_MR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_MR, Y_MR, WIDTH_MR, HEIGHT_MR, CONTADOR_E, ENTITY_CONTAINER, IM_MAESTRO_RIGHT.getNameEntityMaestro(), "Maestro");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_MU, Y_MU, WIDTH_MU, HEIGHT_MU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_MU, Y_MU, WIDTH_MU, HEIGHT_MU, CONTADOR_E, ENTITY_CONTAINER, IM_MAESTRO_UP.getNameEntityMaestro(), "Maestro");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_MB, Y_MB, WIDTH_MB, HEIGHT_MB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_MB, Y_MB, WIDTH_MB, HEIGHT_MB, CONTADOR_E, ENTITY_CONTAINER, IM_MAESTRO_BELOW.getNameEntityMaestro(), "Maestro");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_DL, Y_DL, WIDTH_DL, HEIGHT_DL)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_DL, Y_DL, WIDTH_DL, HEIGHT_DL, CONTADOR_E, ENTITY_CONTAINER, IM_DETALLE_LEFT.getNameEntityDetalle(), "Detalle");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_DR, Y_DR, WIDTH_DR, HEIGHT_DR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_DR, Y_DR, WIDTH_DR, HEIGHT_DR, CONTADOR_E, ENTITY_CONTAINER, IM_DETALLE_RIGHT.getNameEntityDetalle(), "Detalle");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_DU, Y_DU, WIDTH_DU, HEIGHT_DU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_DU, Y_DU, WIDTH_DU, HEIGHT_DU, CONTADOR_E, ENTITY_CONTAINER, IM_DETALLE_UP.getNameEntityDetalle(), "Detalle");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_DB, Y_DB, WIDTH_DB, HEIGHT_DB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_DB, Y_DB, WIDTH_DB, HEIGHT_DB, CONTADOR_E, ENTITY_CONTAINER, IM_DETALLE_BELOW.getNameEntityDetalle(), "Detalle");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CLRL, Y_CLRL, WIDTH_CLRL, HEIGHT_CLRL)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CLRL, Y_CLRL, WIDTH_CLRL, HEIGHT_CLRL, CONTADOR_E, ENTITY_CONTAINER, IM_CLASIFICADOR_LEFT.getNameEntityClasificador(), "Clasificador");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CLRR, Y_CLRR, WIDTH_CLRR, HEIGHT_CLRR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CLRR, Y_CLRR, WIDTH_CLRR, HEIGHT_CLRR, CONTADOR_E, ENTITY_CONTAINER, IM_CLASIFICADOR_RIGHT.getNameEntityClasificador(), "Clasificador");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CLRU, Y_CLRU, WIDTH_CLRU, HEIGHT_CLRU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CLRU, Y_CLRU, WIDTH_CLRU, HEIGHT_CLRU, CONTADOR_E, ENTITY_CONTAINER, IM_CLASIFICADOR_UP.getNameEntityClasificador(), "Clasificador");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CLRB, Y_CLRB, WIDTH_CLRB, HEIGHT_CLRB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CLRB, Y_CLRB, WIDTH_CLRB, HEIGHT_CLRB, CONTADOR_E, ENTITY_CONTAINER, IM_CLASIFICADOR_BELOW.getNameEntityClasificador(), "Clasificador");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CLOL, Y_CLOL, WIDTH_CLOL, HEIGHT_CLOL)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CLOL, Y_CLOL, WIDTH_CLOL, HEIGHT_CLOL, CONTADOR_E, ENTITY_CONTAINER, IM_CLASIFICADO_LEFT.getNameEntityClasificado(), "Clasificado");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CLOR, Y_CLOR, WIDTH_CLOR, HEIGHT_CLOR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CLOR, Y_CLOR, WIDTH_CLOR, HEIGHT_CLOR, CONTADOR_E, ENTITY_CONTAINER, IM_CLASIFICADO_RIGHT.getNameEntityClasificado(), "Clasificado");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CLOU, Y_CLOU, WIDTH_CLOU, HEIGHT_CLOU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CLOU, Y_CLOU, WIDTH_CLOU, HEIGHT_CLOU, CONTADOR_E, ENTITY_CONTAINER, IM_CLASIFICADO_UP.getNameEntityClasificado(), "Clasificado");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CLOB, Y_CLOB, WIDTH_CLOB, HEIGHT_CLOB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CLOB, Y_CLOB, WIDTH_CLOB, HEIGHT_CLOB, CONTADOR_E, ENTITY_CONTAINER, IM_CLASIFICADO_BELOW.getNameEntityClasificado(), "Clasificado");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CONL, Y_CONL, WIDTH_CONL, HEIGHT_CONL)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CONL, Y_CONL, WIDTH_CONL, HEIGHT_CONL, CONTADOR_E, ENTITY_CONTAINER, IM_COMPOSICION_LEFT.getNameEntityComposicion(), "Composicion");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CONR, Y_CONR, WIDTH_CONR, HEIGHT_CONR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CONR, Y_CONR, WIDTH_CONR, HEIGHT_CONR, CONTADOR_E, ENTITY_CONTAINER, IM_COMPOSICION_RIGHT.getNameEntityComposicion(), "Composicion");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CONU, Y_CONU, WIDTH_CONU, HEIGHT_CONU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CONU, Y_CONU, WIDTH_CONU, HEIGHT_CONU, CONTADOR_E, ENTITY_CONTAINER, IM_COMPOSICION_UP.getNameEntityComposicion(), "Composicion");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CONB, Y_CONB, WIDTH_CONB, HEIGHT_CONB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CONB, Y_CONB, WIDTH_CONB, HEIGHT_CONB, CONTADOR_E, ENTITY_CONTAINER, IM_COMPOSICION_BELOW.getNameEntityComposicion(), "Composicion");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CORL, Y_CORL, WIDTH_CORL, HEIGHT_CORL)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CORL, Y_CORL, WIDTH_CORL, HEIGHT_CORL, CONTADOR_E, ENTITY_CONTAINER, IM_COMPONEDOR_LEFT.getNameEntityComponedor(), "Componedor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CORR, Y_CORR, WIDTH_CORR, HEIGHT_CORR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CORR, Y_CORR, WIDTH_CORR, HEIGHT_CORR, CONTADOR_E, ENTITY_CONTAINER, IM_COMPONEDOR_RIGHT.getNameEntityComponedor(), "Componedor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CORU, Y_CORU, WIDTH_CORU, HEIGHT_CORU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CORU, Y_CORU, WIDTH_CORU, HEIGHT_CORU, CONTADOR_E, ENTITY_CONTAINER, IM_COMPONEDOR_UP.getNameEntityComponedor(), "Componedor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_CORB, Y_CORB, WIDTH_CORB, HEIGHT_CORB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CORB, Y_CORB, WIDTH_CORB, HEIGHT_CORB, CONTADOR_E, ENTITY_CONTAINER, IM_COMPONEDOR_BELOW.getNameEntityComponedor(), "Componedor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_GL, Y_GL, WIDTH_GL, HEIGHT_GL)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_GL, Y_GL, WIDTH_GL, HEIGHT_GL, CONTADOR_E, ENTITY_CONTAINER, IM_GENERAL_LEFT.getNameEntityGeneral(), "General");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_GR, Y_GR, WIDTH_GR, HEIGHT_GR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_GR, Y_GR, WIDTH_GR, HEIGHT_GR, CONTADOR_E, ENTITY_CONTAINER, IM_GENERAL_RIGHT.getNameEntityGeneral(), "General");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_GU, Y_GU, WIDTH_GU, HEIGHT_GU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_GU, Y_GU, WIDTH_GU, HEIGHT_GU, CONTADOR_E, ENTITY_CONTAINER, IM_GENERAL_UP.getNameEntityGeneral(), "General");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_GB, Y_GB, WIDTH_GB, HEIGHT_GB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_GB, Y_GB, WIDTH_GB, HEIGHT_GB, CONTADOR_E, ENTITY_CONTAINER, IM_GENERAL_BELOW.getNameEntityGeneral(), "General");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_IL, Y_IL, WIDTH_IL, HEIGHT_IL)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_IL, Y_IL, WIDTH_IL, HEIGHT_IL, CONTADOR_E, ENTITY_CONTAINER, IM_IS_A_LEFT.getNameEntityIsa(), "Isa");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_IR, Y_IR, WIDTH_IR, HEIGHT_IR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_IR, Y_IR, WIDTH_IR, HEIGHT_IR, CONTADOR_E, ENTITY_CONTAINER, IM_IS_A_RIGHT.getNameEntityIsa(), "Isa");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_IU, Y_IU, WIDTH_IU, HEIGHT_IU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_IU, Y_IU, WIDTH_IU, HEIGHT_IU, CONTADOR_E, ENTITY_CONTAINER, IM_IS_A_UP.getNameEntityIsa(), "Isa");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_IB, Y_IB, WIDTH_IB, HEIGHT_IB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_IB, Y_IB, WIDTH_IB, HEIGHT_IB, CONTADOR_E, ENTITY_CONTAINER, IM_IS_A_BELOW.getNameEntityIsa(), "Isa");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_SL, Y_SL, WIDTH_SL, HEIGHT_SL)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_SL, Y_SL, WIDTH_SL, HEIGHT_SL, CONTADOR_E, ENTITY_CONTAINER, IM_RSIMPLE_LEFT.getNameEntityRSimple(), "RSimple");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_SR, Y_SR, WIDTH_SR, HEIGHT_SR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_SR, Y_SR, WIDTH_SR, HEIGHT_SR, CONTADOR_E, ENTITY_CONTAINER, IM_RSIMPLE_RIGHT.getNameEntityRSimple(), "RSimple");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_SU, Y_SU, WIDTH_SU, HEIGHT_SU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_SU, Y_SU, WIDTH_SU, HEIGHT_SU, CONTADOR_E, ENTITY_CONTAINER, IM_RSIMPLE_UP.getNameEntityRSimple(), "RSimple");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_SB, Y_SB, WIDTH_SB, HEIGHT_SB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_SB, Y_SB, WIDTH_SB, HEIGHT_SB, CONTADOR_E, ENTITY_CONTAINER, IM_RSIMPLE_BELOW.getNameEntityRSimple(), "RSimple");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_AL, Y_AL, WIDTH_AL, HEIGHT_AL)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_AL, Y_AL, WIDTH_AL, HEIGHT_AL, CONTADOR_E, ENTITY_CONTAINER, IM_ACTOR_LEFT.getNameEntityActor(), "Actor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_AR, Y_AR, WIDTH_AR, HEIGHT_AR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_AR, Y_AR, WIDTH_AR, HEIGHT_AR, CONTADOR_E, ENTITY_CONTAINER, IM_ACTOR_RIGHT.getNameEntityActor(), "Actor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_AU, Y_AU, WIDTH_AU, HEIGHT_AU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_AU, Y_AU, WIDTH_AU, HEIGHT_AU, CONTADOR_E, ENTITY_CONTAINER, IM_ACTOR_UP.getNameEntityActor(), "Actor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_AB, Y_AB, WIDTH_AB, HEIGHT_AB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_AB, Y_AB, WIDTH_AB, HEIGHT_AB, CONTADOR_E, ENTITY_CONTAINER, IM_ACTOR_BELOW.getNameEntityActor(), "Actor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_HL, Y_HL, WIDTH_HL, HEIGHT_HL)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_HL, Y_HL, WIDTH_HL, HEIGHT_HL, CONTADOR_E, ENTITY_CONTAINER, IM_HISTORIA_LEFT.getNameEntityHistoria(), "Historia");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_HR, Y_HR, WIDTH_HR, HEIGHT_HR)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_HR, Y_HR, WIDTH_HR, HEIGHT_HR, CONTADOR_E, ENTITY_CONTAINER, IM_HISTORIA_RIGHT.getNameEntityHistoria(), "Historia");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_HU, Y_HU, WIDTH_HU, HEIGHT_HU)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_HU, Y_HU, WIDTH_HU, HEIGHT_HU, CONTADOR_E, ENTITY_CONTAINER, IM_HISTORIA_UP.getNameEntityHistoria(), "Historia");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_HB, Y_HB, WIDTH_HB, HEIGHT_HB)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_HB, Y_HB, WIDTH_HB, HEIGHT_HB, CONTADOR_E, ENTITY_CONTAINER, IM_HISTORIA_BELOW.getNameEntityHistoria(), "Historia");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                } else if (estaDentro(ev, X_MD1, Y_MD1, WIDTH_MD1, HEIGHT_MD1)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_MD1, Y_MD1, WIDTH_MD1, HEIGHT_MD1, CONTADOR_E, ENTITY_CONTAINER, IM_MD.getNameEntityMaestro(), "Maestro");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_MD = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_MD2, Y_MD2, WIDTH_MD2, HEIGHT_MD2)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_MD2, Y_MD2, WIDTH_MD2, HEIGHT_MD2, CONTADOR_E, ENTITY_CONTAINER, IM_MD.getNameEntityDetalle(), "Detalle");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_MD = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_CC1, Y_CC1, WIDTH_CC1, HEIGHT_CC1)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CC1, Y_CC1, WIDTH_CC1, HEIGHT_CC1, CONTADOR_E, ENTITY_CONTAINER, IM_CC.getNameEntityClasificador(), "Clasificador");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_CC = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_CC2, Y_CC2, WIDTH_CC2, HEIGHT_CC2)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_CC2, Y_CC2, WIDTH_CC2, HEIGHT_CC2, CONTADOR_E, ENTITY_CONTAINER, IM_CC.getNameEntityClasificado(), "Clasificado");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_CC = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_C1, Y_C1, WIDTH_C1, HEIGHT_C1)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_C1, Y_C1, WIDTH_C1, HEIGHT_C1, CONTADOR_E, ENTITY_CONTAINER, IM_C.getNameEntityComposicion(), "Composicion");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_C = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_C2, Y_C2, WIDTH_C2, HEIGHT_C2)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_C2, Y_C2, WIDTH_C2, HEIGHT_C2, CONTADOR_E, ENTITY_CONTAINER, IM_C.getNameEntityComponedor(), "Componedor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_C = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_C3, Y_C3, WIDTH_C3, HEIGHT_C3)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_C3, Y_C3, WIDTH_C3, HEIGHT_C3, CONTADOR_E, ENTITY_CONTAINER, IM_C.getNameEntityComponedor1(), "Componedor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_C = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_ISA1, Y_ISA1, WIDTH_ISA1, HEIGHT_ISA1)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_ISA1, Y_ISA1, WIDTH_ISA1, HEIGHT_ISA1, CONTADOR_E, ENTITY_CONTAINER, IM_ISA.getNameEntityGeneral(), "General");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_ISA = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_ISA2, Y_ISA2, WIDTH_ISA2, HEIGHT_ISA2)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_ISA2, Y_ISA2, WIDTH_ISA2, HEIGHT_ISA2, CONTADOR_E, ENTITY_CONTAINER, IM_ISA.getNameEntityIsa(), "Isa");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_ISA = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_RS1, Y_RS1, WIDTH_RS1, HEIGHT_RS1)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_RS1, Y_RS1, WIDTH_RS1, HEIGHT_RS1, CONTADOR_E, ENTITY_CONTAINER, IM_RS.getNameEntityRSimple(), "RSimple");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_RS = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_RH1, Y_RH1, WIDTH_RH1, HEIGHT_RH1)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_RH1, Y_RH1, WIDTH_RH1, HEIGHT_RH1, CONTADOR_E, ENTITY_CONTAINER, IM_RH.getNameEntityActor(), "Actor");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_RH = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_RH2, Y_RH2, WIDTH_RH2, HEIGHT_RH2)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_RH2, Y_RH2, WIDTH_RH2, HEIGHT_RH2, CONTADOR_E, ENTITY_CONTAINER, IM_RH.getNameEntityHistoria(), "Historia");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_RH = JPPPMenu.getPermiso();
                } else if (estaDentro(ev, X_B, Y_B, WIDTH_B, HEIGHT_B)) {
                    CONTADOR_E = CONTADOR_E + 1;
                    JPPPMenu = new JPopupMenuEntityType(PG, X_B, Y_B, WIDTH_B, HEIGHT_B, CONTADOR_E, ENTITY_CONTAINER, IM_B.getNameEntityBasico(), "Basico");
                    JPPPMenu.show(ev.getComponent(), ev.getX(), ev.getY());
                    PERMISSION_B = JPPPMenu.getPermiso();
                    System.out.println("PERMISSION_:" + JPPPMenu.getPermiso());
                } else {
                    System.out.println("ISO CLICK DERECHO Y ESTAS AFUERA DE LA ENTIDAD");
                }
                } catch (Exception e1) {
                }
            }
        }

        @Override
        public void mouseClicked(MouseEvent ev) {
        }
    }
//.............................................................................................................................   

    public void addIdioms(IdiomsModel i) {
        this.LIST_IDIOMS.add(i);
        this.repaint();
    }

    public void deletIdioms() {
        try {
        this.IDIOMS_MODEL = this.LIST_IDIOMS.get((this.LIST_IDIOMS.size() - 1));
        if (this.IDIOMS_MODEL.getIdiomsType().equals("MD")) {
            this.IM_MD = null;
            this.X_MD1 = 0;
            this.Y_MD1 = 0;
            this.X_MD2 = 0;
            this.Y_MD2 = 0;
            eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityMaestro(), this.IDIOMS_MODEL.getNameEntityDetalle(), "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getMaestro(), this.ENTITY_CONTAINER.getDetalle());
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("CC")) {
            this.IM_CC = null;
            this.X_CC1 = 0;
            this.Y_CC1 = 0;
            this.X_CC2 = 0;
            this.Y_CC2 = 0;
            eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityClasificador(), this.IDIOMS_MODEL.getNameEntityClasificado(), "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getClasificador(), this.ENTITY_CONTAINER.getClasificado());
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("C")) {
            this.IM_C = null;
            this.X_C1 = 0;
            this.Y_C1 = 0;
            this.X_C2 = 0;
            this.Y_C2 = 0;
            this.X_C3 = 0;
            this.Y_C3 = 0;
            eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComposicion(), this.IDIOMS_MODEL.getNameEntityComponedor(), this.IDIOMS_MODEL.getNameEntityComponedor1(), this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComposicion(), this.ENTITY_CONTAINER.getComponedor());
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("ISA")) {
            this.IM_ISA = null;
            this.X_ISA1 = 0;
            this.Y_ISA1 = 0;
            this.X_ISA2 = 0;
            this.Y_ISA2 = 0;
            eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityGeneral(), this.IDIOMS_MODEL.getNameEntityIsa(), "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getGeneral(), this.ENTITY_CONTAINER.getIsa());
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("RS")) {
            this.IM_RS = null;
            this.X_RS1 = 0;
            this.Y_RS1 = 0;
            eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getRSimple(), new LinkedList());
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("RH")) {
            this.IM_RH = null;
            this.X_RH1 = 0;
            this.Y_RH1 = 0;
            this.X_RH2 = 0;
            this.Y_RH2 = 0;
            eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityActor(), this.IDIOMS_MODEL.getNameEntityHistoria(), "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getActor(), this.ENTITY_CONTAINER.getHistoria());
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("B")) {
            this.IM_B = null;
            this.X_B1 = 0;
            this.Y_B = 0;
            eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityBasico(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getBasico(), new LinkedList());
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("Maestro")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_MAESTRO_LEFT = null;
                this.X_ML = 0;
                this.Y_ML = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityMaestro(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getMaestro(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_MAESTRO_RIGHT = null;
                this.X_MR = 0;
                this.Y_MR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityMaestro(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getMaestro(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_MAESTRO_UP = null;
                this.X_MU = 0;
                this.Y_MU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityMaestro(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getMaestro(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_MAESTRO_BELOW = null;
                this.X_MB = 0;
                this.Y_MB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityMaestro(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getMaestro(), new LinkedList());
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("Detalle")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_DETALLE_LEFT = null;
                this.X_DL = 0;
                this.Y_DL = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityDetalle(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getDetalle(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_DETALLE_RIGHT = null;
                this.X_DR = 0;
                this.Y_DR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityDetalle(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getDetalle(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_DETALLE_UP = null;
                this.X_DU = 0;
                this.Y_DU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityDetalle(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getDetalle(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_DETALLE_BELOW = null;
                this.X_DB = 0;
                this.Y_DB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityDetalle(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getDetalle(), new LinkedList());
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("Clasificador")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_CLASIFICADOR_LEFT = null;
                this.X_CLRL = 0;
                this.Y_CLRL = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityClasificador(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getClasificador(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_CLASIFICADOR_RIGHT = null;
                this.X_CLRR = 0;
                this.Y_CLRR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityClasificador(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getClasificador(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_CLASIFICADOR_UP = null;
                this.X_CLRU = 0;
                this.Y_CLRU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityClasificador(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getClasificador(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_CLASIFICADOR_BELOW = null;
                this.X_CLRB = 0;
                this.Y_CLRB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityClasificador(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getClasificador(), new LinkedList());
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("Clasificado")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_CLASIFICADO_LEFT = null;
                this.X_CLOL = 0;
                this.Y_CLOL = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityClasificado(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getClasificado(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_CLASIFICADO_RIGHT = null;
                this.X_CLOR = 0;
                this.Y_CLOR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityClasificado(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getClasificado(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_CLASIFICADO_UP = null;
                this.X_CLOU = 0;
                this.Y_CLOU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityClasificado(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getClasificado(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_CLASIFICADO_BELOW = null;
                this.X_CLOB = 0;
                this.Y_CLOB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityClasificado(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getClasificado(), new LinkedList());
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("Composicion")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_COMPOSICION_LEFT = null;
                this.X_CONL = 0;
                this.Y_CONL = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComposicion(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComposicion(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_COMPOSICION_RIGHT = null;
                this.X_CONR = 0;
                this.Y_CONR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComposicion(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComposicion(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_COMPOSICION_UP = null;
                this.X_CONU = 0;
                this.Y_CONU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComposicion(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComposicion(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_COMPOSICION_BELOW = null;
                this.X_CONB = 0;
                this.Y_CONB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComposicion(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComposicion(), new LinkedList());
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("Componedor")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_COMPONEDOR_LEFT = null;
                this.X_CORL = 0;
                this.Y_CORL = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComponedor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComponedor(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_COMPONEDOR_RIGHT = null;
                this.X_CORR = 0;
                this.Y_CORR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComponedor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComponedor(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_COMPONEDOR_UP = null;
                this.X_CORU = 0;
                this.Y_CORU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComponedor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComponedor(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_COMPONEDOR_BELOW = null;
                this.X_CORB = 0;
                this.Y_CORB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComponedor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComponedor(), new LinkedList());
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("Componedor")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_COMPONEDOR_LEFT = null;
                this.X_CORL = 0;
                this.Y_CORL = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComponedor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComponedor(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_COMPONEDOR_RIGHT = null;
                this.X_CORR = 0;
                this.Y_CORR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComponedor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComponedor(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_COMPONEDOR_UP = null;
                this.X_CORU = 0;
                this.Y_CORU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComponedor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComponedor(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_COMPONEDOR_BELOW = null;
                this.X_CORB = 0;
                this.Y_CORB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityComponedor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getComponedor(), new LinkedList());
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("General")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_GENERAL_LEFT = null;
                this.X_GL = 0;
                this.Y_GL = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityGeneral(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getGeneral(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_GENERAL_RIGHT = null;
                this.X_GR = 0;
                this.Y_GR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityGeneral(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getGeneral(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_GENERAL_UP = null;
                this.X_GU = 0;
                this.Y_GU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityGeneral(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getGeneral(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_GENERAL_BELOW = null;
                this.X_GB = 0;
                this.Y_GB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityGeneral(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getGeneral(), new LinkedList());
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("Isa")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_IS_A_LEFT = null;
                this.X_IL = 0;
                this.Y_IL = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityIsa(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getIsa(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_IS_A_RIGHT = null;
                this.X_IR = 0;
                this.Y_IR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityIsa(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getIsa(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_IS_A_UP = null;
                this.X_IU = 0;
                this.Y_IU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityIsa(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getIsa(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_IS_A_BELOW = null;
                this.X_IB = 0;
                this.Y_IB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityIsa(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getIsa(), new LinkedList());
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("RSimple")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_RSIMPLE_LEFT = null;
                this.X_SL = 0;
                this.Y_SL = 0;
                for (int i = 0; i < 13; i++) {
                    LinkedList auxList = new LinkedList();
                    if(i == 1){
                        auxList = this.ENTITY_CONTAINER.getMLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 2){
                        auxList = this.ENTITY_CONTAINER.getDLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 3){
                        auxList = this.ENTITY_CONTAINER.getCORLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 4){
                        auxList = this.ENTITY_CONTAINER.getCDOLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 5){
                        auxList = this.ENTITY_CONTAINER.getCLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 6){
                        auxList = this.ENTITY_CONTAINER.getCCLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 7){
                        auxList = this.ENTITY_CONTAINER.getGLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 8){
                        auxList = this.ENTITY_CONTAINER.getILlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 9){
                        auxList = this.ENTITY_CONTAINER.getRSLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 10){
                        auxList = this.ENTITY_CONTAINER.getALlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 11){
                        auxList = this.ENTITY_CONTAINER.getHLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 12){
                        auxList = this.ENTITY_CONTAINER.getBLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                }
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_RSIMPLE_RIGHT = null;
                this.X_SR = 0;
                this.Y_SR = 0;                
                for (int i = 0; i < 13; i++) {
                    LinkedList auxList = new LinkedList();
                    if(i == 1){
                        auxList = this.ENTITY_CONTAINER.getMLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 2){
                        auxList = this.ENTITY_CONTAINER.getDLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 3){
                        auxList = this.ENTITY_CONTAINER.getCORLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 4){
                        auxList = this.ENTITY_CONTAINER.getCDOLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 5){
                        auxList = this.ENTITY_CONTAINER.getCLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 6){
                        auxList = this.ENTITY_CONTAINER.getCCLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 7){
                        auxList = this.ENTITY_CONTAINER.getGLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 8){
                        auxList = this.ENTITY_CONTAINER.getILlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 9){
                        auxList = this.ENTITY_CONTAINER.getRSLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 10){
                        auxList = this.ENTITY_CONTAINER.getALlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 11){
                        auxList = this.ENTITY_CONTAINER.getHLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                    if(i == 12){
                        auxList = this.ENTITY_CONTAINER.getBLlaveSegundaria();
                        eliminarRSimpleDeLaLista(this.IDIOMS_MODEL.getNameEntityRSimple(), auxList);
                    }
                }
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_RSIMPLE_UP = null;
                this.X_SU = 0;
                this.Y_SU = 0;
                
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_RSIMPLE_BELOW = null;
                this.X_SB = 0;
                this.Y_SB = 0;
                
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("Actor")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_ACTOR_LEFT = null;
                this.X_AL = 0;
                this.Y_AL = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityActor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getActor(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_ACTOR_RIGHT = null;
                this.X_AR = 0;
                this.Y_AR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityActor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getActor(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_ACTOR_UP = null;
                this.X_AU = 0;
                this.Y_AU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityActor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getActor(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_ACTOR_BELOW = null;
                this.X_AB = 0;
                this.Y_AB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityActor(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getActor(), new LinkedList());
            }
        }
        if (this.IDIOMS_MODEL.getIdiomsType().equals("Historia")) {
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Left")) {
                this.IM_HISTORIA_LEFT = null;
                this.X_HL = 0;
                this.Y_HL = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityHistoria(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getHistoria(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Right")) {
                this.IM_HISTORIA_RIGHT = null;
                this.X_HR = 0;
                this.Y_HR = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityHistoria(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getHistoria(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Up")) {
                this.IM_HISTORIA_UP = null;
                this.X_HU = 0;
                this.Y_HU = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityHistoria(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getHistoria(), new LinkedList());
            }
            if (this.IDIOMS_MODEL.getIdiomsPosition().equals("Below")) {
                this.IM_HISTORIA_BELOW = null;
                this.X_HB = 0;
                this.Y_HB = 0;
                eliminarModeloDeLaLista(this.IDIOMS_MODEL.getNameEntityHistoria(), "", "", this.ENTITY_CONTAINER, this.ENTITY_CONTAINER.getHistoria(), new LinkedList());
            }
        }
        this.LIST_IDIOMS.remove(this.LIST_IDIOMS.size() - 1);
        this.repaint();
        
            
        } catch (Exception e1) {
        }
    }
    
    private void eliminarRSimpleDeLaLista(String entityName, LinkedList list){
        for (int i = 0; i < list.size(); i++) {
            LinkedList myAuxList = (LinkedList) list.get(i);
            String valueAttribute1 = (String) myAuxList.get(0);
            String valueAttribute2 = (String) myAuxList.get(4);
            if (valueAttribute1.equals(entityName) && valueAttribute2.equals(entityName)) {
                list.remove(i);
            }
        }
    }

    private void eliminarModeloDeLaLista(String entityName1, String entityName2, String entityName3, EntityContainer entityContainer, LinkedList entityList1, LinkedList entityList2) {
        LinkedList fKsList1 = new LinkedList();
        LinkedList fKsList2 = new LinkedList();
        fKsList1.add((LinkedList) entityContainer.getMLlaveSegundaria());
        fKsList2.add((LinkedList) entityContainer.getDLlaveSegundaria());
        fKsList1.add((LinkedList) entityContainer.getCORLlaveSegundaria());
        fKsList2.add((LinkedList) entityContainer.getCDOLlaveSegundaria());
        fKsList2.add((LinkedList) entityContainer.getCLlaveSegundaria());
        fKsList1.add((LinkedList) entityContainer.getCCLlaveSegundaria());
        fKsList1.add((LinkedList) entityContainer.getGLlaveSegundaria());
        fKsList2.add((LinkedList) entityContainer.getILlaveSegundaria());
        fKsList2.add((LinkedList) entityContainer.getRSLlaveSegundaria());
        fKsList1.add((LinkedList) entityContainer.getALlaveSegundaria());
        fKsList2.add((LinkedList) entityContainer.getHLlaveSegundaria());
        fKsList1.add((LinkedList) entityContainer.getBLlaveSegundaria());
        fKsList2.add((LinkedList) entityContainer.getBLlaveSegundaria());
        eliminarEntidad(entityName1, entityName2, entityName3, entityList1, entityList2, fKsList1, fKsList2);
        //eliminarEntidad(entityName1, entityName2, entityName3, entityList2, fKsList1, fKsList2);
    }

    private void eliminarEntidad(String entityName1, String entityName2, String entityName3, LinkedList entityList1, LinkedList entityList2, LinkedList fKsList1, LinkedList fKsList2) {
        for (int i = 0; i < entityList1.size(); i++) {
            LinkedList myAuxList = (LinkedList) entityList1.get(i);
            String attributeName = (String) myAuxList.get(0);
            if (attributeName.equals(entityName1)) {
                entityList1.remove(i);
                for (int j = 0; j < fKsList1.size(); j++) {
                    eliminarFKsDelaEntidadF(entityName1, (LinkedList) fKsList1.get(j));
                }
                for (int j = 0; j < fKsList2.size(); j++) {
                    eliminarFKsDelaEntidadD(entityName1, (LinkedList) fKsList2.get(j));
                }
            }
        }
        for (int i = 0; i < entityList2.size(); i++) {
            LinkedList myAuxList = (LinkedList) entityList2.get(i);
            String attributeName = (String) myAuxList.get(0);
            if (attributeName.equals(entityName2)) {
                entityList2.remove(i);
                for (int j = 0; j < fKsList1.size(); j++) {
                    eliminarFKsDelaEntidadF(entityName2, (LinkedList) fKsList1.get(j));
                }
                for (int j = 0; j < fKsList2.size(); j++) {
                    eliminarFKsDelaEntidadD(entityName2, (LinkedList) fKsList2.get(j));
                }
            }
        }
        for (int i = 0; i < entityList2.size(); i++) {
            LinkedList myAuxList = (LinkedList) entityList2.get(i);
            String attributeName = (String) myAuxList.get(0);
            if (attributeName.equals(entityName3)) {
                entityList2.remove(i);
                for (int j = 0; j < fKsList1.size(); j++) {
                    eliminarFKsDelaEntidadF(entityName3, (LinkedList) fKsList1.get(j));
                }
                for (int j = 0; j < fKsList2.size(); j++) {
                    eliminarFKsDelaEntidadD(entityName3, (LinkedList) fKsList2.get(j));
                }
            }
        }
    }

    private void eliminarFKsDelaEntidadF(String entityName, LinkedList fksList) {
        int cont = 0;
        while (cont < 5) {
            for (int i = 0; i < fksList.size(); i++) {
                LinkedList myAuxList = (LinkedList) fksList.get(i);
                String valueDate1 = (String) myAuxList.get(0);
                String valueAttribute = (String) myAuxList.get(1);
                boolean existAttribute = estdoExistenciaDelAtributo(entityName, valueAttribute);
                if (valueDate1.equals(entityName)) {
                    fksList.remove(i);
                }
                if (existAttribute) {
                    fksList.remove(i);
                }
            }
            cont++;
        }
    }

    private void eliminarFKsDelaEntidadD(String entityName, LinkedList fksList) {
        int cont = 0;
        while (cont < 5) {
            for (int i = 0; i < fksList.size(); i++) {
                LinkedList myAuxList = (LinkedList) fksList.get(i);
                String valueDate1 = (String) myAuxList.get(4);
                String valueAttribute = (String) myAuxList.get(1);
                boolean existAttribute = estdoExistenciaDelAtributo(entityName, valueAttribute);
                if (valueDate1.equals(entityName)) {
                    fksList.remove(i);
                }
                if (existAttribute) {
                    fksList.remove(i);
                }
            }
            cont++;
        }
    }

    private boolean estdoExistenciaDelAtributo(String entityName, String valueAttribute) {
        boolean res = false;
        int sizeEntityName = entityName.length();
        int sizeValueAttribute = valueAttribute.length();
        int asta = (sizeValueAttribute - sizeEntityName) + 1;
        for (int i = 0; i < asta; i++) {
            String palabra = valueAttribute.substring(i, i + sizeEntityName);
            if (entityName.equals(palabra)) {
                res = true;
            }
        }
        return res;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (0 != LIST_IDIOMS.size()) {
            for (IdiomsModel i : LIST_IDIOMS) {
                i.diseniarIdioms(g);
                if (i.getIdiomsType().equals("MD")) {
                    this.IM_MD = i;
                    this.X_MD1 = i.getX();
                    this.Y_MD1 = i.getY();
                    this.X_MD2 = i.getX_E2();
                    this.Y_MD2 = i.getY_E2();
                    this.CONTADOR_E = i.getContadorEntidad();
                }
                if (i.getIdiomsType().equals("CC")) {
                    this.IM_CC = i;
                    this.X_CC1 = i.getX();
                    this.Y_CC1 = i.getY();
                    this.X_CC2 = i.getX_E2();
                    this.Y_CC2 = i.getY_E2();
                    this.CONTADOR_E = i.getContadorEntidad();
                }
                if (i.getIdiomsType().equals("C")) {
                    this.IM_C = i;
                    this.X_C1 = i.getX();
                    this.Y_C1 = i.getY();
                    this.X_C2 = i.getX_E2();
                    this.Y_C2 = i.getY_E2();
                    this.X_C3 = i.getX_E3();
                    this.Y_C3 = i.getY_E3();
                    this.CONTADOR_E = i.getContadorEntidad();
                }
                if (i.getIdiomsType().equals("ISA")) {
                    this.IM_ISA = i;
                    this.X_ISA1 = i.getX();
                    this.Y_ISA1 = i.getY();
                    this.X_ISA2 = i.getX_E2();
                    this.Y_ISA2 = i.getY_E2();
                    this.CONTADOR_E = i.getContadorEntidad();
                }
                if (i.getIdiomsType().equals("RS")) {
                    this.IM_RS = i;
                    this.X_RS1 = i.getX();
                    this.Y_RS1 = i.getY();
                    this.CONTADOR_E = i.getContadorEntidad();
                }
                if (i.getIdiomsType().equals("RH")) {
                    this.IM_RH = i;
                    this.X_RH1 = i.getX();
                    this.Y_RH1 = i.getY();
                    this.X_RH2 = i.getX_E4();
                    this.Y_RH2 = i.getY_E4();
                    this.CONTADOR_E = i.getContadorEntidad();
                }
                if (i.getIdiomsType().equals("B")) {
                    this.IM_B = i;
                    this.X_B = i.getX();
                    this.Y_B = i.getY();
                    this.CONTADOR_E = i.getContadorEntidad();
                }
//MAESTRO...................................................................                 
                if (i.getIdiomsType().equals("Maestro")) {
                    if (i.getIdiomsPosition().equals("Left")) {
                        this.IM_MAESTRO_LEFT = i;
                        this.X_ML = i.getX();
                        this.Y_ML = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Right")) {
                        this.IM_MAESTRO_RIGHT = i;
                        this.X_MR = i.getX();
                        this.Y_MR = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Up")) {
                        this.IM_MAESTRO_UP = i;
                        this.X_MU = i.getX();
                        this.Y_MU = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Below")) {
                        this.IM_MAESTRO_BELOW = i;
                        this.X_MB = i.getX();
                        this.Y_MB = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                }
//DETALLE.......................................................................                 
                if (i.getIdiomsType().equals("Detalle")) {
                    if (i.getIdiomsPosition().equals("Left")) {
                        this.IM_DETALLE_LEFT = i;
                        this.X_DL = i.getX_E3();
                        this.Y_DL = i.getY_E3();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Right")) {
                        this.IM_DETALLE_RIGHT = i;
                        this.X_DR = i.getX_E2();
                        this.Y_DR = i.getY_E2();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Up")) {
                        this.IM_DETALLE_UP = i;
                        this.X_DU = i.getX_E5();
                        this.Y_DU = i.getY_E5();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Below")) {
                        this.IM_DETALLE_BELOW = i;
                        this.X_DB = i.getX_E4();
                        this.Y_DB = i.getY_E4();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                }
//CLASIFICADOR...................................................................                 
                if (i.getIdiomsType().equals("Clasificador")) {
                    if (i.getIdiomsPosition().equals("Left")) {
                        this.IM_CLASIFICADOR_LEFT = i;
                        this.X_CLRL = i.getX();
                        this.Y_CLRL = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Right")) {
                        this.IM_CLASIFICADOR_RIGHT = i;
                        this.X_CLRR = i.getX();
                        this.Y_CLRR = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Up")) {
                        this.IM_CLASIFICADOR_UP = i;
                        this.X_CLRU = i.getX();
                        this.Y_CLRU = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Below")) {
                        this.IM_CLASIFICADOR_BELOW = i;
                        this.X_CLRB = i.getX();
                        this.Y_CLRB = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                }
//LASIFICADOR...................................................................                 
                if (i.getIdiomsType().equals("Clasificado")) {
                    if (i.getIdiomsPosition().equals("Left")) {
                        this.IM_CLASIFICADO_LEFT = i;
                        this.X_CLOL = i.getX_E3();
                        this.Y_CLOL = i.getY_E3();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Right")) {
                        this.IM_CLASIFICADO_RIGHT = i;
                        this.X_CLOR = i.getX_E2();
                        this.Y_CLOR = i.getY_E2();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Up")) {
                        this.IM_CLASIFICADO_UP = i;
                        this.X_CLOU = i.getX_E5();
                        this.Y_CLOU = i.getY_E5();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Below")) {
                        this.IM_CLASIFICADO_BELOW = i;
                        this.X_CLOB = i.getX_E4();
                        this.Y_CLOB = i.getY_E4();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                }
//COMPONEDOR................................................................... 
                if (i.getIdiomsType().equals("Componedor")) {
                    if (i.getIdiomsPosition().equals("Left")) {
                        this.IM_COMPONEDOR_LEFT = i;
                        this.X_CORL = i.getX();
                        this.Y_CORL = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Right")) {
                        this.IM_COMPONEDOR_RIGHT = i;
                        this.X_CORR = i.getX();
                        this.Y_CORR = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Up")) {
                        this.IM_COMPONEDOR_UP = i;
                        this.X_CORU = i.getX();
                        this.Y_CORU = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Below")) {
                        this.IM_COMPONEDOR_BELOW = i;
                        this.X_CORB = i.getX();
                        this.Y_CORB = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                }
//COMPOSICION...................................................................                
                if (i.getIdiomsType().equals("Composicion")) {
                    if (i.getIdiomsPosition().equals("Left")) {
                        this.IM_COMPOSICION_LEFT = i;
                        this.X_CONL = i.getX_E3();
                        this.Y_CONL = i.getY_E3();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Right")) {
                        this.IM_COMPOSICION_RIGHT = i;
                        this.X_CONR = i.getX_E2();
                        this.Y_CONR = i.getY_E2();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Up")) {
                        this.IM_COMPOSICION_UP = i;
                        this.X_CONU = i.getX_E5();
                        this.Y_CONU = i.getY_E5();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Below")) {
                        this.IM_COMPOSICION_BELOW = i;
                        this.X_CONB = i.getX_E4();
                        this.Y_CONB = i.getY_E4();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                }
//GENERAL....................................................................... 
                if (i.getIdiomsType().equals("General")) {
                    if (i.getIdiomsPosition().equals("Left")) {
                        this.IM_GENERAL_LEFT = i;
                        this.X_GL = i.getX();
                        this.Y_GL = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Right")) {
                        this.IM_GENERAL_RIGHT = i;
                        this.X_GR = i.getX();
                        this.Y_GR = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Up")) {
                        this.IM_GENERAL_UP = i;
                        this.X_GU = i.getX();
                        this.Y_GU = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Below")) {
                        this.IM_GENERAL_BELOW = i;
                        this.X_GB = i.getX();
                        this.Y_GB = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                }
//ISA...........................................................................                 
                if (i.getIdiomsType().equals("Isa")) {
                    if (i.getIdiomsPosition().equals("Left")) {
                        this.IM_IS_A_LEFT = i;
                        this.X_IL = i.getX_E3();
                        this.Y_IL = i.getY_E3();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Right")) {
                        this.IM_IS_A_RIGHT = i;
                        this.X_IR = i.getX_E2();
                        this.Y_IR = i.getY_E2();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Up")) {
                        this.IM_IS_A_UP = i;
                        this.X_IU = i.getX_E5();
                        this.Y_IU = i.getY_E5();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Below")) {
                        this.IM_IS_A_BELOW = i;
                        this.X_IB = i.getX_E4();
                        this.Y_IB = i.getY_E4();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                }
//REFLEXIVO SINPLE..............................................................                 
//                if (i.getIdiomsType().equals("RSimple")) {
//                    if (i.getIdiomsPosition().equals("Left")) {
//                        this.IM_RSIMPLE_LEFT = i;
//                        this.X_SL = i.getX();
//                        this.Y_SL = i.getY();
//                        this.CONTADOR_E = i.getContadorEntidad();
//                    }
//                    if (i.getIdiomsPosition().equals("Right")) {
//                        this.IM_RSIMPLE_RIGHT = i;
//                        this.X_SR = i.getX();
//                        this.Y_SR = i.getY();
//                        this.CONTADOR_E = i.getContadorEntidad();
//                    }
//                    if (i.getIdiomsPosition().equals("Up")) {
//                        this.IM_RSIMPLE_UP = i;
//                        this.X_SU = i.getX();
//                        this.Y_SU = i.getY();
//                        this.CONTADOR_E = i.getContadorEntidad();
//                    }
//                    if (i.getIdiomsPosition().equals("Below")) {
//                        this.IM_RSIMPLE_BELOW = i;
//                        this.X_SB = i.getX();
//                        this.Y_SB = i.getY();
//                        this.CONTADOR_E = i.getContadorEntidad();
//                    }
//                }
//ACTOR.........................................................................                
                if (i.getIdiomsType().equals("Actor")) {
                    if (i.getIdiomsPosition().equals("Left")) {
                        this.IM_ACTOR_LEFT = i;
                        this.X_AL = i.getX();
                        this.Y_AL = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Right")) {
                        this.IM_ACTOR_RIGHT = i;
                        this.X_AR = i.getX();
                        this.Y_AR = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Up")) {
                        this.IM_ACTOR_UP = i;
                        this.X_AU = i.getX();
                        this.Y_AU = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Below")) {
                        this.IM_ACTOR_BELOW = i;
                        this.X_AB = i.getX();
                        this.Y_AB = i.getY();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                }
//HISTORIA......................................................................                
                if (i.getIdiomsType().equals("Historia")) {
                    if (i.getIdiomsPosition().equals("Left")) {
                        this.IM_HISTORIA_LEFT = i;
                        this.X_HL = i.getX_E3();
                        this.Y_HL = i.getY_E3();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Right")) {
                        this.IM_HISTORIA_RIGHT = i;
                        this.X_HR = i.getX_E2();
                        this.Y_HR = i.getY_E2();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Up")) {
                        this.IM_HISTORIA_UP = i;
                        this.X_HU = i.getX_E5();
                        this.Y_HU = i.getY_E5();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                    if (i.getIdiomsPosition().equals("Below")) {
                        this.IM_HISTORIA_BELOW = i;
                        this.X_HB = i.getX_E4();
                        this.Y_HB = i.getY_E4();
                        this.CONTADOR_E = i.getContadorEntidad();
                    }
                }
            }
        }
    }

    // Manejadores de eventos de MouseMotionListener..................................................................
    @Override
    public void mouseDragged(MouseEvent e) {
        if (!this.MOVED_MD1) {
            if (estaDentro(e, this.X_MD1, this.Y_MD1, this.WIDTH_MD1, this.HEIGHT_MD1)) {
                this.MOVED_MD1 = true;
            }
        }
        if (!this.MOVED_MD2) {
            if (estaDentro(e, this.X_MD2, this.Y_MD2, this.WIDTH_MD2, this.HEIGHT_MD2)) {
                this.MOVED_MD2 = true;
            }
        }
        if (!this.MOVED_CC1) {
            if (estaDentro(e, this.X_CC1, this.Y_CC1, this.WIDTH_CC1, this.HEIGHT_CC1)) {
                this.MOVED_CC1 = true;
            }
        }
        if (!this.MOVED_CC2) {
            if (estaDentro(e, this.X_CC2, this.Y_CC2, this.WIDTH_CC2, this.HEIGHT_CC2)) {
                this.MOVED_CC2 = true;
            }
        }
        if (!this.MOVED_C1) {
            if (estaDentro(e, this.X_C1, this.Y_C1, this.WIDTH_C1, this.HEIGHT_C1)) {
                this.MOVED_C1 = true;
            }
        }
        if (!this.MOVED_C2) {
            if (estaDentro(e, this.X_C2, this.Y_C2, this.WIDTH_C2, this.HEIGHT_C2)) {
                this.MOVED_C2 = true;
            }
        }
        if (!this.MOVED_C3) {
            if (estaDentro(e, this.X_C3, this.Y_C3, this.WIDTH_C3, this.HEIGHT_C3)) {
                this.MOVED_C3 = true;
            }
        }
        if (!this.MOVED_ISA1) {
            if (estaDentro(e, this.X_ISA1, this.Y_ISA1, this.WIDTH_ISA1, this.HEIGHT_ISA1)) {
                this.MOVED_ISA1 = true;
            }
        }
        if (!this.MOVED_ISA2) {
            if (estaDentro(e, this.X_ISA2, this.Y_ISA2, this.WIDTH_ISA2, this.HEIGHT_ISA2)) {
                this.MOVED_ISA2 = true;
            }
        }
        if (!this.MOVED_RS1) {
            if (estaDentro(e, this.X_RS1, this.Y_RS1, this.WIDTH_RS1, this.HEIGHT_RS1)) {
                this.MOVED_RS1 = true;
            }
        }
        if (!this.MOVED_RH1) {
            if (estaDentro(e, this.X_RH1, this.Y_RH1, this.WIDTH_RH1, this.HEIGHT_RH1)) {
                this.MOVED_RH1 = true;
            }
        }
        if (!this.MOVED_RH2) {
            if (estaDentro(e, this.X_RH2, this.Y_RH2, this.WIDTH_RH2, this.HEIGHT_RH2)) {
                this.MOVED_RH2 = true;
            }
        }
        if (!this.MOVED_B) {
            if (estaDentro(e, this.X_B, this.Y_B, this.WIDTH_B, this.HEIGHT_B)) {
                this.MOVED_B = true;
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        this.MOVED_MD1 = false;
        this.MOVED_MD2 = false;
        this.MOVED_CC1 = false;
        this.MOVED_CC2 = false;
        this.MOVED_C1 = false;
        this.MOVED_C2 = false;
        this.MOVED_C3 = false;
        this.MOVED_ISA1 = false;
        this.MOVED_ISA2 = false;
        this.MOVED_RS1 = false;
        this.MOVED_RH1 = false;
        this.MOVED_RH2 = false;
        this.MOVED_B = false;
    }

    // Manejadores de eventos de MouseListener........................................................................
    @Override
    public void mouseClicked(MouseEvent e) {
        try {
            if (1 == e.getButton()) {
                if (estaDentro(e, this.X_ML, this.Y_ML, this.WIDTH_ML, this.HEIGHT_ML)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Maestro", this.IM_MAESTRO_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_MR, this.Y_MR, this.WIDTH_MR, this.HEIGHT_MR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Maestro", this.IM_MAESTRO_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_MU, this.Y_MU, this.WIDTH_MU, this.HEIGHT_MU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Maestro", this.IM_MAESTRO_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_MB, this.Y_MB, this.WIDTH_MB, this.HEIGHT_MB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Maestro", this.IM_MAESTRO_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_DL, this.Y_DL, this.WIDTH_DL, this.HEIGHT_DL)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Detalle", this.IM_DETALLE_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_DR, this.Y_DR, this.WIDTH_DR, this.HEIGHT_DR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Detalle", this.IM_DETALLE_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_DU, this.Y_DU, this.WIDTH_DU, this.HEIGHT_DU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Detalle", this.IM_DETALLE_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_DB, this.Y_DB, this.WIDTH_DB, this.HEIGHT_DB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Detalle", this.IM_DETALLE_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CLRL, this.Y_CLRL, this.WIDTH_CLRL, this.HEIGHT_CLRL)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Clasificador", this.IM_CLASIFICADOR_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CLRR, this.Y_CLRR, this.WIDTH_CLRR, this.HEIGHT_CLRR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Clasificador", this.IM_CLASIFICADOR_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CLRU, this.Y_CLRU, this.WIDTH_CLRU, this.HEIGHT_CLRU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Clasificador", this.IM_CLASIFICADOR_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CLRB, this.Y_CLRB, this.WIDTH_CLRB, this.HEIGHT_CLRB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Clasificador", this.IM_CLASIFICADOR_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CLOL, this.Y_CLOL, this.WIDTH_CLOL, this.HEIGHT_CLOL)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Clasificado", this.IM_CLASIFICADO_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CLOR, this.Y_CLOR, this.WIDTH_CLOR, this.HEIGHT_CLOR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Clasificado", this.IM_CLASIFICADO_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CLOU, this.Y_CLOU, this.WIDTH_CLOU, this.HEIGHT_CLOU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Clasificado", this.IM_CLASIFICADO_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CLOB, this.Y_CLOB, this.WIDTH_CLOB, this.HEIGHT_CLOB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Clasificado", this.IM_CLASIFICADO_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CONL, this.Y_CONL, this.WIDTH_CONL, this.HEIGHT_CONL)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Composicion", this.IM_COMPOSICION_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CONR, this.Y_CONR, this.WIDTH_CONR, this.HEIGHT_CONR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Composicion", this.IM_COMPOSICION_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CONU, this.Y_CONU, this.WIDTH_CONU, this.HEIGHT_CONU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Composicion", this.IM_COMPOSICION_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CONB, this.Y_CONB, this.WIDTH_CONB, this.HEIGHT_CONB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Composicion", this.IM_COMPOSICION_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CORL, this.Y_CORL, this.WIDTH_CORL, this.HEIGHT_CORL)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Componedor", this.IM_COMPONEDOR_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CORR, this.Y_CORR, this.WIDTH_CORR, this.HEIGHT_CORR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Componedor", this.IM_COMPONEDOR_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CORU, this.Y_CORU, this.WIDTH_CORU, this.HEIGHT_CORU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Componedor", this.IM_COMPONEDOR_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CORB, this.Y_CORB, this.WIDTH_CORB, this.HEIGHT_CORB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Componedor", this.IM_COMPONEDOR_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_GL, this.Y_GL, this.WIDTH_GL, this.HEIGHT_GL)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "General", this.IM_GENERAL_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_GR, this.Y_GR, this.WIDTH_GR, this.HEIGHT_GR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "General", this.IM_GENERAL_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_GU, this.Y_GU, this.WIDTH_GU, this.HEIGHT_GU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "General", this.IM_GENERAL_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_GB, this.Y_GB, this.WIDTH_GB, this.HEIGHT_GB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "General", this.IM_GENERAL_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_IL, this.Y_IL, this.WIDTH_IL, this.HEIGHT_IL)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Isa", this.IM_IS_A_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_IR, this.Y_IR, this.WIDTH_IR, this.HEIGHT_IR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Isa", this.IM_IS_A_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_IU, this.Y_IU, this.WIDTH_IU, this.HEIGHT_IU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Isa", this.IM_IS_A_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_IB, this.Y_IB, this.WIDTH_IB, this.HEIGHT_IB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Isa", this.IM_IS_A_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_SL, this.Y_SL, this.WIDTH_SL, this.HEIGHT_SL)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "RSimple", this.IM_RSIMPLE_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_SR, this.Y_SR, this.WIDTH_SR, this.HEIGHT_SR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "RSimple", this.IM_RSIMPLE_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_SU, this.Y_SU, this.WIDTH_SU, this.HEIGHT_SU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "RSimple", this.IM_RSIMPLE_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_SB, this.Y_SB, this.WIDTH_SB, this.HEIGHT_SB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "RSimple", this.IM_RSIMPLE_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_AL, this.Y_AL, this.WIDTH_AL, this.HEIGHT_AL)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Actor", this.IM_ACTOR_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_AR, this.Y_AR, this.WIDTH_AR, this.HEIGHT_AR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Actor", this.IM_ACTOR_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_AU, this.Y_AU, this.WIDTH_AU, this.HEIGHT_AU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Actor", this.IM_ACTOR_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_AB, this.Y_AB, this.WIDTH_AB, this.HEIGHT_AB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Actor", this.IM_ACTOR_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_HL, this.Y_HL, this.WIDTH_HL, this.HEIGHT_HL)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Historia", this.IM_HISTORIA_LEFT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_HR, this.Y_HR, this.WIDTH_HR, this.HEIGHT_HR)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Historia", this.IM_HISTORIA_RIGHT, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_HU, this.Y_HU, this.WIDTH_HU, this.HEIGHT_HU)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Historia", this.IM_HISTORIA_UP, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_HB, this.Y_HB, this.WIDTH_HB, this.HEIGHT_HB)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Historia", this.IM_HISTORIA_BELOW, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_MD1, this.Y_MD1, this.WIDTH_MD1, this.HEIGHT_MD1)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Maestro", this.IM_MD, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_MD2, this.Y_MD2, this.WIDTH_MD2, this.HEIGHT_MD2)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Detalle", this.IM_MD, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CC1, this.Y_CC1, this.WIDTH_CC1, this.HEIGHT_CC1)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Clasificador", this.IM_CC, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_CC2, this.Y_CC2, this.WIDTH_CC2, this.HEIGHT_CC2)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Clasificado", this.IM_CC, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_C1, this.Y_C1, this.WIDTH_C1, this.HEIGHT_C1)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Composicion", this.IM_C, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_C2, this.Y_C2, this.WIDTH_C2, this.HEIGHT_C2)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Componedor", this.IM_C, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_C3, this.Y_C3, this.WIDTH_C3, this.HEIGHT_C3)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Componedor1", this.IM_C, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_ISA1, this.Y_ISA1, this.WIDTH_ISA1, this.HEIGHT_ISA1)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "General", this.IM_ISA, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_ISA2, this.Y_ISA2, this.WIDTH_ISA2, this.HEIGHT_ISA2)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Isa", this.IM_ISA, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_RS1, this.Y_RS1, this.WIDTH_RS1, this.HEIGHT_RS1)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "RSimple", this.IM_RS, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_RH1, this.Y_RH1, this.WIDTH_RH1, this.HEIGHT_RH1)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Actor", this.IM_RH, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_RH2, this.Y_RH2, this.WIDTH_RH2, this.HEIGHT_RH2)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Historia", this.IM_RH, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else if (estaDentro(e, this.X_B, this.Y_B, this.WIDTH_B, this.HEIGHT_B)) {
                    EditEntityTipe ve = new EditEntityTipe(null, true, "Basico", this.IM_B, ENTITY_CONTAINER, this.CONTADOR_E);
                    ve.setVisible(true);
                    this.repaint();
                } else {
                    System.out.println("ISO CLICK IZQUIERDO Y ESTAS AFUERA DE LA ENTIDAD");
                }
            }
        } catch (Exception e3) {
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {
        try {
            if (this.MOVED_MD1 && this.PERMISSION_MD) {
                this.X_MD1 = e.getX();
                this.Y_MD1 = e.getY();
                this.IM_MD.setX(this.X_MD1);
                this.IM_MD.setY(this.Y_MD1);
                this.X_MD2 = e.getX() + 250;
                this.Y_MD2 = e.getY();
                this.IM_MD.setX_E2(this.X_MD2);
                this.IM_MD.setY_E2(this.Y_MD2);
                this.repaint();
            }
            if (this.MOVED_MD2 && this.PERMISSION_MD) {
                this.X_MD2 = e.getX();
                this.Y_MD2 = e.getY();
                this.IM_MD.setX_E2(this.X_MD2);
                this.IM_MD.setY_E2(this.Y_MD2);
                this.X_MD1 = e.getX() - 250;
                this.Y_MD1 = e.getY();
                this.IM_MD.setX(this.X_MD1);
                this.IM_MD.setY(this.Y_MD1);
                this.repaint();
            }
            if (this.MOVED_CC1 && this.PERMISSION_CC) {
                this.X_CC1 = e.getX();
                this.Y_CC1 = e.getY();
                this.IM_CC.setX(this.X_CC1);
                this.IM_CC.setY(this.Y_CC1);
                this.X_CC2 = e.getX() + 250;
                this.Y_CC2 = e.getY();
                this.IM_CC.setX_E2(this.X_CC2);
                this.IM_CC.setY_E2(this.Y_CC2);
                this.repaint();
            }
            if (this.MOVED_CC2 && this.PERMISSION_CC) {
                this.X_CC2 = e.getX();
                this.Y_CC2 = e.getY();
                this.IM_CC.setX_E2(this.X_CC2);
                this.IM_CC.setY_E2(this.Y_CC2);
                this.X_CC1 = e.getX() - 250;
                this.Y_CC1 = e.getY();
                this.IM_CC.setX(this.X_CC1);
                this.IM_CC.setY(this.Y_CC1);
                this.repaint();
            }
            if (this.MOVED_C1 && this.PERMISSION_C) {
                this.X_C1 = e.getX();
                this.Y_C1 = e.getY();
                this.IM_C.setX(this.X_C1);
                this.IM_C.setY(this.Y_C1);
                this.X_C2 = e.getX() + 250;
                this.Y_C2 = e.getY();
                this.IM_C.setX_E2(this.X_C2);
                this.IM_C.setY_E2(this.Y_C2);
                this.X_C3 = e.getX() - 250;
                this.Y_C3 = e.getY();
                this.IM_C.setX_E3(this.X_C3);
                this.IM_C.setY_E3(this.Y_C3);
                this.repaint();
            }
            if (this.MOVED_C2 && this.PERMISSION_C) {
                this.X_C2 = e.getX();
                this.Y_C2 = e.getY();
                this.IM_C.setX_E2(this.X_C2);
                this.IM_C.setY_E2(this.Y_C2);
                this.X_C1 = e.getX() - 250;
                this.Y_C1 = e.getY();
                this.IM_C.setX(this.X_C1);
                this.IM_C.setY(this.Y_C1);
                this.X_C3 = e.getX() - 500;
                this.Y_C3 = e.getY();
                this.IM_C.setX_E3(this.X_C3);
                this.IM_C.setY_E3(this.Y_C3);
                this.repaint();
            }
            if (this.MOVED_C3 && this.PERMISSION_C) {
                this.X_C3 = e.getX();
                this.Y_C3 = e.getY();
                this.IM_C.setX_E3(this.X_C3);
                this.IM_C.setY_E3(this.Y_C3);
                this.X_C1 = e.getX() + 250;
                this.Y_C1 = e.getY();
                this.IM_C.setX(this.X_C1);
                this.IM_C.setY(this.Y_C1);
                this.X_C2 = e.getX() + 500;
                this.Y_C2 = e.getY();
                this.IM_C.setX_E2(this.X_C2);
                this.IM_C.setY_E2(this.Y_C2);
                this.repaint();
            }
            if (this.MOVED_ISA1 && this.PERMISSION_ISA) {
                this.X_ISA1 = e.getX();
                this.Y_ISA1 = e.getY();
                this.IM_ISA.setX(this.X_ISA1);
                this.IM_ISA.setY(this.Y_ISA1);
                this.X_ISA2 = e.getX() + 250;
                this.Y_ISA2 = e.getY();
                this.IM_ISA.setX_E2(this.X_ISA2);
                this.IM_ISA.setY_E2(this.Y_ISA2);
                this.repaint();
            }
            if (this.MOVED_ISA2 && this.PERMISSION_ISA) {
                this.X_ISA2 = e.getX();
                this.Y_ISA2 = e.getY();
                this.IM_ISA.setX_E2(this.X_ISA2);
                this.IM_ISA.setY_E2(this.Y_ISA2);
                this.X_ISA1 = e.getX() - 250;
                this.Y_ISA1 = e.getY();
                this.IM_ISA.setX(this.X_ISA1);
                this.IM_ISA.setY(this.Y_ISA1);
                this.repaint();
            }
            if (this.MOVED_RS1 && PERMISSION_RS) {
                this.X_RS1 = e.getX();
                this.Y_RS1 = e.getY();
                this.IM_RS.setX(this.X_RS1);
                this.IM_RS.setY(this.Y_RS1);
                this.repaint();
            }
            if (this.MOVED_RH1 && PERMISSION_RH) {
                this.X_RH1 = e.getX();
                this.Y_RH1 = e.getY();
                this.IM_RH.setX(this.X_RH1);
                this.IM_RH.setY(this.Y_RH1);
                this.X_RH2 = e.getX();
                this.Y_RH2 = e.getY() + 200;
                this.IM_RH.setX_E4(this.X_RH2);
                this.IM_RH.setY_E4(this.Y_RH2);
                this.repaint();
            }
            if (this.MOVED_RH2 && PERMISSION_RH) {
                this.X_RH2 = e.getX();
                this.Y_RH2 = e.getY();
                this.IM_RH.setX_E4(this.X_RH2);
                this.IM_RH.setY_E4(this.Y_RH2);
                this.X_RH1 = e.getX();
                this.Y_RH1 = e.getY() - 200;
                this.IM_RH.setX(this.X_RH1);
                this.IM_RH.setY(this.Y_RH1);
                this.repaint();
            }
            if (this.MOVED_B && PERMISSION_B) {
                this.X_B = e.getX();
                this.Y_B = e.getY();
                this.IM_B.setX(this.X_B);
                this.IM_B.setY(this.Y_B);
                this.repaint();
            }
        } catch (Exception e2) {

        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public Point getIdiomsLocation(MouseEvent e) {
        Point p = e.getPoint();
        Point p_location = this.getLocationOnScreen();
        return new Point((int) (p_location.getX() * p.getX()), (int) (p_location.getY() * p.getY()));
    }

    private boolean estaDentro(MouseEvent e, int x, int y, int w, int h) {
        if ((e.getX() > x) && (e.getX() < (x + w)) && (e.getY() > y) && (e.getY() < (y + h))) {
            return true;
        }
        return false;
    }
}
