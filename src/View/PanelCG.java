
package View;

import java.awt.FlowLayout;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class PanelCG extends JPanel{
    public JCheckBox jrbttn_MYSQL,jrbttn_POSTGRES;
    public PanelCG(){
        this.setSize(200,200);
        this.setLayout(new FlowLayout());
        this.definirComponentesPanel();
        this.setBorder(new TitledBorder(new EtchedBorder(), "DBMS"));
    }
    private void definirComponentesPanel() {
        jrbttn_MYSQL =  new JCheckBox("MYSQL");
        jrbttn_POSTGRES = new JCheckBox("POSTGRES");
        
        this.add(jrbttn_MYSQL);
        this.add(jrbttn_POSTGRES);
    }
}
