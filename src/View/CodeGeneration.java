/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import javax.swing.*;
import Container.EntityContainer;
import java.io.IOException;
import Export.ExportMySQL;
import Export.ExportPgSQL;
import java.awt.Image;

public class CodeGeneration extends javax.swing.JDialog implements ItemListener{
    
    private JButton jbttn_GUARDAR, jbttn_CANCELAR;
    private boolean generar_MYSQL, generar_POSTGRES;
    private JCheckBox jrbttn_MYSQL, jrbttn_POSTGRES;
    EntityContainer ENTITY_CONTAINER;
    ExportMySQL EXPORTAR_MYSQL;
    ExportPgSQL EXPORTAR_PGSQL;
    JTextArea jtxta_WARNIN;
    
    public CodeGeneration(java.awt.Frame parent, boolean modal, EntityContainer entityContainer, JTextArea jTxtA_warnin){
        super(parent, modal);
        //super("Generacion de codigo SQL");
        this.setLayout(new FlowLayout());
        this.setLocationRelativeTo(null);
        this.setSize(340,180);
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        Image icon = new ImageIcon(getClass().getResource("/Images/export.png")).getImage();
        this.setIconImage(icon);
        PanelCG p = new PanelCG();
        jrbttn_MYSQL = p.jrbttn_MYSQL;
        jrbttn_POSTGRES = p.jrbttn_POSTGRES;
        jbttn_GUARDAR = new JButton("GUARDAR");
        jbttn_CANCELAR = new JButton("CANCELAR");
        jbttn_GUARDAR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/guardar.png")));
        jbttn_CANCELAR.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cancel.png")));
        EXPORTAR_MYSQL =  new ExportMySQL();
        EXPORTAR_PGSQL = new ExportPgSQL();
        ENTITY_CONTAINER = entityContainer;
        jtxta_WARNIN = jTxtA_warnin;
                
        this.add(p);
        this.add(jbttn_GUARDAR);
        this.add(jbttn_CANCELAR);
        
        jbttn_GUARDAR.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jtxta_WARNIN.append("Generacion de codigo en marcha...."+"\n");
                jbttn_GuardarActionPerformed(evt, jtxta_WARNIN);
            }
        });
        jbttn_CANCELAR.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jbttn_CancelarActionPerformed(evt);
            }
        });
        jrbttn_MYSQL.addItemListener(this);
        jrbttn_POSTGRES.addItemListener(this);
        //this.setVisible(true);
    }

    public void itemStateChanged(ItemEvent ie) {
        if(jrbttn_MYSQL.isSelected()){
            setEstadoMYSQL(true);
        }
        if(jrbttn_POSTGRES.isSelected()) {
            setEstadoPOSTGRES(true);
        }
        if(jrbttn_MYSQL.isSelected() && jrbttn_POSTGRES.isSelected()){
            setEstadoMYSQL(true);
            setEstadoPOSTGRES(true);
        }
        if(!jrbttn_MYSQL.isSelected()){
            setEstadoMYSQL(false);
        }
        if(!(jrbttn_POSTGRES.isSelected())){
            setEstadoPOSTGRES(false);
        }
        if (!jrbttn_MYSQL.isSelected() && !jrbttn_POSTGRES.isSelected()){
            setEstadoMYSQL(false);
            setEstadoPOSTGRES(false);
        } 
    }
    private void jbttn_GuardarActionPerformed(ActionEvent evt, JTextArea textA) {
        if (evt.getSource() == jbttn_GUARDAR) {
            System.out.println("A GUARDADO, MYSQL: " + getEstadoMYSQL() + " " + "POSTGRES: " + getEstadoPOSTGRES());
            if (getEstadoMYSQL() == true && getEstadoPOSTGRES() == false) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setAcceptAllFileFilterUsed(false);
                int result = fileChooser.showSaveDialog(this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    String nombreArchivo = fileChooser.getDescription(fileChooser.getSelectedFile());
                    File ruta = fileChooser.getCurrentDirectory();
                    String rutaArchivo = ruta.getPath();                    
                    System.out.println("nombre = "+nombreArchivo);
                    System.out.println("ruta = "+rutaArchivo);                    
                    try { 
                        File archivo = new File(rutaArchivo+"\\"+nombreArchivo+".sql");
                        System.out.println("archivo: "+archivo);
                        if (!archivo.exists()) {
                            EXPORTAR_MYSQL.exportar(ENTITY_CONTAINER, archivo, textA);
                            JOptionPane.showMessageDialog(rootPane, "Archivo creado con éxito");
                        } else {
                            JOptionPane.showMessageDialog(rootPane, "El archivo ya existe");
                        }
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(rootPane, "Error al crear el archivo, intente de nuevo");
                    }
                }
            } else if (getEstadoPOSTGRES() == true && getEstadoMYSQL() == false) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setAcceptAllFileFilterUsed(false);
                int result = fileChooser.showSaveDialog(this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    String nombreArchivo = fileChooser.getDescription(fileChooser.getSelectedFile());
                    File ruta = fileChooser.getCurrentDirectory();
                    String rutaArchivo = ruta.getPath();                    
                    System.out.println("nombre = "+nombreArchivo);
                    System.out.println("ruta = "+rutaArchivo);                    
                    try { 
                        File archivo = new File(rutaArchivo+"\\"+nombreArchivo+".sql");
                        System.out.println("archivo: "+archivo);
                        if (!archivo.exists()) {
                            EXPORTAR_PGSQL.exportar(ENTITY_CONTAINER, archivo);
                            JOptionPane.showMessageDialog(rootPane, "Archivo creado con éxito");
                        } else {
                            JOptionPane.showMessageDialog(rootPane, "El archivo ya existe");
                        }
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(rootPane, "Error al crear el archivo");
                    }
                }
            } else if (getEstadoMYSQL() == true && getEstadoPOSTGRES() == true) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setAcceptAllFileFilterUsed(false);
                int result = fileChooser.showSaveDialog(this);
                if (result == JFileChooser.APPROVE_OPTION) {
                    String nombreArchivo = fileChooser.getDescription(fileChooser.getSelectedFile());
                    File ruta = fileChooser.getCurrentDirectory();
                    String rutaArchivo = ruta.getPath();                    
                    System.out.println("nombre = "+nombreArchivo);
                    System.out.println("ruta = "+rutaArchivo);                    
                    try { 
                        File archivoMYSQL = new File(rutaArchivo+"\\"+nombreArchivo+"MYSQL.sql");
                        System.out.println("archivo: "+archivoMYSQL);
                        File archivoPGSQL = new File(rutaArchivo+"\\"+nombreArchivo+"PGSQL.sql");
                        System.out.println("archivo: "+archivoPGSQL);
                        if (!archivoMYSQL.exists() && !archivoPGSQL.exists()) {
                            EXPORTAR_MYSQL.exportar(ENTITY_CONTAINER, archivoMYSQL, textA);
                            EXPORTAR_PGSQL.exportar(ENTITY_CONTAINER, archivoPGSQL);
                            JOptionPane.showMessageDialog(rootPane, "Archivos SQL creados con éxito");
                        } else {
                            JOptionPane.showMessageDialog(rootPane, "Uno del los archivos ya existe");
                        }
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(rootPane, "Error al crear los archivos");
                    }
                }
            } else {
                JOptionPane.showMessageDialog(rootPane, "selexione un motor de base de datos");
            }
        }
    }   
    private void jbttn_CancelarActionPerformed(ActionEvent evt) {
        if (evt.getSource() == jbttn_CANCELAR) {
            this.dispose();
        }
    }
    public boolean getEstadoMYSQL(){
        return generar_MYSQL;
    }
    public void setEstadoMYSQL(boolean nuevoEstado){
        generar_MYSQL = nuevoEstado;
    }
    public boolean getEstadoPOSTGRES(){
        return generar_POSTGRES;
    }
    public void setEstadoPOSTGRES(boolean nuevoEstado){
        generar_POSTGRES = nuevoEstado;
    }
}
    