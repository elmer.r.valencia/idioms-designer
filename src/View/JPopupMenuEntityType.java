/**
 * @author Elmer Valencia Ledezmas / gmail: elmer.r.valencia@gmail.com / cel:
 * 71413596
 */

package View;

import Container.EntityContainer;
import Model.IdiomsModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

public class JPopupMenuEntityType extends JPopupMenu{
    
    private JMenu JM_MAESTRO;
    private JMenuItem JMI_M_LEFT;
    private JMenuItem JMI_M_RIGHT;
    private JMenuItem JMI_M_UP;
    private JMenuItem JMI_M_BELOW;
//  -----------------------------------
    private JMenu JM_DETALLE;
    private JMenuItem JMI_D_LEFT;
    private JMenuItem JMI_D_RIGHT;
    private JMenuItem JMI_D_UP;
    private JMenuItem JMI_D_BELOW;
//  -----------------------------------
    private JMenu JM_CLASIFICADOR;
    private JMenuItem JMI_C_LEFT;
    private JMenuItem JMI_C_RIGHT;
    private JMenuItem JMI_C_UP;
    private JMenuItem JMI_C_BELOW;
//  -----------------------------------    
    private JMenu JM_CLASIFICADO;
    private JMenuItem JMI_CC_LEFT;
    private JMenuItem JMI_CC_RIGHT;
    private JMenuItem JMI_CC_UP;
    private JMenuItem JMI_CC_BELOW;
//  -----------------------------------    
    private JMenu JM_COMPOSICION;
    private JMenuItem JMI_CON_LEFT;
    private JMenuItem JMI_CON_RIGHT;
    private JMenuItem JMI_CON_UP;
    private JMenuItem JMI_CON_BELOW;
//  -----------------------------------    
    private JMenu JM_COMPONEDOR;
    private JMenuItem JMI_COR_LEFT;
    private JMenuItem JMI_COR_RIGHT;
    private JMenuItem JMI_COR_UP;
    private JMenuItem JMI_COR_BELOW;
//  -----------------------------------    
    private JMenu JM_GENERAL;
    private JMenuItem JMI_G_LEFT;
    private JMenuItem JMI_G_RIGHT;
    private JMenuItem JMI_G_UP;
    private JMenuItem JMI_G_BELOW;
//  -----------------------------------    
    private JMenu JM_ISA;
    private JMenuItem JMI_ISA_LEFT;
    private JMenuItem JMI_ISA_RIGHT;
    private JMenuItem JMI_ISA_UP;
    private JMenuItem JMI_ISA_BELOW;
//  -----------------------------------    
    private JMenu JM_RSIMPLE;
    private JMenuItem JMI_RS_LEFT;
    private JMenuItem JMI_RS_RIGHT;
    private JMenuItem JMI_RS_UP;
    private JMenuItem JMI_RS_BELOW;
//  -----------------------------------    
    private JMenu JM_ACTOR;
    private JMenuItem JMI_A_LEFT;
    private JMenuItem JMI_A_RIGHT;
    private JMenuItem JMI_A_UP;
    private JMenuItem JMI_A_BELOW;
//  -----------------------------------    
    private JMenu JM_HISTORIA;
    private JMenuItem JMI_H_LEFT;
    private JMenuItem JMI_H_RIGHT;
    private JMenuItem JMI_H_UP;
    private JMenuItem JMI_H_BELOW;
//------------------------------------------------------------------------------  
    private JSeparator S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, S13, S14, S15, S16, S17, S18, S19, S20, S43;
    private JSeparator S21, S22, S23, S24, S25, S26, S27, S28, S29, S30, S31, S32, S33, S34, S35, S36, S37, S38, S39, S40, S41, S42;
    private final PanelGraphics P_G;
    private int X;
    private int Y;
    private int WIDTH;
    private int HEIGHT;
    
    private IdiomsModel I_M;
    
    private EntityContainer E_C;
//CONTADORE PARA CADA EBTIDAD CREADA    
    private int CONTADOR_E;
//TIPO DE ENTIDAD RELACIONADORA:
    private String ENTITY_RELATION_NAME;
    private String ENTITY_TIPE;
//PERMISO PARA MOVIMIENTO DE LA ENTIDAD
    private boolean PERMISSION;
//UBICACIONES PARA EL DISEÑO DE UN TIPO DE ENTIDAD 
    private String IZQUIERDA = "Izquierda", DERECHA = "Derecha", ARRIBA = "Arriba", ABAJO = "Abajo";
    
    public JPopupMenuEntityType(PanelGraphics panelGraphics, int x, int y, int width, int height, int contador, EntityContainer entityContainer, String entityName, String entityTipe) {
        this.init();
        this.add();
        this.P_G = panelGraphics;
        this.E_C = entityContainer;
        this.ENTITY_RELATION_NAME = entityName;
        this.ENTITY_TIPE = entityTipe;
        this.X = x;
        this.Y = y;
        this.WIDTH = width;
        this.HEIGHT = height;
        this.CONTADOR_E = contador;
    }

    private void init() {
        
//MAESTRO.......................................................................        
        this.JM_MAESTRO = new JMenu("Maestro");
        this.JMI_M_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_M_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_M_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X - (250);
                int y = Y;
                String tipo = "Maestro";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                setPermiso(false);
            }
        });
        this.JMI_M_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_M_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_M_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X + (250);
                int y = Y;
                String tipo = "Maestro";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i); 
                PERMISSION = false;
            }
        });
        this.JMI_M_UP = new JMenuItem(this.ARRIBA);
        this.JMI_M_UP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/up.png")));
        this.JMI_M_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y - (200);
                String tipo = "Maestro";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);   
                PERMISSION = false;
            }
        });
        this.JMI_M_BELOW = new JMenuItem(this.ABAJO);
        this.JMI_M_BELOW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/down.png")));
        this.JMI_M_BELOW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y + (200);
                String tipo = "Maestro";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i); 
                PERMISSION = false;
            }
        });
// DETALE.......................................................................        
        this.JM_DETALLE = new JMenu("Detalle");
        this.JMI_D_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_D_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_D_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Detalle";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_D_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_D_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_D_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Detalle";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_D_UP = new JMenuItem(this.ARRIBA);
        this.JMI_D_UP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/up.png")));
        this.JMI_D_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Detalle";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_D_BELOW = new JMenuItem(this.ABAJO);
        this.JMI_D_BELOW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/down.png")));
        this.JMI_D_BELOW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Detalle";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
//CLASIFICADOR..................................................................
        this.JM_CLASIFICADOR = new JMenu("Clasificador");
        this.JMI_C_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_C_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_C_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X - (250);
                int y = Y;
                String tipo = "Clasificador";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_C_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_C_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_C_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X + (250);
                int y = Y;
                String tipo = "Clasificador";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_C_UP = new JMenuItem(this.ARRIBA);
        this.JMI_C_UP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/up.png")));
        this.JMI_C_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y - (200);
                String tipo = "Clasificador";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_C_BELOW = new JMenuItem(this.ABAJO);
        this.JMI_C_BELOW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/down.png")));
        this.JMI_C_BELOW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y + (200);
                String tipo = "Clasificador";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
//CLASIFICADO...................................................................
        this.JM_CLASIFICADO = new JMenu("Clasificado");
        this.JMI_CC_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_CC_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_CC_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Clasificado";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_CC_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_CC_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_CC_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Clasificado";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
           }
        });
        this.JMI_CC_UP = new JMenuItem(this.ARRIBA);
        this.JMI_CC_UP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/up.png")));
        this.JMI_CC_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Clasificado";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_CC_BELOW = new JMenuItem(this.ABAJO);
        this.JMI_CC_BELOW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/down.png")));
        this.JMI_CC_BELOW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Clasificado";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
//COMPOSICION...................................................................
        this.JM_COMPOSICION = new JMenu("Composicion");
        this.JMI_CON_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_CON_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_CON_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Composicion";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_CON_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_CON_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_CON_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Composicion";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_CON_UP = new JMenuItem(this.ARRIBA);
        this.JMI_CON_UP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/up.png")));
        this.JMI_CON_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Composicion";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_CON_BELOW = new JMenuItem(this.ABAJO);
        this.JMI_CON_BELOW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/down.png")));
        this.JMI_CON_BELOW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Composicion";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
//COMPONEDOR....................................................................
        this.JM_COMPONEDOR = new JMenu("Componedor");
        this.JMI_COR_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_COR_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_COR_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X - (250);
                int y = Y;
                String tipo = "Componedor";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_COR_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_COR_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_COR_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X + (250);
                int y = Y;
                String tipo = "Componedor";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_COR_UP = new JMenuItem(this.ARRIBA);
        this.JMI_COR_UP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/up.png")));
        this.JMI_COR_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y - (200);
                String tipo = "Componedor";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_COR_BELOW = new JMenuItem(this.ABAJO);
        this.JMI_COR_BELOW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/down.png")));
        this.JMI_COR_BELOW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y + (200);
                String tipo = "Componedor";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
//GENERAL.......................................................................
        this.JM_GENERAL = new JMenu("General");
        this.JMI_G_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_G_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_G_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X - (250);
                int y = Y;
                String tipo = "General";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_G_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_G_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_G_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X + (250);
                int y = Y;
                String tipo = "General";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_G_UP = new JMenuItem(this.ARRIBA);
        this.JMI_G_UP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/up.png")));
        this.JMI_G_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y - (200);
                String tipo = "General";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_G_BELOW = new JMenuItem(this.ABAJO);
        this.JMI_G_BELOW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/down.png")));
        this.JMI_G_BELOW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y + (200);
                String tipo = "General";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
//ISA...........................................................................
        this.JM_ISA = new JMenu("Isa");
        this.JMI_ISA_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_ISA_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_ISA_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Isa";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_ISA_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_ISA_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_ISA_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Isa";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_ISA_UP = new JMenuItem(this.ARRIBA);
        this.JMI_ISA_UP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/up.png")));
        this.JMI_ISA_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Isa";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_ISA_BELOW = new JMenuItem(this.ABAJO);
        this.JMI_ISA_BELOW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/down.png")));
        this.JMI_ISA_BELOW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Isa";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
//RSIMPLE.......................................................................
        this.JM_RSIMPLE = new JMenu("RSimple");
        this.JMI_RS_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_RS_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_RS_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "RSimple";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_RS_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_RS_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_RS_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "RSimple";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
//        this.JMI_RS_UP = new JMenuItem("Up");
//        this.JMI_RS_UP.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                int x = X;
//                int y = Y;
//                String tipo = "RSimple";
//                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up");
//                PG.addIdioms(i);
//            }
//        });
//        this.JMI_RS_BELOW = new JMenuItem("Below");
//        this.JMI_RS_BELOW.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                int x = X;
//                int y = Y;
//                String tipo = "RSimple";
//                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below");
//                P_G.addIdioms(i);
//            }
//        });
//ACTOR.........................................................................
        this.JM_ACTOR = new JMenu("Actor");
        this.JMI_A_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_A_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_A_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X - (250);
                int y = Y;
                String tipo = "Actor";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_A_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_A_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_A_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X + (250);
                int y = Y;
                String tipo = "Actor";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_A_UP = new JMenuItem(this.ARRIBA);
        this.JMI_A_UP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/up.png")));
        this.JMI_A_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y - (200);
                String tipo = "Actor";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_A_BELOW = new JMenuItem(this.ABAJO);
        this.JMI_A_BELOW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/down.png")));
        this.JMI_A_BELOW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y + (200);
                String tipo = "Actor";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
//HISTORIA......................................................................
        this.JM_HISTORIA = new JMenu("Historia");
        this.JMI_H_LEFT = new JMenuItem(this.IZQUIERDA);
        this.JMI_H_LEFT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/left.png")));
        this.JMI_H_LEFT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Historia";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Left", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_H_RIGHT = new JMenuItem(this.DERECHA);
        this.JMI_H_RIGHT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/right.png")));
        this.JMI_H_RIGHT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Historia";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Right", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_H_UP = new JMenuItem(this.ARRIBA);
        this.JMI_H_UP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/up.png")));
        this.JMI_H_UP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Historia";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Up", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        this.JMI_H_BELOW = new JMenuItem(this.ABAJO);
        this.JMI_H_BELOW.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/down.png")));
        this.JMI_H_BELOW.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int x = X;
                int y = Y;
                String tipo = "Historia";
                IdiomsModel i = new IdiomsModel(x, y, tipo, "Below", E_C, CONTADOR_E, ENTITY_RELATION_NAME, ENTITY_TIPE);
                P_G.addIdioms(i);
                PERMISSION = false;
            }
        });
        
        this.S1 = new JSeparator();
        this.S2 = new JSeparator();
        this.S3 = new JSeparator();
        this.S3 = new JSeparator();
        this.S4 = new JSeparator();
        this.S5 = new JSeparator();
        this.S6 = new JSeparator();
        this.S7 = new JSeparator();
        this.S8 = new JSeparator();
        this.S9 = new JSeparator();
        this.S10 = new JSeparator();
        this.S11 = new JSeparator();
        this.S12 = new JSeparator();
        this.S13 = new JSeparator();
        this.S14 = new JSeparator();
        this.S15 = new JSeparator();
        this.S16 = new JSeparator();
        this.S17 = new JSeparator();
        this.S18 = new JSeparator();
        this.S19 = new JSeparator();
        this.S20 = new JSeparator();
        this.S21 = new JSeparator();
        this.S22 = new JSeparator();
        this.S23 = new JSeparator();
        this.S24 = new JSeparator();
        this.S25 = new JSeparator();
        this.S26 = new JSeparator();
        this.S27 = new JSeparator();
        this.S28 = new JSeparator();
        this.S29 = new JSeparator();
        this.S30 = new JSeparator();
        this.S31 = new JSeparator();
        this.S32 = new JSeparator();
        this.S33 = new JSeparator();
        this.S34 = new JSeparator();
        this.S35 = new JSeparator();
        this.S36 = new JSeparator();
        this.S37 = new JSeparator();
        this.S38 = new JSeparator();
        this.S39 = new JSeparator();
        this.S40 = new JSeparator();
        this.S41 = new JSeparator();
        this.S42 = new JSeparator();
        this.S43 = new JSeparator();
        
    }
    private void add(){
        this.JM_MAESTRO.add(JMI_M_LEFT);
        this.JM_MAESTRO.add(S11);
        this.JM_MAESTRO.add(JMI_M_RIGHT);
        this.JM_MAESTRO.add(S12);
        this.JM_MAESTRO.add(JMI_M_UP);
        this.JM_MAESTRO.add(S43);
        this.JM_MAESTRO.add(JMI_M_BELOW);
        
        this.JM_DETALLE.add(JMI_D_LEFT);
        this.JM_DETALLE.add(S13);
        this.JM_DETALLE.add(JMI_D_RIGHT);
        this.JM_DETALLE.add(S14);
        this.JM_DETALLE.add(JMI_D_UP);
        this.JM_DETALLE.add(S15);
        this.JM_DETALLE.add(JMI_D_BELOW);
        
        this.JM_CLASIFICADOR.add(JMI_C_LEFT);
        this.JM_CLASIFICADOR.add(S16);
        this.JM_CLASIFICADOR.add(JMI_C_RIGHT);
        this.JM_CLASIFICADOR.add(S17);
        this.JM_CLASIFICADOR.add(JMI_C_UP);
        this.JM_CLASIFICADOR.add(S18);
        this.JM_CLASIFICADOR.add(JMI_C_BELOW);
        
        this.JM_CLASIFICADO.add(JMI_CC_LEFT);
        this.JM_CLASIFICADO.add(S19);
        this.JM_CLASIFICADO.add(JMI_CC_RIGHT);
        this.JM_CLASIFICADO.add(S20);
        this.JM_CLASIFICADO.add(JMI_CC_UP);
        this.JM_CLASIFICADO.add(S21);
        this.JM_CLASIFICADO.add(JMI_CC_BELOW);
        
        this.JM_COMPOSICION.add(JMI_CON_LEFT);
        this.JM_COMPOSICION.add(S22);
        this.JM_COMPOSICION.add(JMI_CON_RIGHT);
        this.JM_COMPOSICION.add(S23);
        this.JM_COMPOSICION.add(JMI_CON_UP);
        this.JM_COMPOSICION.add(S24);
        this.JM_COMPOSICION.add(JMI_CON_BELOW);
        
        this.JM_COMPONEDOR.add(JMI_COR_LEFT);
        this.JM_COMPONEDOR.add(S25);
        this.JM_COMPONEDOR.add(JMI_COR_RIGHT);
        this.JM_COMPONEDOR.add(S26);
        this.JM_COMPONEDOR.add(JMI_COR_UP);
        this.JM_COMPONEDOR.add(S27);
        this.JM_COMPONEDOR.add(JMI_COR_BELOW);
        
        this.JM_GENERAL.add(JMI_G_LEFT);
        this.JM_GENERAL.add(S28);
        this.JM_GENERAL.add(JMI_G_RIGHT);
        this.JM_GENERAL.add(S29);
        this.JM_GENERAL.add(JMI_G_UP);
        this.JM_GENERAL.add(S30);
        this.JM_GENERAL.add(JMI_G_BELOW);
        
        this.JM_ISA.add(JMI_ISA_LEFT);
        this.JM_ISA.add(S31);
        this.JM_ISA.add(JMI_ISA_RIGHT);
        this.JM_ISA.add(S32);
        this.JM_ISA.add(JMI_ISA_UP);
        this.JM_ISA.add(S33);
        this.JM_ISA.add(JMI_ISA_BELOW);
        
        this.JM_RSIMPLE.add(JMI_RS_LEFT);
        this.JM_RSIMPLE.add(S34);
        this.JM_RSIMPLE.add(JMI_RS_RIGHT);
        
        this.JM_ACTOR.add(JMI_A_LEFT);
        this.JM_ACTOR.add(S37);
        this.JM_ACTOR.add(JMI_A_RIGHT);
        this.JM_ACTOR.add(S38);
        this.JM_ACTOR.add(JMI_A_UP);
        this.JM_ACTOR.add(S39);
        this.JM_ACTOR.add(JMI_A_BELOW);
        
        this.JM_HISTORIA.add(JMI_H_LEFT);
        this.JM_HISTORIA.add(S40);
        this.JM_HISTORIA.add(JMI_H_RIGHT);
        this.JM_HISTORIA.add(S41);
        this.JM_HISTORIA.add(JMI_H_UP);
        this.JM_HISTORIA.add(S42);
        this.JM_HISTORIA.add(JMI_H_BELOW);
        
        this.add(this.JM_MAESTRO);
        this.add(S1);
        this.add(JM_DETALLE);
        this.add(S2);
        this.add(JM_CLASIFICADOR);
        this.add(S3);
        this.add(JM_CLASIFICADO);
        this.add(S4);
        this.add(JM_COMPOSICION);
        this.add(S5);
        this.add(JM_COMPONEDOR);
        this.add(S6);
        this.add(JM_GENERAL);
        this.add(S7);
        this.add(JM_ISA);
        this.add(S8);
        this.add(JM_RSIMPLE);
        this.add(S9);
        this.add(JM_ACTOR);
        this.add(S10);
        this.add(JM_HISTORIA);
    }
    //SET AND GET :
    public int getXE(){
        return this.X;
    }
    public void setXE(int newX){
        this.X =  newX;
    }
    public int getYE(){
        return this.Y;
    }
    public void setYE(int newY){
        this.X =  newY;
    }
    public int getWIDTH(){
        return this.WIDTH;
    }
    public void setWIDTH(int newWIDTH){
        this.WIDTH =  newWIDTH;
    }
    public int getHEIGHT(){
        return this.HEIGHT;
    }
    public void setHEIGHT(int newHEIGHT){
        this.HEIGHT =  newHEIGHT;
    }
    //PERMISO 
    public boolean getPermiso(){
        return this.PERMISSION;
    }
    public void setPermiso( boolean newPermission){
        this.PERMISSION = newPermission;
    }
}