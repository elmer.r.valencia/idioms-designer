/**
 * @author Elmer Valencia Ledezmas elmer.r.valencia@gmail.com 71413596
 */
package Container;

import java.util.LinkedList;

public class EntityContainer {

    private LinkedList MAESTRO, maestro, m_llaveSegundaria;
    private LinkedList DETALLE, detalle, d_llaveSegundaria;
    private LinkedList CLASIFICADOR, clasificador, clr_llaveSegundaria;
    private LinkedList CLASIFICADO, clasificado, clo_llaveSegundaria;
    private LinkedList COMPOSICION, composicion, c_llaveSegundaria;
    private LinkedList COMPONEDOR, componedor, cc_llaveSegundaria;
    private LinkedList GENERAL, general, g_llaveSegundaria;
    private LinkedList IS_A, is_a, i_llaveSegundaria;
    private LinkedList REFLEXIVO_SIMPLE, rSimple, rs_llaveSegundaria;
    private LinkedList ACTOR, actor, a_llaveSegundaria;
    private LinkedList HISTORIA, historia, h_llaveSegundaria;
    private LinkedList BASICO, basico, b_llaveSegundaria;

    public EntityContainer() {
        init();
        add();
    }

    private void init() {
        this.MAESTRO = new LinkedList();
        this.maestro = new LinkedList();
        this.m_llaveSegundaria = new LinkedList();

        this.DETALLE = new LinkedList();
        this.detalle = new LinkedList();
        this.d_llaveSegundaria = new LinkedList();

        this.CLASIFICADOR = new LinkedList();
        this.clasificador = new LinkedList();
        this.clr_llaveSegundaria = new LinkedList();

        this.CLASIFICADO = new LinkedList();
        this.clasificado = new LinkedList();
        this.clo_llaveSegundaria = new LinkedList();

        this.COMPOSICION = new LinkedList();
        this.composicion = new LinkedList();
        this.c_llaveSegundaria = new LinkedList();

        this.COMPONEDOR = new LinkedList();
        this.componedor = new LinkedList();
        this.cc_llaveSegundaria = new LinkedList();

        this.GENERAL = new LinkedList();
        this.general = new LinkedList();
        this.g_llaveSegundaria = new LinkedList();

        this.IS_A = new LinkedList();
        this.is_a = new LinkedList();
        this.i_llaveSegundaria = new LinkedList();

        this.REFLEXIVO_SIMPLE = new LinkedList();
        this.rSimple = new LinkedList();
        this.rs_llaveSegundaria = new LinkedList();

        this.ACTOR = new LinkedList();
        this.actor = new LinkedList();
        this.a_llaveSegundaria = new LinkedList();

        this.HISTORIA = new LinkedList();
        this.historia = new LinkedList();
        this.h_llaveSegundaria = new LinkedList();

        this.BASICO = new LinkedList();
        this.basico = new LinkedList();
        this.b_llaveSegundaria = new LinkedList();

    }

    private void add() {
        this.MAESTRO.add(this.maestro);
        this.MAESTRO.add(this.m_llaveSegundaria);

        this.DETALLE.add(this.detalle);
        this.DETALLE.add(this.d_llaveSegundaria);

        this.CLASIFICADOR.add(this.clasificador);
        this.CLASIFICADOR.add(this.clr_llaveSegundaria);

        this.CLASIFICADO.add(this.clasificado);
        this.CLASIFICADO.add(this.clo_llaveSegundaria);

        this.COMPOSICION.add(this.composicion);
        this.COMPOSICION.add(this.c_llaveSegundaria);

        this.COMPONEDOR.add(this.componedor);
        this.COMPONEDOR.add(this.cc_llaveSegundaria);

        this.GENERAL.add(this.general);
        this.GENERAL.add(this.g_llaveSegundaria);

        this.IS_A.add(this.is_a);
        this.IS_A.add(this.i_llaveSegundaria);

        this.REFLEXIVO_SIMPLE.add(this.rSimple);
        this.REFLEXIVO_SIMPLE.add(this.rs_llaveSegundaria);

        this.ACTOR.add(this.actor);
        this.ACTOR.add(this.a_llaveSegundaria);

        this.HISTORIA.add(this.historia);
        this.HISTORIA.add(this.h_llaveSegundaria);

        this.BASICO.add(this.basico);
        this.BASICO.add(this.b_llaveSegundaria);
    }

    //MAESTRO...................................................................
    public LinkedList getMAESTRO() {
        return this.MAESTRO;
    }

    public LinkedList getMaestro() {
        return (LinkedList) MAESTRO.get(0);
    }

    public LinkedList getMLlaveSegundaria() {
        return (LinkedList) MAESTRO.get(1);
    }

    //DETALLE...................................................................
    public LinkedList getDETALLE() {
        return this.DETALLE;
    }

    public LinkedList getDetalle() {
        return (LinkedList) DETALLE.get(0);
    }

    public LinkedList getDLlaveSegundaria() {
        return (LinkedList) DETALLE.get(1);
    }

    //CLASIFICADOR..............................................................
    public LinkedList getCLASIFICADOR() {
        return this.CLASIFICADOR;
    }

    public LinkedList getClasificador() {
        return (LinkedList) CLASIFICADOR.get(0);
    }

    public LinkedList getCORLlaveSegundaria() {
        return (LinkedList) CLASIFICADOR.get(1);
    }

    //CLASIFICADO...............................................................
    public LinkedList getCLASIFICADO() {
        return this.CLASIFICADO;
    }

    public LinkedList getClasificado() {
        return (LinkedList) CLASIFICADO.get(0);
    }

    public LinkedList getCDOLlaveSegundaria() {
        return (LinkedList) CLASIFICADO.get(1);
    }

    //COMPOSICION...............................................................
    public LinkedList getCOMPOSICION() {
        return this.COMPOSICION;
    }

    public LinkedList getComposicion() {
        return (LinkedList) COMPOSICION.get(0);
    }

    public LinkedList getCLlaveSegundaria() {
        return (LinkedList) COMPOSICION.get(1);
    }

    //COMPONEDOR...............................................................
    public LinkedList getCOMPONEDOR() {
        return this.COMPONEDOR;
    }

    public LinkedList getComponedor() {
        return (LinkedList) COMPONEDOR.get(0);
    }

    public LinkedList getCCLlaveSegundaria() {
        return (LinkedList) COMPONEDOR.get(1);
    }

    //GENERAL...................................................................
    public LinkedList getGENERAL() {
        return this.GENERAL;
    }

    public LinkedList getGeneral() {
        return (LinkedList) GENERAL.get(0);
    }

    public LinkedList getGLlaveSegundaria() {
        return (LinkedList) GENERAL.get(1);
    }

    //ISA.......................................................................
    public LinkedList getISA() {
        return this.IS_A;
    }

    public LinkedList getIsa() {
        return (LinkedList) IS_A.get(0);
    }

    public LinkedList getILlaveSegundaria() {
        return (LinkedList) IS_A.get(1);
    }

    //REFLESIVO-SIMPLE..........................................................
    public LinkedList getRSIMPLE() {
        return this.REFLEXIVO_SIMPLE;
    }

    public LinkedList getRSimple() {
        return (LinkedList) REFLEXIVO_SIMPLE.get(0);
    }

    public LinkedList getRSLlaveSegundaria() {
        return (LinkedList) REFLEXIVO_SIMPLE.get(1);
    }

    //ACTOR.....................................................................
    public LinkedList getACTOR() {
        return this.ACTOR;
    }

    public LinkedList getActor() {
        return (LinkedList) ACTOR.get(0);
    }

    public LinkedList getALlaveSegundaria() {
        return (LinkedList) ACTOR.get(1);
    }

    //HISTORIA..................................................................
    public LinkedList getHISTORIA() {
        return this.HISTORIA;
    }

    public LinkedList getHistoria() {
        return (LinkedList) HISTORIA.get(0);
    }

    public LinkedList getHLlaveSegundaria() {
        return (LinkedList) HISTORIA.get(1);
    }

    //BASICO....................................................................
    public LinkedList getBASICO() {
        return this.BASICO;
    }

    public LinkedList getBasico() {
        return (LinkedList) BASICO.get(0);
    }

    public LinkedList getBLlaveSegundaria() {
        return (LinkedList) BASICO.get(1);
    }
}
